
CREATE TABLE RunSSIMText(
	RunId int NULL,
	ScheduleId int NULL,
	SchdVersionSeqNum int NULL,
	RunRowId int NOT NULL,
	RowText varchar(201) NULL
);


CREATE TABLE Flights(
	ScheduleId int NULL,
	FlightId int NULL,
	EqpTypeCd varchar(3) NULL,
	SegSeqNum int NULL,
	LegSeqNum int NULL,
	LegTypeCde char(1) NULL,
	ACOwner char(2) NULL,
	OpAirline char(2) NULL,
	OpFltNum int NULL,
	Orig char(3) NULL,
	Dest char(3) NULL,
	DepTime char(5) NULL,
	ArrTime char(5) NULL,
	DepTimeGmtOffset int NULL,
	ArrTimeGmtOffset int NULL,
	FromDate varchar(100) NULL,
	ToDate varchar(100) NULL,
	Frequency varchar(7) NULL,
	CxnAirline varchar(2) NULL,
	CxnFltNum int NULL,
	LayoverDays int NULL
);

CREATE TABLE FlightLegs(
	FlightId int NULL,
	ScheduleId int NULL,
	EqpTypeCd varchar(3) NULL,
	DepartDate varchar(50) NULL,
	ACOwner char(2) NULL,
	OpAirline char(2) NULL,
	OpFltNum int NULL,
	Orig char(3) NULL,
	Dest char(3) NULL,
	DepTime varchar(50) NULL,
	ArrTime varchar(50) NULL,
	DepTimeGmt varchar(50) NULL,
	ArrTimeGmt varchar(50) NULL,
	LegTypeCde char(1) NULL,
	RedEyeInd varchar(1) NOT NULL,
	AdjLocalDepDate varchar(50) NULL,
	AdjLocalArrDate varchar(50) NULL
);