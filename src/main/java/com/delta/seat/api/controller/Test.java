package com.delta.seat.api.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Random;
import java.util.UUID;
import org.apache.commons.io.IOUtils;

import com.delta.seat.api.dao.SeatEvtMsg;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Test {

	public static void main(String[] args) {
		Connection conn = null;
	    PreparedStatement pstmt = null;
	    try {
	    	conn = DriverManager.getConnection
					  ("jdbc:oracle:thin:@localhost:1522:orcl", "placovu", "rahaman");
	    	for(int i= 0;i<1;i++)
	    	{
	    	String uid = UUID.randomUUID().toString();
	    	String id = uid.substring(0,6);
	    	Random random = new Random();
	    	String flightNo = String.format("%04d", random.nextInt(10000));
            String jsonRequestText = readJsonFile("E:\\JAVA Development\\POCUI_doc\\RE_ regarding PocUI project\\Req_Scenario_INFTWithUnseated.json");
            if(jsonRequestText == null)
            	return;
            String compressedJsonRequestTextText = compressedJsonData(jsonRequestText);
           // compressedJsonRequestTextText = compressedJsonRequestTextText.replace("GPONVP", id);
           // compressedJsonRequestTextText = compressedJsonRequestTextText.replace("2547", flightNo);
           // compressedJsonRequestTextText = compressedJsonRequestTextText.replace("2018-09-28", "2018-11-05");
            String jsonResponsetText = readJsonFile("E:\\JAVA Development\\POCUI_doc\\RE_ regarding PocUI project\\Resp_Scenario_INFTWithUnseated.json");
            if(jsonResponsetText == null)
            	return;
            String compressedjsonResponsetText = compressedJsonData(jsonResponsetText);
            //compressedjsonResponsetText = compressedjsonResponsetText.replace("GPONVP", id);
            //compressedjsonResponsetText = compressedjsonResponsetText.replace("2547", flightNo);


//            String sql = "update SENG_EVT_MSG set SENG_RSP_MSG = ? where SENG_REQ_TRNS_ID = '1E1AB07BBDB24E108C842D83C71A0C3C'";
//            pstmt = conn.prepareStatement(sql);
//            Clob responseClob = conn.createClob();
//            responseClob.setString(1, compressedjsonResponsetText);
//            pstmt.setClob(1, responseClob);
//            pstmt.executeUpdate();
            
            insertFlightInfo(conn,compressedJsonRequestTextText,compressedjsonResponsetText,uid);
	    	}
//    		Connection conn = null;
//    		PreparedStatement pstmt = null;
//    		
//             SeatEvtMsg seatEvtMsg = new SeatEvtMsg();
//
//    		try {
//    			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:orcl", "placovu", "rahaman");
//    			String selectSQl = "select SENG_REQ_TRNS_ID as transactionId,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].operatingFlightNum') as operatingFlightNum,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].scheduledDepartureLocalDate') as scheduledDepartureLocalDate,SENG_REQ_MSG as offers from SENG_EVT_MSG se where   json_textcontains(SENG_REQ_MSG, '$.flightSegments.offers.recordLocatorId', 'c93095') and   json_textcontains(SENG_REQ_MSG, '$.flightSegments.operatingFlightNum', '3082')";
//    			pstmt = conn.prepareStatement(selectSQl);
//    			ResultSet rs = pstmt.executeQuery();
//    			System.out.println(rs);
//    			if (rs.next()) {
//    				System.out.println(rs.getString(1));
//    				System.out.println(rs.getString(2));
//    				//seatEvtMsg.setResponseClob(rs.get);
//    			}
//    		}

//    		catch (Exception ex) {
//    			ex.printStackTrace();
//    		}
//
//    		System.out.println (seatEvtMsg);
//    		System.out.println (seatEvtMsg.getRequestClob());
	        
	    } catch (Exception sqlex) {
	        System.out.println(sqlex);
	    }  finally {
	        if (pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    }
	}
	
	/* read json file */
	public static String readJsonFile(String filePath) throws Exception
	{
		
        String jsonTxt = null;
        File f = new File(filePath);
        if (f.exists()){
            InputStream is = new FileInputStream(f);
            jsonTxt = IOUtils.toString(is, "UTF-8");
            System.out.println("jsonTxt length: "+jsonTxt.length());
        }

        return jsonTxt;
	}
	
	/* compressed json data */
	public static String compressedJsonData(String jsonTxt) throws JsonParseException, JsonMappingException, IOException
	{
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(jsonTxt, JsonNode.class);
        
        System.out.println("JSON after compressed length: "+jsonNode.toString().length());
        return jsonNode.toString();
	}
	
	 /* insert flight info in database */
	public static void insertFlightInfo(Connection conn, String jsonRequestText,String jsonResponseText,String uid) throws SQLException
	{
		try{
		PreparedStatement pstmt = null;
        Clob requestClob = conn.createClob();
        requestClob.setString(1, jsonRequestText);
        Clob responseClob = conn.createClob();
        responseClob.setString(1, jsonResponseText);
       
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        
        String sql = "INSERT INTO SENG_EVT_MSG  " +
        "VALUES(?,?,?,?,?,?,?,?)";
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, uid);
		pstmt.setTimestamp(2, timestamp);
		pstmt.setClob(3, requestClob);
		pstmt.setClob(4, responseClob);
		pstmt.setInt(5,1);
		pstmt.setString(6, "SRVR_01");
		pstmt.setTimestamp(7, timestamp);
		pstmt.setString(8, "25HrBatchJob");
		pstmt.executeUpdate();
        System.out.println("insert complete");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
