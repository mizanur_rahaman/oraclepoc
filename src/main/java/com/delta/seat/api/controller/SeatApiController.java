package com.delta.seat.api.controller;

import com.delta.seat.api.common.EventTypeEnum;
import com.delta.seat.api.dao.PolicyParmDao;
import com.delta.seat.api.dao.SeatEvtMsg;
import com.delta.seat.api.dao.SeatTransaction;
import com.delta.seat.api.selection.Policy;
import com.delta.seat.api.service.*;
import com.delta.seat.util.StringUtils;

import io.swagger.annotations.ApiParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.delta.seat.api.util.SeatUtil;
import com.delta.seat.api.vo.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.*;
import java.util.List;



import static com.delta.seat.constants.SeatEngineConstants.NO_SEAT_SELECTION_MSG;

@RestController
public class SeatApiController {

    private static final Logger log = LogManager.getLogger(SeatApiController.class);
    private static final String SUCCESSFULL_SEAT_ASSIGNMENT_DB_UPDATE_MSG = "Seat Engine successfully updated DB with assigned seat value.";
    private static final String UNSUCCESSFULL_SEAT_ASSIGNMENT_DB_UPDATE_MSG = "Seat Engine couldn't update DB with assigned seat value.";

    @Autowired
    private SeatApiServiceImpl seatApiService;
    
    @Autowired
    private SeatAnalyticsService seatAnalyticsService;

    @Autowired
    private SeatEngineAsyncTaskExecutor seatEngineAsyncTaskExecutor;

    @Autowired
    private SeatAssignmentDataProcessor seatAssignmentDataProcessor;

    @Resource(name="seatdbJdbcTemplate")
    private JdbcTemplate seatdbJdbcTemplate;

    @Autowired
    private PolicyParmDao policyParmDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView welcome(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("index");
    }

    @ResponseBody
    @RequestMapping(value = "/v1/seats/selection", method = RequestMethod.GET)
    public SeatEvtMsg getFlightInfo(HttpServletRequest request, HttpServletResponse response) {
        String transactionId = request.getParameter("transactionId");
        return seatApiService.getSeatEvgMsgBasedOntransactionId(transactionId);
    }
    
    @ResponseBody
    @RequestMapping(value = "/v1/seats/analytics", method = RequestMethod.POST,headers="Accept=application/json", produces={"application/json"}, consumes={"application/json"})
    public SeatAnalyticsResponse getFlightAnalyticsData(HttpServletRequest request, HttpServletResponse response, @RequestBody SeatAnalyticsRequest seatAnalyticsRequest) {
    	
        return seatAnalyticsService.getFlightAnalyticsData(seatAnalyticsRequest);
    }

    @ResponseBody
    @RequestMapping(value = "/v1/seats/analytics/batchJobs", method = RequestMethod.GET)
    public List<String> getSeatAnalyticsBatchJobs(HttpServletRequest request, HttpServletResponse response) {
    	List<String> seatEngineTypes = seatApiService.getSeatEngineEventType();
    	return seatEngineTypes;
    }
    
    @ResponseBody
    @RequestMapping(value = "/v1/seats/passengers", method = RequestMethod.GET)
    public List<Passenger> getPassengers(HttpServletRequest request, HttpServletResponse response) {
    	String transactionId = request.getParameter("transactionId");
    	return seatAnalyticsService.getPassengersByTransactionId(transactionId);
    }
    
    @ResponseBody
    @RequestMapping(value = "/v1/seats/transactions", method = RequestMethod.POST,headers="Accept=application/json", produces={"application/json"}, consumes={"application/json"})
    public List<SeatTransaction> getTransactionsByResourceLocatorId(HttpServletRequest request, HttpServletResponse response
            , @RequestBody TransactionSelection transactionSelection) {
        return seatApiService.getTransactionsByResourceLocatorId(transactionSelection);
    }

    @ResponseBody
    @RequestMapping(value = "/v1/seats/seatassignment", method = RequestMethod.POST, headers="Accept=application/json", produces={"application/json"}, consumes={"application/json"})
    public SeatAssignmentDataUpdate updateSeatAssignmentValue(
            @RequestBody SeatSelectionOutput seatSelectionOutputRequest,
            @ApiParam(value = "Authorization") @RequestHeader(name="Authorization",  required=true) String authorization,
            @ApiParam(value = "Content-Type") @RequestHeader(name="Content-Type",  required=true) String contentType,
            @ApiParam(value = "Transation Id") @RequestHeader(name="TransactionId",  required=true) String transactionId,
            @ApiParam(value = "Application Id") @RequestHeader(name="appId",  required=true) String appId,
            @ApiParam(value = "Channel Id") @RequestHeader(name="channelId",  required=true) String channelId,
            @ApiParam(value = "Identifier of the API's consumer application") @RequestHeader(name="applicationName",  required=true) String applicationName,
            @ApiParam(value = "CaptureSeatAssignemnt") @RequestHeader(name="CaptureSeatAssignemnt", required=true) String captureSeatAssignemnt
    ) throws IOException {


        SeatAssignmentDataUpdate seatAssignmentDataUpdate = null;

        // verify the required parameter 'seatRecommendationRequest' is set. Check each status
        if (seatSelectionOutputRequest == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'seatSelectionOutputRequest' when calling seatassignment");
        }

        // verify the required parameter 'authorization' is set
        if (authorization == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'authorization' when calling seatassignment");
        }

        // verify the required parameter 'contentType' is set
        if (contentType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'contentType' when calling seatassignment");
        }

        // verify the required parameter 'transactionId' is set
        if (transactionId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'transactionId' when calling seatassignment");
        }

        // verify the required parameter 'channelId' is set
        if (channelId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'channelId' when calling seatassignment");
        }

        // verify the required parameter 'appId' is set
        if (appId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'appId' when calling seatassignment");
        }

        // verify the required parameter 'applicationName' is set
        if (applicationName == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'applicationName' when calling seatassignment");
        }

        // verify the required parameter 'applicationName' is set
        if (captureSeatAssignemnt == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'CaptureSeatAssignemnt' when calling seatassignment");
        }

        if (seatSelectionOutputRequest != null && seatSelectionOutputRequest.getFlightSegmentOutputs() != null &&
                seatSelectionOutputRequest.getFlightSegmentOutputs().size() >= 1  && seatSelectionOutputRequest.getFlightSegmentOutputs().get(0).getSeatSelections() != null
                && seatSelectionOutputRequest.getFlightSegmentOutputs().get(0).getSeatSelections().size() == 0){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "SeatSelections in json response is empty when calling seatassignment");
        }

        String jsonResponseStr = null;
        try {
            jsonResponseStr = StringUtils.getJsonString(seatSelectionOutputRequest);
        }catch (Exception ex){
            throw new RuntimeException(String.format("For the transactionId[%s], problem convertring incoming seat assignment json request!", transactionId));
        }

        log.debug(String.format("For the transactionId[%s], incoming seatSelectionOutputRequest message [%s] to update seatening db.",transactionId, jsonResponseStr));

        if (seatSelectionOutputRequest != null && seatSelectionOutputRequest.getFlightSegmentOutputs() != null && seatSelectionOutputRequest.getFlightSegmentOutputs().size() >= 1
            && seatSelectionOutputRequest.getFlightSegmentOutputs().get(0).getSeatSelections() != null
                && seatSelectionOutputRequest.getFlightSegmentOutputs().get(0).getSeatSelections().size() >=1) {

            boolean updateSeatAssignmentValue = seatAssignmentDataProcessor.updateSeatAssignmentValue(transactionId, jsonResponseStr);

            if (updateSeatAssignmentValue){
                seatAssignmentDataUpdate = new SeatAssignmentDataUpdate();
                seatAssignmentDataUpdate.setTransactionId(transactionId);
                seatAssignmentDataUpdate.setResponseCode("2000");
                seatAssignmentDataUpdate.setResponseMessageText(SUCCESSFULL_SEAT_ASSIGNMENT_DB_UPDATE_MSG);
                log.debug(String.format("For the transactionId[%s], seat engine successfully updated the ", transactionId));
                return seatAssignmentDataUpdate;
            }else {
                seatAssignmentDataUpdate = new SeatAssignmentDataUpdate();
                seatAssignmentDataUpdate.setTransactionId(transactionId);
                seatAssignmentDataUpdate.setResponseCode("4000");
                seatAssignmentDataUpdate.setResponseMessageText(UNSUCCESSFULL_SEAT_ASSIGNMENT_DB_UPDATE_MSG);
            }
        }

        return seatAssignmentDataUpdate;
    }

    @ResponseBody
    @RequestMapping(value = "/v1/seats/selection", method = RequestMethod.POST, headers="Accept=application/json", produces={"application/json"}, consumes={"application/json"})
    public SeatSelectionOutput selectseat(
            @RequestBody SeatSelectionInput seatSelectionRequest,
            @ApiParam(value = "Authorization") @RequestHeader(name="Authorization",  required=true) String authorization,
            @ApiParam(value = "Content-Type") @RequestHeader(name="Content-Type",  required=true) String contentType,
            @ApiParam(value = "Transation Id") @RequestHeader(name="TransactionId",  required=true) String transactionId,
            @ApiParam(value = "Application Id") @RequestHeader(name="appId",  required=true) String appId,
            @ApiParam(value = "Channel Id") @RequestHeader(name="channelId",  required=true) String channelId,
            @ApiParam(value = "SeatScore") @RequestHeader(name="seatScore", required=false) String seatScore,
            @ApiParam(value = "Identifier of the API's consumer application") @RequestHeader(name="applicationName",  required=true) String applicationName,
            @ApiParam(value = "MasterList") @RequestHeader(name="masterList", required=false) String masterList,
            @ApiParam(value = "InvokeOptimizer") @RequestHeader(name="invokeOptimizer", required=false) String invokeOptimizer
    ) throws IOException {

        // verify the required parameter 'seatRecommendationRequest' is set
        if (seatSelectionRequest == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'seatRecommendationRequest' when calling selectseat");
        }

        // verify the required parameter 'authorization' is set
        if (authorization == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'authorization' when calling selectseat");
        }

        // verify the required parameter 'contentType' is set
        if (contentType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'contentType' when calling selectseat");
        }

        // verify the required parameter 'transactionId' is set
        if (transactionId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'transactionId' when calling selectseat");
        }

        // verify the required parameter 'channelId' is set
        if (channelId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'channelId' when calling selectseat");
        }

        // verify the required parameter 'appId' is set
        if (appId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'appId' when calling selectseat");
        }

        // verify the required parameter 'applicationName' is set
        if (applicationName == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'applicationName' when calling selectseat");
        }

        if (seatSelectionRequest == null){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "seatRecommendationRequest is null when calling selectseat");
        }

        long startTime = System.currentTimeMillis();

        List<FlightSegmentInput> flightInfoRequestList= seatSelectionRequest.getFlightSegments();
        long latestPolicyId = 0;
        if (flightInfoRequestList == null || flightInfoRequestList.size() == 0){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "flightInfoRequestList is null when calling selectseat");
        }
        String jsonReqStr = StringUtils.getJsonString(seatSelectionRequest);
        jsonReqStr = jsonReqStr.replaceAll("(\\r|\\n)", "");



        log.debug(String.format("For the transactionId[%s] and appId[%s], incoming seatRecommendationRequest json [%s] ", transactionId, appId, jsonReqStr));

        String sengEventTypeCode = null;

        if (appId.equalsIgnoreCase(seatApiService.getBatchJobId())){
            sengEventTypeCode = EventTypeEnum.TwentyFiveHrBatchJob.value();
        } else if (appId.equalsIgnoreCase("ML")){
            sengEventTypeCode = EventTypeEnum.MasterList.value();
        } else {
            sengEventTypeCode = EventTypeEnum.ASAScoringAdjacency.value();
        }
        SeatSelectionOutput seatSelectionOutput = null;

        if (invokeOptimizer != null && !invokeOptimizer.isEmpty() && invokeOptimizer.equalsIgnoreCase("Y")){

            seatSelectionOutput = seatApiService.getSeatSelectionUsingOptimizer(jsonReqStr,transactionId);
            if (seatSelectionOutput != null){
                seatSelectionOutput.setTransactionId(transactionId);
            }
        } else if (seatScore != null && !seatScore.isEmpty() && seatScore.equalsIgnoreCase("Y") && (invokeOptimizer == null || invokeOptimizer.isEmpty() || (!invokeOptimizer.isEmpty() && invokeOptimizer.equalsIgnoreCase("N")))){
            if (sengEventTypeCode == null || sengEventTypeCode.isEmpty())
                sengEventTypeCode = EventTypeEnum.ASAScoringAdjacency.value();
            log.info(String.format("For the transactionId[%s], this will take seat score info and will apply logic.", transactionId));
            Policy.reload(policyParmDao);
            SeatingOption seatingOption = new SeatingOptionImpl();
            seatSelectionOutput  = seatingOption.selectSeats(seatSelectionRequest, transactionId, false);

        } else if (masterList != null && !masterList.isEmpty() && masterList.equalsIgnoreCase("Y") && (invokeOptimizer == null || invokeOptimizer.isEmpty() || (!invokeOptimizer.isEmpty() && invokeOptimizer.equalsIgnoreCase("N")))){
            if (sengEventTypeCode == null || sengEventTypeCode.isEmpty())
                sengEventTypeCode = EventTypeEnum.MasterList.value();
            log.debug(String.format("For the transactionId[%s], this will take masterList data and will apply logic.", transactionId));
            SeatingOption seatingOption = new SeatingOptionImpl();
            seatSelectionOutput  = seatingOption.selectSeats(seatSelectionRequest, transactionId, true);
        }
        else {
            try {
                seatSelectionOutput = seatApiService.getSeatSelection(seatSelectionRequest, transactionId);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(String.format("For the transactionId[%s], SeatEngineAPI couldn't process incoming request and threw error. Message is [%s]", transactionId, e.getMessage()));
            }
        }
        //no seat was selected by the seat engine. So, send a response message
        if (seatSelectionOutput == null || seatSelectionOutput.getFlightSegmentOutputs() == null || (seatSelectionOutput.getFlightSegmentOutputs() != null  && seatSelectionOutput.getFlightSegmentOutputs().size()>=1 && seatSelectionOutput.getFlightSegmentOutputs().get(0).getSeatSelections() == null || seatSelectionOutput.getFlightSegmentOutputs().get(0).getSeatSelections().isEmpty())) {
            seatSelectionOutput = new SeatSelectionOutput();
            seatSelectionOutput.setResponseMessageText(NO_SEAT_SELECTION_MSG);
        }
        String jsonRespStr = StringUtils.getJsonString(seatSelectionOutput).replaceAll("(\\r|\\n)", "");

        long sengCoreProcessingTime = System.currentTimeMillis();

        log.debug(String.format("For the transactionId[%s], total time taken to apply seat engine logic is [%s] msec", transactionId, (sengCoreProcessingTime - startTime)));

        seatEngineAsyncTaskExecutor.upsertSeatEvtRequestJsonDataAsynchronously(createSeatEvtMsg(jsonReqStr, jsonRespStr, transactionId,SeatUtil.getSeatEngineServerName(),startTime,sengCoreProcessingTime,sengEventTypeCode),transactionId);


        //This is the total time.
        log.debug(String.format("For the transactionId[%s], outgoing seatRecommendationResponse json [%s] with total time [%s] msecs including DB async write. ", transactionId, jsonRespStr, (System.currentTimeMillis() - startTime)));

        return seatSelectionOutput;
    }

    @RequestMapping(value = "/v1/seats/healthcheck", method = RequestMethod.GET)
    public String checkHealth(){
        return "It worked!";
    }

    private SeatEvtMsg createSeatEvtMsg(String jsonReqStr, String jsonRespStr, String transactionId_DB,String serverName,long startTime, long sengCoreProcessingTime, String sengEventTypeCode) {
        SeatEvtMsg seatEvtMsg = new SeatEvtMsg();
        seatEvtMsg.setTransactionid(transactionId_DB);
        seatEvtMsg.setCrtnGts(new Timestamp(System.currentTimeMillis()));
        seatEvtMsg.setRequestClob(jsonReqStr);
        seatEvtMsg.setResponseClob(jsonRespStr);
        seatEvtMsg.setServerName(serverName);
        seatEvtMsg.setStartTime(startTime);
        seatEvtMsg.setEndTime(sengCoreProcessingTime);
        seatEvtMsg.setSengEventTypeCode(sengEventTypeCode);
        return seatEvtMsg;
    }



    private void updateEvtMstWithResponseJsonData(String transactionId_DB, String jsonRespStr, long policyId, String serverName) {
        SeatEvtMsg seatEvtMsg = new SeatEvtMsg();
        seatEvtMsg.setTransactionid(transactionId_DB);
        seatEvtMsg.setCrtnGts(new Timestamp(System.currentTimeMillis()));
        seatEvtMsg.setResponseClob(jsonRespStr);
        seatEvtMsg.setPolicyId(policyId);
        seatEvtMsg.setServerName(serverName);
    }   

}
