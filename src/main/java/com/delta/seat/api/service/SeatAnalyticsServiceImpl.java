package com.delta.seat.api.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.delta.seat.api.dao.SeatAnalytics;
import com.delta.seat.api.dao.SeatEngineEvtMsgDao;
import com.delta.seat.api.dao.SeatEvtMsg;
import com.delta.seat.api.util.AppConstants;
import com.delta.seat.api.vo.LoyaltyMemberPreference;
import com.delta.seat.api.vo.Passenger;
import com.delta.seat.api.vo.SeatAnalyticsRequest;
import com.delta.seat.api.vo.SeatAnalyticsResponse;
import com.delta.seat.api.vo.SeatAnalyticsSengEvtMsg;
import com.delta.seat.api.vo.Ssr;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SeatAnalyticsServiceImpl implements SeatAnalyticsService {
    private static final Logger log = LogManager.getLogger(SeatAnalyticsServiceImpl.class);

	@Autowired
    private SeatEngineEvtMsgDao seatEngineEvtMsgDao;
	 @Override
    public SeatAnalyticsResponse getFlightAnalyticsData(SeatAnalyticsRequest seatAnalyticsRequest) {
        // TODO Auto-generated method stub
		 
	    SeatAnalyticsResponse seatAnalyticsResponse  = new SeatAnalyticsResponse();
	    try
	    {
	    	List<SeatAnalytics> seatEvtMsgList = seatEngineEvtMsgDao.getFlightAnalyticsData(seatAnalyticsRequest);
	    	if(seatEvtMsgList == null || seatEvtMsgList.size()<=0)
	    	{
	    		seatAnalyticsResponse.setSuccess(false);
	    		seatAnalyticsResponse.setMessage(AppConstants.noRecordFoundText);
	    		return seatAnalyticsResponse;
	    	}
	    	String result = "";
	    	seatAnalyticsResponse.setSuccess(true);
	    	seatAnalyticsResponse.setDataList(this.generateSeatAnalyticsEvtMsgList(seatEvtMsgList));
	    	this.processResponseAndUnseatedPaxPNRList(seatEvtMsgList);
	    
	    	if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.SinglePartyPNRValue))
			{
	    		int total = getTotalSinglePNRByRecommendationType(seatEvtMsgList,Integer.parseInt(seatAnalyticsRequest.getSeatRecommendation()));
	    		result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.SinglePartyPNRText+": " 
			           + total;
		    	seatAnalyticsResponse.setMessage(result);
	    		seatAnalyticsResponse.setShowTransaction(total>0);
			}
	    	else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.MultiPartyPNRValue))
			{
	    		this.getTotalMultiPNRByRecommendationType(seatEvtMsgList,Integer.parseInt(seatAnalyticsRequest.getSeatRecommendation()), seatAnalyticsResponse);
				
			}
	    	else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.FamilyPNRsValue))
			{
	    		this.getTotalFamilyPNRByRecommendationType(seatEvtMsgList,Integer.parseInt(seatAnalyticsRequest.getSeatRecommendation()),seatAnalyticsResponse);
		
			}
	    	else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.NonLoyalityProgramPassengerValue))
	    	{
	    		int total = this.getNonLoyalityProgramTireCodePassengers(seatEvtMsgList, seatAnalyticsRequest);
				result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.NonLoyalityProgramPassengerText+" " + AppConstants.passengerText+": "+total;
		    	seatAnalyticsResponse.setMessage(result);
				seatAnalyticsResponse.setShowTransaction(total>0);
	    	}
	    	else
	    	{
				int total = getTotalMedalationByMedalationType(seatEvtMsgList,seatAnalyticsRequest);
				if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.DiamondMedallionValue))
				{
					result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.DiamondMedallionText+" " + AppConstants.passengerText+": "+total;
				}
				else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.PlatinumMedallionValue))
				{
					result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.PlatinumMedallionText+" " + AppConstants.passengerText+": "+total;
				}
				else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.GoldMedallionValue))
				{
					result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.GoldMedallionText+" " + AppConstants.passengerText+": "+total;
				}
				else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.SilverMedallionValue))
				{
					result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.SilverMedallionText+" " + AppConstants.passengerText+": "+total;
				}
				else if(seatAnalyticsRequest.getSeatCriteria().equals(AppConstants.SeatEngineSearchCriteriaType.BaseMedallionValue))
				{
					result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.BaseMedallionText+" " + AppConstants.passengerText+": "+total;
				}
				seatAnalyticsResponse.setShowTransaction(total>0);
		    	seatAnalyticsResponse.setMessage(result);
	    	}
	    	
	    }
	    catch(Exception ex)
	    {
	    	ex.printStackTrace();
	    }

    	return seatAnalyticsResponse;
    }
	 
	 @Override
	 public List<SeatAnalyticsSengEvtMsg> generateSeatAnalyticsEvtMsgList(List<SeatAnalytics> seatEvtMsgList)
	 {
		 List<SeatAnalyticsSengEvtMsg> dataList = new ArrayList<SeatAnalyticsSengEvtMsg>();
		 try
		 {
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 
				 SeatAnalyticsSengEvtMsg seatAnalyticsSengEvtMsg = new SeatAnalyticsSengEvtMsg();
				 seatAnalyticsSengEvtMsg.setTransactionId(seatEvtMsgList.get(i).getTransactionId());
				 seatAnalyticsSengEvtMsg.setFlightNo(seatEvtMsgList.get(i).getFlightNo());
				 seatAnalyticsSengEvtMsg.setOrigin(seatEvtMsgList.get(i).getFlightOrigin());
				 seatAnalyticsSengEvtMsg.setDestination(seatEvtMsgList.get(i).getFlightDestination());
				 
				 ObjectMapper mapper = new ObjectMapper();
			     
	            JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getRequestMessage());
	            
	            if(obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments) != null && obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0) != null)
	            {
	            	if(obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.scheduledDepartureLocalDate) != null)
	            	{
	            		String date = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.scheduledDepartureLocalDate).asText();
		            	String time = "";
		            	if(obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.flightDepartureTime) != null)
		            		time = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.flightDepartureTime).asText();
		            	seatAnalyticsSengEvtMsg.setDepartureDate(date+" "+time);
	            	}
	            	
	            	if(obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers) != null)
	            	{
	            		JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
	            		int offerSize = offers.size();
	            		int totalPassengers = 0;
	            		for(int j=0; j<offerSize;j++)
	            		{
	            			if(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers) != null)
	            			{
	            				totalPassengers += this.calculateTotalPassenger(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers));
	            			}
	            		}
	            		seatAnalyticsSengEvtMsg.setTotalPNR(offerSize);
	            		seatAnalyticsSengEvtMsg.setTotalPassenger(totalPassengers);
	            	}
	            }
	            dataList.add(seatAnalyticsSengEvtMsg);
			 } 
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
		 return dataList;
	 }
	 
	 @Override
	 public void processResponseAndUnseatedPaxPNRList(List<SeatAnalytics> seatEvtMsgList)
	 {
		 List<String> responsePNRList = new ArrayList<String>();
		 List<String> unseatedPaxPNRList = new ArrayList<String>();
		 try
		 {
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 ObjectMapper mapper = new ObjectMapper();
			     
	            JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getResponseMessage());
	            int seatSelectionSize = 0;
	            JsonNode seatSelections = null;
	            if(obj.get(AppConstants.FlightInfoJSONFieldText.flightSegmentOutputs) != null && obj.get(AppConstants.FlightInfoJSONFieldText.flightSegmentOutputs).get(0) != null && obj.get(AppConstants.FlightInfoJSONFieldText.flightSegmentOutputs).get(0).get(AppConstants.FlightInfoJSONFieldText.seatSelections) != null)
	            {
	            	seatSelections = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegmentOutputs).get(0).get(AppConstants.FlightInfoJSONFieldText.seatSelections);
	            	seatSelectionSize = seatSelections.size();
	            }
	            
	            responsePNRList = new ArrayList<String>();
	   		    unseatedPaxPNRList = new ArrayList<String>();
	            for(int j=0; j<seatSelectionSize;j++)
	            {
	            	responsePNRList.add(seatSelections.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString());
	            	if(seatSelections.get(j).get(AppConstants.FlightInfoJSONFieldText.isAssigedByTPFText) == null || seatSelections.get(j).get(AppConstants.FlightInfoJSONFieldText.isAssigedByTPFText).toString().equals("null") 
	            			|| seatSelections.get(j).get(AppConstants.FlightInfoJSONFieldText.isAssigedByTPFText).asText().equals("N"))
	            	{
	            		unseatedPaxPNRList.add(responsePNRList.get(j));
	            	}
	            }
	            //System.out.println(responsePNRList);
	            //System.out.println(unseatedPaxPNRList);
	            seatEvtMsgList.get(i).setResponsePNRList(responsePNRList);
	            seatEvtMsgList.get(i).setUnseatedPaxPNRList(unseatedPaxPNRList);
			 } 
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
	 }
	 
	 public int getTotalSinglePNRByRecommendationType(List<SeatAnalytics> seatEvtMsgList, int recommendationType)
	 {
		 int total = 0;
		 try
		 {
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 ObjectMapper mapper = new ObjectMapper();
				 JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getRequestMessage());
				 List<String> responsePnrList = seatEvtMsgList.get(i).getResponsePNRList();
				 List<String> unseatedPnrList = seatEvtMsgList.get(i).getUnseatedPaxPNRList();
		         JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
		         int offerSize = offers.size();
		         for(int j=0;j<offerSize;j++)
		         {
		        	 if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 
		        		 if(this.calculateTotalPassenger(passengers) == 1)
			        	 {
			        		 total++;
			        	 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineValue && !responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 if(this.calculateTotalPassenger(passengers) == 1)
			        	 {
			        		 total++;
			        	 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfValue && unseatedPnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 if(this.calculateTotalPassenger(passengers) == 1)
			        	 {
			        		 total++;
			        	 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnseatedPaxValue)
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
			        	 if(this.calculateTotalPassenger(passengers)==1)
			        	 {
			        		 
			        		 int passengerSize = passengers != null ? passengers.size() : 0;
			        		 boolean hasUnseatedPassenger = false;
			        		 for(int l=0;l<passengerSize;l++)
			        		 {
			        			 JsonNode passenger = passengers.get(l);
			        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
			        			 {
			        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
					        		 {
					        			 hasUnseatedPassenger = true;
					        		 }
			        			 }
			        		 }
			        		 if(hasUnseatedPassenger)
			        			 total++;
			        	 }
		        	 }
		        	 
		        	 else  if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 int passengerCount = 0;
		        		 for(int k=0;k<passengerSize;k++)
		        		 {
		        			 JsonNode passenger = passengers.get(k);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		            		 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
		        				 {
		        					 passengerCount++;
		        				 }
		            		 }
		        		 }
			        	 if(passengerCount == 1)
			        	 {
			        		 total++;
			        	 }
		        	 }
		        		 
		         }
				 
			 }
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
		 return total;
	 }
	 
	 public int calculateTotalPassenger(JsonNode passengers)
	 {
		 int passengerCount = 0;
		 if(passengers == null)
			 return passengerCount;
		 int passengerSize = passengers.size();
		 for(int k=0;k<passengerSize;k++)
		 {
			 JsonNode passenger = passengers.get(k);
			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
    		 {
				 passengerCount++;
    		 }
		 }
         return passengerCount;
	 }
	 
	 public void getTotalMultiPNRByRecommendationType(List<SeatAnalytics> seatEvtMsgList, int recommendationType,SeatAnalyticsResponse seatAnalyticsResponse)
	 {
		 int total = 0;
		 int totalPassenger = 0;
		 try
		 {
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 ObjectMapper mapper = new ObjectMapper();
				 JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getRequestMessage());
				 List<String> responsePnrList = seatEvtMsgList.get(i).getResponsePNRList();
				 List<String> unseatedPnrList = seatEvtMsgList.get(i).getUnseatedPaxPNRList();
		         JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
		         int offerSize = offers.size();
		         for(int j=0;j<offerSize;j++)
		         {
		        	 if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerCount = this.calculateTotalPassenger(passengers); 
			        	 if(passengerCount > 1)
			        	 {
			        		 total++;
			        		 totalPassenger += passengerCount;
			        	 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineValue && !responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerCount = this.calculateTotalPassenger(passengers); 
			        	 if(passengerCount > 1)
			        	 {
			        		 total++;
			        		 totalPassenger += passengerCount;
			        	 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfValue && unseatedPnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerCount = this.calculateTotalPassenger(passengers); 
			        	 if(passengerCount > 1)
			        	 {
			        		 total++;
			        		 totalPassenger += passengerCount;
			        	 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnseatedPaxValue)
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
			        	 if(this.calculateTotalPassenger(passengers)>1)
			        	 {
			        		 int passengerSize = passengers != null ? passengers.size() : 0;
			        		 boolean hasUnseatedPassenger = false;
			        		 for(int l=0;l<passengerSize;l++)
			        		 {
			        			 JsonNode passenger = passengers.get(l);
			        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
			        			 {
			        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
					        		 {
					        			 totalPassenger++;
					        			 hasUnseatedPassenger = true;
					        		 }
			        			 }
			        		 }
			        		 if(hasUnseatedPassenger)
			        			 total++;
			        		 
			        	 }
		        	 }
		        	 else  if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 int passengerCount = 0;
		        		 for(int k=0;k<passengerSize;k++)
		        		 {
		        			 JsonNode passenger = passengers.get(k);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		            		 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
		        				 {
		        					 passengerCount++;
		        				 }
		            		 }
		        		 }
			        	 if(passengerCount > 1)
			        	 {
			        		 total++;
			        		 totalPassenger += passengerCount;
			        	 }
		        	 }
		         }
				 
			 }
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
		 String result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.MultiPartyPNRText+": "
		          + total+" , "+AppConstants.TotalText + " " + AppConstants.passengerText+": "+totalPassenger;
		 seatAnalyticsResponse.setMessage(result);
		 seatAnalyticsResponse.setShowTransaction(total>0);
	 }
	 
	 public void getTotalFamilyPNRByRecommendationType(List<SeatAnalytics> seatEvtMsgList, int recommendationType,SeatAnalyticsResponse seatAnalyticsResponse)
	 {
		 int total = 0;
		 int totalPassenger = 0;
		 try
		 {
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 ObjectMapper mapper = new ObjectMapper();
				 JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getRequestMessage());
				 List<String> responsePnrList = seatEvtMsgList.get(i).getResponsePNRList();
				 List<String> unseatedPnrList = seatEvtMsgList.get(i).getUnseatedPaxPNRList();
		         JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
		         int offerSize = offers.size();
		         for(int j=0;j<offerSize;j++)
		         {
		        	 if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 boolean hasFamilyPnr = false;
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 if(passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange) != null && passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.familyPnrRangeText))
		        			 {
		        				 hasFamilyPnr = true;
		        				 totalPassenger++;
		        			 }
		        		 }
		        		 if(hasFamilyPnr)
		        			 total++;
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineValue && !responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 boolean hasFamilyPnr = false;
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 if(passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange) != null && passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.familyPnrRangeText))
		        			 {
		        				 hasFamilyPnr = true;
		        				 totalPassenger++;
		        			 }
		        		 }
		        		 if(hasFamilyPnr)
		        			 total++;
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfValue && unseatedPnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 boolean hasFamilyPnr = false;
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 if(passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange) != null && passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.familyPnrRangeText))
		        			 {
		        				 hasFamilyPnr = true;
		        				 totalPassenger++;
		        			 }
		        		 }
		        		 if(hasFamilyPnr)
		        			 total++;
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnseatedPaxValue)
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers != null ? passengers.size() : 0;

		        		 boolean hasUnseatedPassenger = false;
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange) != null && passengers.get(l).get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.familyPnrRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
				        		 {
				        			 totalPassenger++;
				        			 hasUnseatedPassenger = true;
				        		 } 
		        			 }
		        		 }
		        		 if(hasUnseatedPassenger)
		        			 total++;
		        	 }
		        	 
		        	 else  if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 int passengerCount = 0;
		        		 for(int k=0;k<passengerSize;k++)
		        		 {
		        			 JsonNode passenger = passengers.get(k);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) != null && passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.familyPnrRangeText))
		            		 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
		        				 {
		        					 passengerCount++;
		        				 }
		            		 }
		        		 }
			        	 if(passengerCount > 0)
			        	 {
			        		 total++;
			        		 totalPassenger += passengerCount;
			        	 }
		        	 }
		        	 
		         }
				 
			 }
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
		 String result = AppConstants.TotalText + " " +AppConstants.SeatEngineSearchCriteriaType.FamilyPNRsText+": "
 				+total+" , "+AppConstants.TotalText + " " + AppConstants.passengerText+": "+totalPassenger;
		 seatAnalyticsResponse.setMessage(result);
		 seatAnalyticsResponse.setShowTransaction(total>0);
	 }
	 
	 public int getTotalMedalationByMedalationType(List<SeatAnalytics> seatEvtMsgList, SeatAnalyticsRequest seatAnalyticsRequest)
	 {
		 int total = 0;
		 try
		 {
			 int recommendationType = Integer.parseInt(seatAnalyticsRequest.getSeatRecommendation());
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 ObjectMapper mapper = new ObjectMapper();
				 JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getRequestMessage());
				 List<String> responsePnrList = seatEvtMsgList.get(i).getResponsePNRList();
				 List<String> unseatedPnrList = seatEvtMsgList.get(i).getUnseatedPaxPNRList();
		         JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
		         int offerSize = offers.size();
		         for(int j=0;j<offerSize;j++)
		         {
		        	 if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) != null && passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).asText().equals(seatAnalyticsRequest.getSeatCriteria()))
			        			 {
			        				 total++;
			        			 }
		        			 }
		        			 
		        			 
		        		 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineValue && !responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) != null && passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).asText().equals(seatAnalyticsRequest.getSeatCriteria()))
			        			 {
			        				 total++;
			        			 }
		        			 }
		        		 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfValue && unseatedPnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) != null && passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).asText().equals(seatAnalyticsRequest.getSeatCriteria()))
			        			 {
			        				 total++;
			        			 }
		        			 }
		        		 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnseatedPaxValue)
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) != null && passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).asText().equals(seatAnalyticsRequest.getSeatCriteria()))
			        			 {
			        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
					        		 {
					        			 total++;
					        		 } 
			        			 }
		        			 }
		        			
		        		 }
		        	 }
		        	 else  if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int k=0;k<passengerSize;k++)
		        		 {
		        			 JsonNode passenger = passengers.get(k);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) != null && passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).asText().equals(seatAnalyticsRequest.getSeatCriteria()))
			        			 {
			        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
					        		 {
					        			 total++;
					        		 } 
			        			 }
		        			 }
		        		 }
		        	 }
		         }
				 
			 }
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
		 return total;
	 }
	 public int getNonLoyalityProgramTireCodePassengers(List<SeatAnalytics> seatEvtMsgList, SeatAnalyticsRequest seatAnalyticsRequest)
	 {
		 int total = 0;
		 try
		 {
			 int recommendationType = Integer.parseInt(seatAnalyticsRequest.getSeatRecommendation());
			 int size = seatEvtMsgList.size();
			 for(int i=0;i<size;i++)
			 {
				 ObjectMapper mapper = new ObjectMapper();
				 JsonNode obj = mapper.readTree(seatEvtMsgList.get(i).getRequestMessage());
				 List<String> responsePnrList = seatEvtMsgList.get(i).getResponsePNRList();
				 List<String> unseatedPnrList = seatEvtMsgList.get(i).getUnseatedPaxPNRList();
		         JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
		         int offerSize = offers.size();
		         for(int j=0;j<offerSize;j++)
		         {
		        	 if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).toString().equalsIgnoreCase("null"))
			        			 {
			        				 total++;
			        			 }
		        			 }
		        			 
		        		 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineValue && !responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).toString().equalsIgnoreCase("null"))
			        			 {
			        				 total++;
			        			 }
		        			 }
		        		 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfValue && unseatedPnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).toString().equalsIgnoreCase("null"))
			        			 {
			        				 total++;
			        			 }
		        			 }
		        		 }
		        	 }
		        	 else if(recommendationType == AppConstants.SeatEngineSearchCriteria.UnseatedPaxValue)
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int l=0;l<passengerSize;l++)
		        		 {
		        			 JsonNode passenger = passengers.get(l);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).toString().equalsIgnoreCase("null"))
			        			 {
		        					 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
					        		 {
					        			 total++;
					        		 }
			        			 }
		        			 }
		        		 }
		        	 }
		        	 else  if(recommendationType == AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXValue && responsePnrList.contains(offers.get(j).get(AppConstants.FlightInfoJSONFieldText.recordLocatorId).toString()))
		        	 {
		        		 JsonNode passengers = offers.get(j).get(AppConstants.FlightInfoJSONFieldText.passengers);
		        		 int passengerSize = passengers.size();
		        		 for(int k=0;k<passengerSize;k++)
		        		 {
		        			 JsonNode passenger = passengers.get(k);
		        			 if(passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange) == null || !passenger.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		        			 {
		        				 if(passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).toString().equalsIgnoreCase("null"))
			        			 {
		        					 if(passenger.get(AppConstants.FlightInfoJSONFieldText.seatId) == null || passenger.get(AppConstants.FlightInfoJSONFieldText.seatId).toString().equals("null"))
					        		 {
					        			 total++;
					        		 }
			        			 }
		        			 }
		        		 }
		        	 }
		         }
				 
			 }
		 }
		 catch (Exception e) 
		 {
			 e.printStackTrace();
		 }
		 return total;
	 }
	 public List<Passenger> getPassengersByTransactionId(String transactionId)
	 {
		 List<Passenger> passengerList = new ArrayList<Passenger>();
		 try 
		 {
			SeatEvtMsg seatEvtMsg =  seatEngineEvtMsgDao.getSeatEvgMsgBasedOntransactionId(transactionId);
			if(seatEvtMsg != null)
			{
				 ObjectMapper mapper = new ObjectMapper();
			     
		          JsonNode obj = mapper.readTree(seatEvtMsg.getRequestClob());
		            
		            if(obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments) != null 
		            		&& obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0) != null
		            		&& obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers) != null)
		            {
		            	JsonNode offers = obj.get(AppConstants.FlightInfoJSONFieldText.flightSegments).get(0).get(AppConstants.FlightInfoJSONFieldText.offers);
		            	int offerSize = offers.size();
		            	for(int i=0;i<offerSize;i++)
		            	{
		            		JsonNode passengers = offers.get(i).get(AppConstants.FlightInfoJSONFieldText.passengers);
		            		int passengerSize = passengers.size();
		            		for(int j=0;j<passengerSize;j++)
		            		{
		            			Passenger passenger = new Passenger();
		            			JsonNode passengerObj = passengers.get(j);
		            			
		            			if(passengerObj.get(AppConstants.FlightInfoJSONFieldText.AgeRange) != null && passengerObj.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText().equals(AppConstants.infantAgeRangeText))
		            				continue;
			        			 
		            			passenger.setFirstNameNum(passengerObj.get(AppConstants.FlightInfoJSONFieldText.firstNameNum) != null ? passengerObj.get(AppConstants.FlightInfoJSONFieldText.firstNameNum).asText() : "");
		            			passenger.setLastNameNum(passengerObj.get(AppConstants.FlightInfoJSONFieldText.lastNameNum) != null ? passengerObj.get(AppConstants.FlightInfoJSONFieldText.lastNameNum).asText() : "");
		            			passenger.setTravelerId(passengerObj.get(AppConstants.FlightInfoJSONFieldText.travelerId) != null ? passengerObj.get(AppConstants.FlightInfoJSONFieldText.travelerId).asText() :"");
		            			passenger.setLoyaltyProgramTierCode(passengerObj.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode) != null ?passengerObj.get(AppConstants.FlightInfoJSONFieldText.loyaltyProgramTierCode).asText() : "");
		            			passenger.setAgeRangeText(passengerObj.get(AppConstants.FlightInfoJSONFieldText.AgeRange) !=null ? passengerObj.get(AppConstants.FlightInfoJSONFieldText.AgeRange).asText() : "");
		            			passenger.setSeatId(passengerObj.get(AppConstants.FlightInfoJSONFieldText.seatId) != null ? passengerObj.get(AppConstants.FlightInfoJSONFieldText.seatId).asText(): "");
		            			List<Ssr> ssrList = new ArrayList<Ssr>();
		            			if(passengerObj.get(AppConstants.FlightInfoJSONFieldText.ssrs) != null)
		            			{
		            			   JsonNode ssrs = 	passengerObj.get(AppConstants.FlightInfoJSONFieldText.ssrs);
		            			   int ssrSize = ssrs.size();
		            			   for(int l=0;l<ssrSize;l++)
		            			   {
		            				   Ssr ssr = new Ssr();
		            				   ssr.setSsrCode(ssrs.get(l).get(AppConstants.FlightInfoJSONFieldText.ssrCode) != null ? ssrs.get(l).get(AppConstants.FlightInfoJSONFieldText.ssrCode).asText() : "");
		            				   ssrList.add(ssr);
		            			   }
		            			}
		            			passenger.setSsrs(ssrList);
		            			
		            			List<LoyaltyMemberPreference> loyaltyMemberPreferences = new ArrayList<LoyaltyMemberPreference>();
		            			if(passengerObj.get(AppConstants.FlightInfoJSONFieldText.loyaltyMemberPreferences) != null)
		            			{
		            			   JsonNode preferenceObj = 	passengerObj.get(AppConstants.FlightInfoJSONFieldText.loyaltyMemberPreferences);
		            			   int preferenceObjSize = preferenceObj.size();
		            			   for(int l=0;l<preferenceObjSize;l++)
		            			   {
		            				   LoyaltyMemberPreference loyaltyMemberPreference = new LoyaltyMemberPreference();
		            				   loyaltyMemberPreference.setPreferenceCode(preferenceObj.get(l).get(AppConstants.FlightInfoJSONFieldText.preferenceCode) != null ? preferenceObj.get(l).get(AppConstants.FlightInfoJSONFieldText.preferenceCode).asText() : "");
		            				   loyaltyMemberPreference.setPreferenceCodeDesc(preferenceObj.get(l).get(AppConstants.FlightInfoJSONFieldText.preferenceCodeDesc) != null ? preferenceObj.get(l).get(AppConstants.FlightInfoJSONFieldText.preferenceCodeDesc).asText() : "");
		            				   loyaltyMemberPreferences.add(loyaltyMemberPreference);
		            			   }
		            			}
		            			passenger.setLoyaltyMemberPreferences(loyaltyMemberPreferences);
		            			
		            			passengerList.add(passenger);
		            			
		            		}
		            	}
		            }				 
		       } 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return passengerList;
	 }
	
}
