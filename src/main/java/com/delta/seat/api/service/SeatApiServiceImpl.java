package com.delta.seat.api.service;

import com.dashoptimization.XPRMModel;
import com.dashoptimization.XPRMValue;
import com.delta.seat.api.common.AlphaNumericComperator;
import com.delta.seat.api.dao.SeatEngineEvtMsgDao;
import com.delta.seat.api.dao.SeatEvtMsg;
import com.delta.seat.api.dao.SeatTransaction;
import com.delta.seat.api.util.SeatUtil;
import com.delta.seat.api.vo.*;
import com.delta.seat.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.dashoptimization.XPRM;
import org.springframework.web.client.HttpClientErrorException;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static com.delta.seat.constants.SeatEngineConstants.PREFERENCE_AISLE_CODE;
import static com.delta.seat.constants.SeatEngineConstants.PREFERENCE_WINDOW_CODE;

public class SeatApiServiceImpl implements SeatApiService{

    private static final Logger log = LogManager.getLogger(SeatApiServiceImpl.class);

    @Autowired
    private SeatServiceDelegator seatServiceDelegator;

    @Autowired
    private SeatEngineEvtMsgDao seatEngineEvtMsgDao;

    private String jsonParamFileLocation = null;
    private String modelCompiledFileLocation = null;
    private String moselOutPutLogFile = null;
    private String batchJobId = null;

    @Override
    public SeatSelectionOutput getSeatSelection(SeatSelectionInput seatSelectionRequest, String transactionid) {

        seatServiceDelegator.checkValuesInRequestAndIgnore(seatSelectionRequest, transactionid);

        List<FlightSegmentInput> flightSegmentInputList = seatSelectionRequest.getFlightSegments();

        SeatSelectionOutput seatSelectionResponse = new SeatSelectionOutput();
        seatSelectionResponse.setTransactionId(transactionid);
        List<FlightSegmentOutput> flightInfoResponseList = new ArrayList<FlightSegmentOutput>();

        FlightSegmentOutput flightInfoResponse = null;
        for (FlightSegmentInput flightInfoRqst : flightSegmentInputList){
            flightInfoResponse = new FlightSegmentOutput();
            flightInfoResponse.setDestinationAirportCode(flightInfoRqst.getDestinationAirportCode());
            flightInfoResponse.setAssociationId(flightInfoRqst.getAssociationId());
            flightInfoResponse.setClassOfServiceCode(flightInfoRqst.getClassOfServiceCode());
            flightInfoResponse.setOriginAirportCode(flightInfoRqst.getOriginAirportCode());
            flightInfoResponse.setOperatingCarrierCode(flightInfoRqst.getOperatingCarrierCode());
            flightInfoResponse.setScheduledDepartureLocalDate(flightInfoRqst.getScheduledDepartureLocalDate());
            flightInfoResponse.setMarketingCarrierCode(flightInfoRqst.getMarketingCarrierCode());
            flightInfoResponse.setMarketingFlightNum(flightInfoRqst.getMarketingFlightNum());
            flightInfoResponse.setOperatingCarrierCode(flightInfoRqst.getOperatingCarrierCode());
            flightInfoResponse.setOperatingFlightNum(flightInfoRqst.getOperatingFlightNum());
            flightInfoResponse.setFlightSegmentNum(flightInfoRqst.getFlightSegmentNum());
            //Bereket wants it under seatselection
//            flightInfoResponse.setFlightIdRefText(flightInfoRqst.getFlightIdRefText());
            flightInfoResponse.setFlightDepartureTime(flightInfoRqst.getFlightDepartureTime());
            flightInfoResponse.setInboundFlightCode(flightInfoRqst.getInboundFlightCode());
            flightInfoResponse.setMarketedAsConnectionCarrierFlightDetailsText(flightInfoRqst.getMarketedAsConnectionCarrierFlightDetailsText());
            flightInfoResponse.setOperatingClassOfServiceCode(flightInfoRqst.getOperatingClassOfServiceCode());
            flightInfoResponse.setEquipmentCode(flightInfoRqst.getEquipmentCode());

            SeatMap seatMap = flightInfoRqst.getSeatMap();

            seatServiceDelegator.sortSeatMapData(seatMap, transactionid);

            List<Offer> offerList = flightInfoRqst.getOffers();

            //for MVP there will be at most onr PNR, offerid since they will call be calling seatengine one PNR at a time. So, take the first one from the list
            Offer offer = offerList.get(0);

            List<Passenger> passengerList = offer.getPassengers();
            List<SeatSelection> seatSelectionList = new ArrayList<SeatSelection>();
            SeatSelection seatSelection = null;
            for (Passenger passenger : passengerList){
                seatSelection = new SeatSelection();
                List<LoyaltyMemberPreference> loyaltyPreferenceList = passenger.getLoyaltyMemberPreferences();
                //for MVP there will be at most onr unique pnr, offerid and one associated  preference
                String preferenceCode = loyaltyPreferenceList.get(0).getPreferenceCode();
                String preferenceDesc = loyaltyPreferenceList.get(0).getPreferenceCodeDesc();
                List<Seatoffer> seatOfferList = passenger.getSeatOffers();

                Collections.sort(seatOfferList, new AlphaNumericComperator());
                    if (preferenceCode != null && !preferenceCode.isEmpty() &&
                            (preferenceCode.equalsIgnoreCase(PREFERENCE_AISLE_CODE) ||
                                    (preferenceCode.equalsIgnoreCase(PREFERENCE_WINDOW_CODE)))){

                        //for Aisle preference
                        if (preferenceCode.equalsIgnoreCase(PREFERENCE_AISLE_CODE)){
                            //check if Aisle is found
                            if (selectAisleSeatBasedOnAislePreference(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList)){
                                return seatSelectionResponse;
                            }
                            //Aisle wasn't found. So look for Window
                            else if (selectWindowSeatBasedOnWindowPreference(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList)){
                                return seatSelectionResponse;
                            }
                            //Aisle and Window seats weren't found. So, look for any seat(s)
                            else {
                                    return selectAnyAvailableMiddleSeatThatPaxIsEligible(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList);
                            }
                        }

                        //for Window preference
                        if (preferenceCode.equalsIgnoreCase(PREFERENCE_WINDOW_CODE)) {
                            //check if window is found
                            if (selectWindowSeatBasedOnWindowPreference(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList)){
                                return seatSelectionResponse;
                            }
                            //Window wasn't found. So, look for Aisle
                            else if (selectAisleSeatBasedOnAislePreference(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList)){
                                return seatSelectionResponse;
                            }
                            //Aisle and Window seats weren't found. So, look for any seat(s)
                            else {
                                return selectAnyAvailableMiddleSeatThatPaxIsEligible(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList);
                            }


                        }
                    }
                    //no preference. So, first find Aisle and then window in each row from front to back exhaustively and then find middle
                    else {
                        if (selectAisleWindowSeatNoPreferenceCase(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList)) {
                            return seatSelectionResponse;
                        }
                        else{
                            return selectAnyAvailableMiddleSeatThatPaxIsEligible(seatSelectionResponse, flightInfoResponseList, flightInfoResponse, seatMap, offer, seatSelectionList, seatSelection, passenger, preferenceCode, seatOfferList);
                        }
                    }
            }
        }

        return seatSelectionResponse;
    }

    @Override
    public SeatSelectionOutput getSeatSelectionUsingOptimizer(String seatEngineJsonRequest, String transactionid) {

        SeatSelectionOutput seatSelectionOutput = null;
        XPRM mosel = null;
        XPRMModel model = null;


        String parmJsonData = null;
        try {
            parmJsonData = SeatUtil.readAllBytesToString(jsonParamFileLocation);
            log.debug(String.format("For the transactionId[%s], jsonParamFileLocation[%s] and parmJsonData[%s]", transactionid, jsonParamFileLocation, parmJsonData));
        }catch (Exception ex){
            String errorMsg = String.format("For the transactionid[%s], problem reading file [%s]", transactionid, jsonParamFileLocation);
            log.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }

        ByteBuffer jsonInput =  null;
        try {
            jsonInput = ByteBuffer.wrap(seatEngineJsonRequest.getBytes());
        }catch (Exception e){
            String errorMsg = String.format("For the transactionId[%s], ByteBuffer.wrap threw exception for jsonInput!", transactionid);
            log.error(errorMsg);
            e.printStackTrace();
            throw new RuntimeException (errorMsg);
        }

        ByteBuffer parmJsonInput =  null;
        try {
            parmJsonInput = ByteBuffer.wrap(parmJsonData.getBytes());
        }catch (Exception e){
            String errorMsg = String.format("For the transactionId[%s], ByteBuffer.wrap threw exception for parmJsonInput!", transactionid);
            log.error(errorMsg);
            e.printStackTrace();
            throw new RuntimeException (errorMsg);
        }

        if (jsonInput != null ) {
            try {
                mosel = new XPRM(); // Initialize Mosel
                log.debug(String.format("For the transactionId[%s], modelCompiledFileLocation[%s]", transactionid, modelCompiledFileLocation));
                model = mosel.loadModel(modelCompiledFileLocation); // Load compiled mosel .bim file
                String moselLogFileWithLocation = moselOutPutLogFile + File.separatorChar + transactionid + "_log.txt";
                log.debug(String.format("For the transactionId[%s], moselLogFileWithLocation[%s]", transactionid, moselLogFileWithLocation));

                long startTime = System.currentTimeMillis();
                model.bind("jsonInput", jsonInput);
                model.bind("parmJsonInput", parmJsonInput);
                //pass parameters, json reques and logfile to mosel model
                model.execParams = "PARMFILE='java:parmJsonInput'" + "',"
                        + "DATAFILE='java:jsonInput'" + ","
                        + "LOGFILE='" + moselLogFileWithLocation + "'";
                try {
                    // Run the model
                    model.run();
                    log.info(String.format("For the transactionId[%s], total optimizer execution time [%s] mses. ", transactionid, (System.currentTimeMillis() - startTime)));
                    //if model gets executed fine then get the json response and send it along
                    if (model.getExitCode() == 0) {
                        log.info(String.format("For the transactionId[%s], mosel model executed fine with objective value [%s]: ", transactionid, model.getObjectiveValue()));
                        XPRMValue xprmValue = (XPRMValue) model.findIdentifier("jsonOutputStr");
                        if (xprmValue != null) {
                            String jsonOutPutResponseFromMoselModel = xprmValue.asString();
                            log.info(String.format("For the transactionId [%s], json response from mosel model [%s] and total optimizer execution time + intercepting json output [%s] msecs", transactionid, jsonOutPutResponseFromMoselModel, (System.currentTimeMillis() - startTime)));
                            try {
                                seatSelectionOutput = (SeatSelectionOutput) StringUtils.getJsonObjectFromString(jsonOutPutResponseFromMoselModel, SeatSelectionOutput.class);

                            } catch (Exception ex) {
                                //conversion from mosel json response to seat engine api wasn't ok
                                throw new RuntimeException(String.format("For the transactionId[%s], mosel json response to the seat engine object translation wasn't correct!!", transactionid));

                            }
                            return seatSelectionOutput;

                        } else {
                            //return empty seat selection output
                            return seatSelectionOutput;
                        }
                    } else if (model.getExitCode() >= 1) {
                        String errorMsg = String.format("For the transactionId [%s], mosel model didn't execute properly and exit code [%s] !", transactionid, model.getExitCode());
                        log.error(errorMsg);
                        throw new RuntimeException(errorMsg);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    String errorMsg = String.format("For the transactionid[%s], problem running FICO Mosel model.", transactionid);
                    log.error(errorMsg);
                    throw new RuntimeException(errorMsg);
                } finally {
                    //resetting everything so that model can be executed for the next run
                    model.reset();
                }

            } catch (IOException e) {
                e.printStackTrace();
                String errorMsg = String.format("For the transactionid[%s], problem loading mosel model file [%s]", transactionid, modelCompiledFileLocation);
                log.error(errorMsg);
                throw new RuntimeException(errorMsg);

            }
        }

        //clear the buffer
        if (parmJsonInput != null){
            parmJsonInput.clear();
        }

        if (jsonInput != null){
            jsonInput.clear();
        }

        return seatSelectionOutput;
    }

    private SeatSelectionOutput selectAnyAvailableMiddleSeatThatPaxIsEligible(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, SeatMap seatMap, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, String preferenceCode, List<Seatoffer> seatofferList) {
        List<Compartment> cabinList = seatMap.getCompartments();
        //first try to pick preferred middle seat
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList){
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;
                    for (Seatoffer seatoffer : seatofferList) {
                        if (seatoffer == null)
                            continue;
                        //only take offer that is 0.00 and seatId matches
                        if (seatoffer.getPaymentAmt() != null && new Double(seatoffer.getPaymentAmt()).doubleValue() == 0.00
                                && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true")
                                 && seatInfo.getMiddle() != null && seatInfo.getMiddle()
                                 && (seatInfo.getPreferredText() !=null && seatInfo.getPreferredText().equalsIgnoreCase("true"))
                                 && seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId())) {

                                seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                                seatSelection.setLastNameNum(passenger.getLastNameNum());
                                seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                                seatSelection.setOfferId(offer.getOfferId());
                                seatSelection.setTravelerId(passenger.getTravelerId());
                                seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                                seatSelectionList.add(seatSelection);

                                flightInfoResponse.setSeatSelections(seatSelectionList);
                                flightInfoResponseList.add(flightInfoResponse);
                                SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                                //found the seat.
                                return SeatSelectionOutput;
                        }

                    }
                }
            }
        }

        //then try to pick non-preferred middle seat
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList){
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;
                    for (Seatoffer seatoffer : seatofferList) {
                        if (seatoffer == null)
                            continue;;
                        //only take offer that is 0.00 and seatId matches
                        if (seatoffer.getPaymentAmt() != null && new Double(seatoffer.getPaymentAmt()).doubleValue() == 0.00
                                && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true")
                                && seatInfo.getMiddle() != null && seatInfo.getMiddle()
                                && (seatInfo.getPreferredText() !=null && !seatInfo.getPreferredText().equalsIgnoreCase("true") || seatInfo.getPreferredText() ==null )
                                && seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId())) {

                            seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                            seatSelection.setLastNameNum(passenger.getLastNameNum());
                            seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                            seatSelection.setOfferId(offer.getOfferId());
                            seatSelection.setTravelerId(passenger.getTravelerId());
                            seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                            seatSelectionList.add(seatSelection);

                            flightInfoResponse.setSeatSelections(seatSelectionList);
                            flightInfoResponseList.add(flightInfoResponse);
                            SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                            //found the seat.
                            return SeatSelectionOutput;
                        }

                    }
                }
            }
        }

        return SeatSelectionOutput;
    }

    private boolean selectWindowSeatBasedOnWindowPreference(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, SeatMap seatMap, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, String preferenceCode, Seatoffer seatoffer) {
        List<Compartment> cabinList = seatMap.getCompartments();
        for (Compartment cabin : cabinList) {
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;
                    //select preferred Window
                    if (seatInfo.getWindow() && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true")
                            && seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId()) && seatInfo.getPreferredText().equalsIgnoreCase("true")) {

                        seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                        seatSelection.setLastNameNum(passenger.getLastNameNum());
                        seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                        seatSelection.setOfferId(offer.getOfferId());
                        seatSelection.setTravelerId(passenger.getTravelerId());
                        seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                        seatSelectionList.add(seatSelection);

                        flightInfoResponse.setSeatSelections(seatSelectionList);
                        flightInfoResponseList.add(flightInfoResponse);
                        SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                        //found the seat.
                        return true;

                    }
                    //select non-preferred Window
                    else if (seatInfo.getWindow() && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true")
                            && seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId()) && !seatInfo.getPreferredText().equalsIgnoreCase("true")) {

                        seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                        seatSelection.setLastNameNum(passenger.getLastNameNum());
                        seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                        seatSelection.setOfferId(offer.getOfferId());
                        seatSelection.setTravelerId(passenger.getTravelerId());
                        seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                        seatSelectionList.add(seatSelection);

                        flightInfoResponse.setSeatSelections(seatSelectionList);
                        flightInfoResponseList.add(flightInfoResponse);
                        SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                        //found the seat.
                        return true;

                    }

                }
            }
        }
        return false;
    }

    private boolean selectAisleSeatBasedOnAislePreference(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, SeatMap seatMap, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, String preferenceCode, List<Seatoffer> seatofferList) {
        List<Compartment> cabinList = seatMap.getCompartments();
        //try to find preferred Aisle
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;
                    if (selectPreferredAisle(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seatInfo))
                        return true;


                }
            }
        }

        // try to find non-preferred Aisle
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;
                    if (selectNonPreferredAisle(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seatInfo))
                        return true;

                }
            }
        }

        return false;
    }

    private boolean selectNonPreferredAisle(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, List<Seatoffer> seatofferList, Seat seatInfo) {
        //select non-preferred Aisle
        if (seatInfo.getAisle() != null && seatInfo.getAisle() && seatInfo.getSeatAvailableCode() != null && seatofferList!= null && seatofferList.size() >=1 && seatInfo.getSeatId() != null
                && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true")
                && seatInfo.getPreferredText() != null && !seatInfo.getPreferredText().equalsIgnoreCase("true")){
            for (Seatoffer seatoffer : seatofferList) {
                if (seatoffer == null)
                    continue;
                //only take offer that is 0.00 and seatId matches
                if (seatoffer.getPaymentAmt() != null && new Double(seatoffer.getPaymentAmt()).doubleValue() == 0.00 &&
                        seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId())) {
                    seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                    seatSelection.setLastNameNum(passenger.getLastNameNum());
                    seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                    seatSelection.setOfferId(offer.getOfferId());
                    seatSelection.setTravelerId(passenger.getTravelerId());
                    seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                    seatSelectionList.add(seatSelection);

                    flightInfoResponse.setSeatSelections(seatSelectionList);
                    flightInfoResponseList.add(flightInfoResponse);
                    SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                    //found the seat.
                    return true;
                }
            }

        }
        return false;
    }

    private boolean selectPreferredAisle(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, List<Seatoffer> seatofferList, Seat seatInfo) {
        //select preferred Aisle
        if (seatInfo.getAisle() != null && seatInfo.getAisle() && seatInfo.getSeatAvailableCode() != null && seatofferList!= null && seatofferList.size() >=1 && seatInfo.getSeatId() != null
                && seatInfo.getPreferredText() != null && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true") && seatInfo.getPreferredText().equalsIgnoreCase("true")){

            for (Seatoffer seatoffer : seatofferList) {
                if (seatoffer == null)
                    continue;
                //only take offer that is 0.00 and seatId matches
                if ( new Double(seatoffer.getPaymentAmt()).doubleValue() == 0.00 &&
                        seatoffer.getSeatId() != null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId())) {
                    seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                    seatSelection.setLastNameNum(passenger.getLastNameNum());
                    seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                    seatSelection.setOfferId(offer.getOfferId());
                    seatSelection.setTravelerId(passenger.getTravelerId());
                    seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                    seatSelectionList.add(seatSelection);

                    flightInfoResponse.setSeatSelections(seatSelectionList);
                    flightInfoResponseList.add(flightInfoResponse);
                    SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                    //found the seat.
                    return true;
                }
            }

        }
        return false;
    }

    private boolean selectWindowSeatBasedOnWindowPreference(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, SeatMap seatMap, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, String preferenceCode, List<Seatoffer> seatofferList) {
        List<Compartment> cabinList = seatMap.getCompartments();
        //try to find preferred Window
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;

                    //select preferred window first
                    if (selectPreferredWindow(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seatInfo))
                        return true;
                }
            }
        }

        // try to find non-preferred Window next if preferred window wasn't found
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatInfoList = row.getSeats();
                for (Seat seatInfo : seatInfoList) {
                    if (seatInfo == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seatInfo.getExitRowSeat() != null && seatInfo.getExitRowSeat())
                        continue;
                    //select non-preferred Window
                    if (selectNonPreferredWindow(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seatInfo))
                        return true;
                }
            }
        }

        return false;
    }

    private boolean selectNonPreferredWindow(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, List<Seatoffer> seatofferList, Seat seatInfo) {
        if (seatInfo.getWindow() != null && seatInfo.getWindow() && seatInfo.getSeatAvailableCode() != null && seatofferList!= null && seatofferList.size() >=1 && seatInfo.getSeatId() != null
                && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true")
                && seatInfo.getPreferredText() != null && !seatInfo.getPreferredText().equalsIgnoreCase("true")){
            for (Seatoffer seatoffer : seatofferList) {
                if (seatoffer == null)
                    continue;
                //only take offer that is 0.00 and seatId matches
                if (seatoffer.getPaymentAmt() != null && new Double(seatoffer.getPaymentAmt()).doubleValue() == 0.00 &&
                        seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId())) {
                    seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                    seatSelection.setLastNameNum(passenger.getLastNameNum());
                    seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                    seatSelection.setOfferId(offer.getOfferId());
                    seatSelection.setTravelerId(passenger.getTravelerId());
                    seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                    seatSelectionList.add(seatSelection);

                    flightInfoResponse.setSeatSelections(seatSelectionList);
                    flightInfoResponseList.add(flightInfoResponse);
                    SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                    //found the seat.
                    return true;
                }
            }

        }
        return false;
    }

    private boolean selectPreferredWindow(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, List<Seatoffer> seatofferList, Seat seatInfo) {
        if (seatInfo.getWindow() != null && seatInfo.getWindow() && seatInfo.getSeatAvailableCode() != null && seatofferList!= null && seatofferList.size() >=1 && seatInfo.getSeatId() != null
                && seatInfo.getPreferredText() != null && seatInfo.getSeatAvailableCode() != null && seatInfo.getSeatAvailableCode().equalsIgnoreCase("true") && seatInfo.getPreferredText().equalsIgnoreCase("true")){

            for (Seatoffer seatoffer : seatofferList) {
                if (seatoffer == null)
                    continue;
                //only take offer that is 0.00 and seatId matches
                if ( new Double(seatoffer.getPaymentAmt()).doubleValue() == 0.00 &&
                        seatoffer.getSeatId()!= null && seatoffer.getSeatId().equalsIgnoreCase(seatInfo.getSeatId())) {
                    seatSelection.setFirstNameNum(passenger.getFirstNameNum());
                    seatSelection.setLastNameNum(passenger.getLastNameNum());
                    seatSelection.setSelectedSeatId(seatoffer.getSeatId());
                    seatSelection.setOfferId(offer.getOfferId());
                    seatSelection.setTravelerId(passenger.getTravelerId());
                    seatSelection.setOfferItemNum(seatoffer.getOfferItemNum());
                    seatSelectionList.add(seatSelection);

                    flightInfoResponse.setSeatSelections(seatSelectionList);
                    flightInfoResponseList.add(flightInfoResponse);
                    SeatSelectionOutput.setFlightSegmentOutputs(flightInfoResponseList);

                    //found the seat.
                    return true;
                }
            }

        }
        return false;
    }

    //No preference
    private boolean selectAisleWindowSeatNoPreferenceCase(SeatSelectionOutput SeatSelectionOutput, List<FlightSegmentOutput> flightInfoResponseList, FlightSegmentOutput flightInfoResponse, SeatMap seatMap, Offer offer, List<SeatSelection> seatSelectionList, SeatSelection seatSelection, Passenger passenger, String preferenceCode, List<Seatoffer> seatofferList) {
        List<Compartment> cabinList = seatMap.getCompartments();
        //try to find preferred Window
        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatList = row.getSeats();
                for (Seat seat : seatList) {
                    if (seat == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seat.getExitRowSeat() != null && seat.getExitRowSeat())
                        continue;

                    //select preferred aisle first - in same row
                    if (selectPreferredAisle(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seat))
                        return true;
                }
                for (Seat seat : seatList) {
                    if (seat == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seat.getExitRowSeat() != null && seat.getExitRowSeat())
                        continue;
                    //select preferred window second - in same row
                    if (selectPreferredWindow(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seat))
                        return true;
                }
            }
        }

        for (Compartment cabin : cabinList){
            if (cabin == null)
                continue;
            List<SeatRow> rowList = cabin.getSeatRows();
            for (SeatRow row : rowList) {
                if (row == null)
                    continue;
                List<Seat> seatList = row.getSeats();
                for (Seat seat : seatList) {
                    if (seat == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seat.getExitRowSeat() != null && seat.getExitRowSeat())
                        continue;

                    //select non-preferred aisle first - in same row
                    if (selectNonPreferredAisle(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seat))
                        return true;
                }

                for (Seat seat : seatList) {
                    if (seat == null)
                        continue;
                    //exit row seats are not considered for MVP
                    if (seat.getExitRowSeat() != null && seat.getExitRowSeat())
                        continue;
                    //select non-preferred window second - in same row
                    if (selectNonPreferredWindow(SeatSelectionOutput, flightInfoResponseList, flightInfoResponse, offer, seatSelectionList, seatSelection, passenger, seatofferList, seat))
                        return true;
                }
            }
        }

        return false;
    }

    @Override
    public SeatEvtMsg getSeatEvgMsgBasedOntransactionId(String transactionId) {
        // TODO Auto-generated method stub
        SeatEvtMsg seatEvtMsg = new SeatEvtMsg();
        try {
            seatEvtMsg = seatEngineEvtMsgDao.getSeatEvgMsgBasedOntransactionId(transactionId);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return seatEvtMsg;
    }

    @Override
    public List<SeatTransaction> getTransactionsByResourceLocatorId(TransactionSelection transactionSelection) {
        // TODO Auto-generated method stub
        return seatEngineEvtMsgDao.getTransactionsByResourceLocatorId(transactionSelection);
    }
    
    @Override
    public List<String> getSeatEngineEventType(){
        return seatEngineEvtMsgDao.getSeatEngineEventType();	
    }

    public void setSeatServiceDelegator(SeatServiceDelegator seatServiceDelegator) {
        this.seatServiceDelegator = seatServiceDelegator;
    }

    public void setJsonParamFileLocation(String jsonParamFileLocation) {
        this.jsonParamFileLocation = jsonParamFileLocation;
    }

    public void setModelCompiledFileLocation(String modelCompiledFileLocation) {
        this.modelCompiledFileLocation = modelCompiledFileLocation;
    }

    public void setMoselOutPutLogFile(String moselOutPutLogFile) {
        this.moselOutPutLogFile = moselOutPutLogFile;
    }

    public String getBatchJobId() {
        return batchJobId;
    }

    public void setBatchJobId(String batchJobId) {
        this.batchJobId = batchJobId;
    }
}
