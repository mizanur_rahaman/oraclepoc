package com.delta.seat.api.service;

import com.delta.seat.api.dao.SeatEvtMsg;
import com.delta.seat.api.dao.SeatTransaction;
import com.delta.seat.api.vo.SeatSelectionInput;
import com.delta.seat.api.vo.SeatSelectionOutput;
import com.delta.seat.api.vo.TransactionSelection;

import java.util.List;

public interface SeatApiService {

    public SeatSelectionOutput getSeatSelection(SeatSelectionInput seatSelectionInput, String transactionid);

    public SeatEvtMsg getSeatEvgMsgBasedOntransactionId(String transactionId);
    public List<SeatTransaction> getTransactionsByResourceLocatorId(TransactionSelection transactionSelection);

    public SeatSelectionOutput getSeatSelectionUsingOptimizer(String seatEngineJsonRequest, String transactionid);
    
    public List<String> getSeatEngineEventType();
}
