package com.delta.seat.api.service;

import com.delta.seat.api.selection.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class SeatingOptionImpl implements SeatingOption {
    private static final Logger log = LogManager.getLogger(SeatingOptionImpl.class);

    @Override
    public AdjacentOption[] createAdjacentOptions(FlightLeg fltLeg, String transactionId) {
        fltLeg.createAdjacentSeats();
        fltLeg.createAdjacentOptions(transactionId);
        List<AdjacentOption> adjOptList = new ArrayList<>();
        Cabin[] cabins = fltLeg.getCabins();
        for (int i = 0; i < cabins.length; i++) {
            AdjacentOption[] adjOptions = cabins[i].getAdjacentOption();
            if (adjOptions.length != 0) {
                for (int j = 0; j < adjOptions.length; j++) {
                    if (adjOptions[j].size() == 3) {
                        adjOptList.add(adjOptions[j]);
                    }
                }
            }
        }
        for (int i = 0; i < cabins.length; i++) {
            AdjacentOption[] adjOptions = cabins[i].getAdjacentOption();
            if (adjOptions.length != 0) {
                for (int j = 0; j < adjOptions.length; j++) {
                    if (adjOptions[j].size() == 2) {
                        adjOptList.add(adjOptions[j]);
                    }
                }
            }
        }
        AdjacentOption[] adjOptions = adjOptList.toArray(new AdjacentOption[0]);
        return adjOptions;
    }

    @Override
    public Option[] createOptions(FlightLeg fltLeg, Pnr pnr, String transactionId) {
        pnr.createSeatScores();
        pnr.debug(transactionId);
        pnr.createOptions(fltLeg, transactionId);
        return pnr.getOptions();
    }

    @Override
    public com.delta.seat.api.vo.SeatSelectionOutput selectSeats(com.delta.seat.api.vo.SeatSelectionInput voReq, String transactionId, boolean masterList) {
        Request req = new Request(voReq, transactionId);
        FlightLeg fltLeg = req.getFltLeg();
        com.delta.seat.api.vo.SeatSelectionOutput voSeatSelectionResp = new com.delta.seat.api.vo.SeatSelectionOutput();
        voSeatSelectionResp.setTransactionId(transactionId);

        List<com.delta.seat.api.vo.SeatSelection> voSeatSelectionList = new ArrayList<>();
        Pnr[] pnrs = req.getPnrs();
        long start = System.currentTimeMillis();
        if (pnrs.length == 1) {
            fltLeg.debugInventory(transactionId);
            if (pnrs[0].getPsgrs().length > 1)
                createAdjacentOptions(fltLeg, transactionId);
            createOptions(fltLeg, pnrs[0], transactionId);
            pnrs[0].debugOption(transactionId);
            log.info(transactionId + " - created " + pnrs[0].getOptions().length + " options for " + pnrs[0].getPnr() + " in " + (System.currentTimeMillis() - start) + " ms.");

            if (pnrs[0].selectOption(masterList) == null)
                return voSeatSelectionResp;
            List<com.delta.seat.api.vo.SeatSelection> voSelectionList = pnrs[0].getSeatSelectionListVO(voReq.getFlightSegments().get(0).getFlightIdRefText());
            voSeatSelectionList.addAll(voSelectionList);
            log.info(transactionId + " - " + pnrs[0].getPnr() + " selection: " + pnrs[0].getSelection());
        } else if (pnrs.length > 1) {    //multiple pnrs
            for (int i = 0; i < pnrs.length; i++) {
                fltLeg.debugInventory(transactionId);
                pnrs[i].refreshPsgrSeats();
                if (pnrs[i].getPsgrs().length > 1)
                    createAdjacentOptions(fltLeg, transactionId);
                createOptions(fltLeg, pnrs[i], transactionId);
                pnrs[i].debugOption(transactionId);
                log.info(transactionId + " - created " + pnrs[i].getOptions().length + " options for " + pnrs[i].getPnr() + " in " + (System.currentTimeMillis() - start) + " ms.");

                pnrs[i].selectOption(masterList);
                List<com.delta.seat.api.vo.SeatSelection> voSelectionList = pnrs[i].getSeatSelectionListVO(voReq.getFlightSegments().get(0).getFlightIdRefText());
                voSeatSelectionList.addAll(voSelectionList);
                pnrs[i].markSelection();
                log.info(transactionId + " - " + pnrs[i].getPnr() + " selection: " + pnrs[i].getSelection());
            }
        }
        if (voSeatSelectionList.size() != 0) {
            List<com.delta.seat.api.vo.FlightSegmentOutput> voFltInfoRespList = new ArrayList<>();
            voSeatSelectionResp.setFlightSegmentOutputs(voFltInfoRespList);
            com.delta.seat.api.vo.FlightSegmentOutput voFltInfoResp = fltLeg.getFlightInfoRspnsVO();
            voFltInfoRespList.add(voFltInfoResp);
            voFltInfoResp.setSeatSelections(voSeatSelectionList);
        }
        return voSeatSelectionResp;
    }
}
