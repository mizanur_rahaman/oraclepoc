package com.delta.seat.api.service;

import com.delta.seat.api.selection.AdjacentSeat;
import com.delta.seat.api.selection.Policy;
import com.delta.seat.api.selection.Seat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdjacentSeatPairsCalculatorImpl implements AdjacentSeatPairsCalculator {

    @Override
    public void createAdjacentSeatsWithScore(Seat[] seats, Policy policy, HashMap<String, Seat> seatMap) {

        for (int i = 0; i < seats.length; i++) {
            if (!seats[i].isAvailable())
                continue;
            List<AdjacentSeat> adjSeatList = new ArrayList<AdjacentSeat>(seats.length);
            for (int j = 0; j < seats.length; j++) {
                if (i == j || !seats[j].isAvailable())
                    continue;
                double score = 0;
                String adjType = null;
                if (seats[i].getRowNumber() == seats[j].getRowNumber()) {
                    if (Math.abs(seats[i].getColNumber() - seats[j].getColNumber()) == 1) {
                        //directly adjacency
                        adjType = AdjacentSeat.SIDE_BY_SIDE;
                        score = policy.getAdjacencyScoreSideBySide();
                    } else if (seats[i].isSameSection(seats[j])) {
                        // same rowSection
                        if (seatMissingBtwn(seats[i], seats[j], seatMap)) {    //adjacent seats with 1 missing seat between them
                            adjType = AdjacentSeat.SIDE_BY_SIDE;
                            score = policy.getAdjacencyScoreSideBySide();
                        } else {
                            adjType = AdjacentSeat.SAME_SECTION_SAME_ROW;
                            score = policy.getAdjacencyScoreSameSectSameRow();
                        }
                    } else if (Math.abs(seats[i].getColNumber() - seats[j].getColNumber()) == 2) {
                        // across aisle
                        adjType = AdjacentSeat.ACROSS_AISLE;
                        score = policy.getAdjacencyScoreAcrossAisle();
                    }
                } else if (Math.abs(seats[i].getRowNumber() - seats[j].getRowNumber()) == 1 && seats[i].isSameSection(seats[j])) {
                    adjType = AdjacentSeat.SAME_SECTION_NEXT_ROW;
                    score = policy.getAdjacencyScoreSameSectNextRow();
                }
                if (score != 0) {
                    AdjacentSeat adjacentSeat = new AdjacentSeat(seats[j], score, adjType);
                    adjSeatList.add(adjacentSeat);
                }
            }
            seats[i].setAdjacentSeats(adjSeatList.toArray(new AdjacentSeat[0]));
        }
    }

    //return true if there is 1 seat missing between seat1, seat2, false otherwise.
    //precondition: seat1, seat2 in the same row section, not directly adjacent
    private boolean seatMissingBtwn(Seat seat1, Seat seat2, HashMap<String, Seat> seatMap) {
        char s1 = seat1.getSeatId().charAt(2);
        char s2 = seat2.getSeatId().charAt(2);
        char st1 = (s1 < s2) ? s1 : s2;
        char st2 = (s1 < s2) ? s2 : s1;
        if (st2 - st1 == 2) {
            char midS = (char) (st1 + 1);
            String midSeat = seat1.getSeatId().substring(0, 2) + midS;
            if (seatMap.get(midSeat) == null)
                return true;
        } else if (st2 - st1 == 3) {
            char midS1 = (char) (st1 + 1);
            char midS2 = (char) (st1 + 2);
            String midSeat1 = seat1.getSeatId().substring(0, 2) + midS1;
            String midSeat2 = seat1.getSeatId().substring(0, 2) + midS2;
            if (seatMap.get(midSeat1) == null && seatMap.get(midSeat2) == null)
                return true;
        }
        return false;
    }

}
