package com.delta.seat.api.service;

import com.delta.seat.api.selection.*;

import java.util.HashMap;
import java.util.List;

public interface AdjacentSeatPairsCalculator {

    public void createAdjacentSeatsWithScore(Seat[] seats, Policy policy, HashMap<String, Seat> seatMap);

}
