package com.delta.seat.api.service;

import com.delta.seat.api.dao.SeatEvtMsg;

public interface SeatEngineAsyncTaskExecutor {

    public void upsertSeatEvtRequestJsonDataAsynchronously(SeatEvtMsg seatEvtMsg, String transactionId);

    public void updateSeatEvtResponseJsonDataAsynchronously(SeatEvtMsg seatEvtMsg);
}
