package com.delta.seat.api.service;

import com.delta.seat.api.dao.PolicyParmDao;
import com.delta.seat.api.dao.SeatEngineEvtMsgDao;
import com.delta.seat.api.dao.SeatEvtMsg;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

@Service
public class SeatEngineAsyncTaskExecutorImpl implements SeatEngineAsyncTaskExecutor {

    private static final Logger log = LogManager.getLogger(SeatEngineAsyncTaskExecutorImpl.class);

    @Autowired
    private SeatEngineEvtMsgDao seatEngineEvtMsgDao;

    @Autowired
    private PolicyParmDao policyParmDao;

    @Override
    @Async("asyncTaskExecutor")
    public void upsertSeatEvtRequestJsonDataAsynchronously(SeatEvtMsg seatEvtMsg, String transactionId) {
        try {
            long startTime = System.currentTimeMillis();

            long latestPolicyId = policyParmDao.getMaxPolicyId();
            log.info(String.format("For the transactionId[%s], latest policyId used is [%s]", transactionId,latestPolicyId));
            seatEvtMsg.setPolicyId(latestPolicyId);
            if (seatEvtMsg != null && seatEvtMsg.getRequestClob() != null && !seatEvtMsg.getRequestClob().isEmpty()) {

                //Remove elements which has null value so that it saves spaces in the database
                String jsonRequestWithoutNullValueElement = removeNullValueElementFromJsonString(seatEvtMsg.getRequestClob());
                seatEvtMsg.setRequestClob(jsonRequestWithoutNullValueElement);

                seatEngineEvtMsgDao.insertSeatEvtMsgRequestResponseJsonData(seatEvtMsg);
                log.info(String.format("For the DB TransactionID[%s], total time took to insert event msg[%s] msecs.", seatEvtMsg.getTransactionid(), (System.currentTimeMillis() - startTime)));
            }

        } catch (IOException e) {
            log.error(String.format("For the DB TransactionID[%s], upsertSeatEvtRequestJsonDataAsynchronously failed.",seatEvtMsg.getTransactionid()));
            e.printStackTrace();
        }
    }

    @Override
    public void updateSeatEvtResponseJsonDataAsynchronously(SeatEvtMsg seatEvtMsg) {
        try {
            long startTime = System.currentTimeMillis();
            seatEngineEvtMsgDao.updateSeatEvtMsgResponseJsonData(seatEvtMsg);
            log.info(String.format("For the DB TransactionID[%s], total time took to update event msg[%s] msecs.",seatEvtMsg.getTransactionid(), (System.currentTimeMillis() - startTime)));
        } catch (IOException e) {
            log.error(String.format("For the DB TransactionID[%s], updateSeatEvtResponseJsonDataAsynchronously failed.",seatEvtMsg.getTransactionid()));
            e.printStackTrace();
        }

    }

    private String removeNullValueElementFromJsonString(String incomingJson){
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        Map<String, Object> data = new Gson().fromJson(incomingJson, type);

        for (Iterator<Map.Entry<String, Object>> it = data.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, Object> entry = it.next();
            if (entry.getValue() == null) {
                it.remove();
            } else if (entry.getValue() instanceof ArrayList) {
                if (((ArrayList<?>) entry.getValue()).isEmpty()) {
                    it.remove();
                }
            }
        }
        return  new GsonBuilder().setPrettyPrinting().create().toJson(data);
    }
}
