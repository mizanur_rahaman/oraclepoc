package com.delta.seat.api.service;

import com.delta.seat.api.vo.SeatSelectionOutput;

public interface SeatAssignmentDataProcessor {

    public boolean updateSeatAssignmentValue(String transactionId, String jsonResponseStr);

}
