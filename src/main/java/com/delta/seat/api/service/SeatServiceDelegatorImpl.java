package com.delta.seat.api.service;

import com.delta.seat.api.controller.SeatApiController;
import com.delta.seat.api.vo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

@Component
public class SeatServiceDelegatorImpl implements SeatServiceDelegator {

    private static final Logger log = LogManager.getLogger(SeatServiceDelegatorImpl.class);

    @Override
    public void checkValuesInRequestAndIgnore(SeatSelectionInput seatSelectionRqst, String transactionId){
        List<FlightSegmentInput> FlightInfoRqstList = seatSelectionRqst.getFlightSegments();
        if (FlightInfoRqstList != null && FlightInfoRqstList.size() >= 1 ) {
            Seat seat = null;
            for (FlightSegmentInput flightInfoRqst : FlightInfoRqstList) {

                if (flightInfoRqst != null) {
                    List<Offer> offerList = flightInfoRqst.getOffers();

                    if (offerList != null && offerList.size() >=1){
                        for (Offer offer : offerList){
                            List<Passenger> passengerList = offer.getPassengers();
                            if (passengerList != null && passengerList.size() >=1){
                                for (Passenger passenger : passengerList) {
                                  if (passenger != null){
                                      List<Seatoffer> seatOfferList = passenger.getSeatOffers();
                                      if (seatOfferList != null && seatOfferList.size() >=1){
                                          for (int i = 0; i < seatOfferList.size(); i++) {
                                              Seatoffer seatoffer = seatOfferList.get(i);
                                              if ((seatoffer != null && seatoffer.getPaymentAmt() == null)  || (seatoffer != null && seatoffer.getPaymentAmt().isEmpty())){
                                                  log.debug(String.format("For the transactionId[%s], opasCrrCd[%s], opasFlt#[f%s], seatOfferItemNum[%s], priceAmt is null or empty. So, removing that offer from consideration!!", transactionId, flightInfoRqst.getOperatingCarrierCode(), flightInfoRqst.getOperatingFlightNum(), seatoffer.getOfferItemNum()));
                                                  seatOfferList.remove(i);
                                                  i--;
                                              }
                                          }
                                      }
                                  }
                                }
                            }
                        }
                    }

                    SeatMap seatMap = flightInfoRqst.getSeatMap();
                    if (seatMap != null) {
                        List<Compartment> cabinList = seatMap.getCompartments();
                        if (cabinList != null && cabinList.size() >= 1) {
                            for (Compartment cabin : cabinList) {
                                if (cabin != null) {
                                    List<SeatRow> rowList = cabin.getSeatRows();
                                    if (rowList != null && rowList.size() >= 1) {
                                        for (SeatRow row : rowList) {
                                            if (row != null) {
                                                List<Seat> seatList = row.getSeats();
                                                if (seatList != null && seatList.size() >= 1) {
                                                    for (int i = 0; i < seatList.size(); i++) {
                                                        seat = seatList.get(i);
                                                        if (seatList.get(i) != null) {
                                                            if (seat.getSeatId() == null || (seat.getSeatId() != null && seat.getSeatId().isEmpty())) {
                                                                log.debug(String.format("For the transactionId[%s], opasCrrCd[%s], opasFlt#[f%s], rowNum[%s], one of the seatId is missing. So, removing that seatblock from SeatMap!!", transactionId, flightInfoRqst.getOperatingCarrierCode(), flightInfoRqst.getOperatingFlightNum(), row.getCabinRowSequenceNum()));
                                                                seatList.remove(seat);
                                                                i--;
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                }
            }

            }
        }
    }

    @Override
    public void sortSeatMapData(SeatMap seatMap, String transactionId) {
        List<Compartment> cabinList = seatMap.getCompartments();
        //sort cabin
        Collections.sort(cabinList, new Comparator<Compartment>() {
            @Override
            public int compare (Compartment cabin1, Compartment cabin2){
                return cabin1.getCompartmentCode().compareToIgnoreCase(cabin2.getCompartmentCode());
            }

        });

        for (Compartment cabin : cabinList) {
            List<SeatRow> rowList = cabin.getSeatRows();
            //sort row for each cabin
            Collections.sort(rowList, new Comparator<SeatRow>() {
                @Override
                public int compare(SeatRow row1, SeatRow row2) {
                    return row1.getCabinRowSequenceNum().compareTo(row2.getCabinRowSequenceNum());
                }
            });
            for (SeatRow row : rowList) {
                List<Seat> seatInfoList = row.getSeats();
                //sort each seat in the row
                Collections.sort(seatInfoList, new Comparator<Seat>() {
                    @Override
                    public int compare(Seat seatInfo1, Seat seatInfo2) {
                        return seatInfo1.getSeatId().compareToIgnoreCase(seatInfo2.getSeatId());
                    }
                });
            }
        }
    }
}
