package com.delta.seat.api.service;

import com.delta.seat.api.selection.AdjacentOption;
import com.delta.seat.api.selection.FlightLeg;
import com.delta.seat.api.selection.Option;
import com.delta.seat.api.selection.Pnr;

public interface SeatingOption {
    public AdjacentOption[] createAdjacentOptions(FlightLeg fltLeg, String transactionId);

    public Option[] createOptions(FlightLeg fltLeg, Pnr pnr, String transactionId);

    public com.delta.seat.api.vo.SeatSelectionOutput selectSeats(com.delta.seat.api.vo.SeatSelectionInput seatSelectionInput, String transactionId, boolean masterList);

}
