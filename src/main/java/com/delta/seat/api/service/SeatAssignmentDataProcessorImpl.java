package com.delta.seat.api.service;

import com.delta.seat.api.dao.SeatEngineEvtMsgDao;
import com.delta.seat.api.dao.SeatEvtMsg;
import com.delta.seat.api.vo.SeatSelectionOutput;
import com.delta.seat.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;

@Service
public class SeatAssignmentDataProcessorImpl implements SeatAssignmentDataProcessor {

    @Autowired
    private SeatEngineEvtMsgDao seatEngineEvtMsgDao;

    @Override
    public boolean updateSeatAssignmentValue(String transactionId, String jsonResponseStr) {

        boolean isUpdateSuccessfully = false;
        if (jsonResponseStr != null){
            SeatEvtMsg seatEvtMsg = createSeatEvtMsg(jsonResponseStr, transactionId);
            try {
                int updateSeatEvtMsgResponseJsonData = seatEngineEvtMsgDao.updateSeatEvtMsgResponseJsonData(seatEvtMsg);
                if (updateSeatEvtMsgResponseJsonData >= 1){
                    isUpdateSuccessfully = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(String.format("For the transactionId[%s], problem inserting seat assignment json response!", transactionId));
            }
        }
        return isUpdateSuccessfully;
    }

    private SeatEvtMsg createSeatEvtMsg(String jsonRespStr, String transactionId) {
        SeatEvtMsg seatEvtMsg = new SeatEvtMsg();
        seatEvtMsg.setTransactionid(transactionId);
        seatEvtMsg.setCrtnGts(new Timestamp(System.currentTimeMillis()));
        seatEvtMsg.setResponseClob(jsonRespStr);
        return seatEvtMsg;
    }
}
