package com.delta.seat.api.service;

import java.util.List;

import com.delta.seat.api.dao.SeatAnalytics;
import com.delta.seat.api.vo.Passenger;
import com.delta.seat.api.vo.SeatAnalyticsRequest;
import com.delta.seat.api.vo.SeatAnalyticsResponse;
import com.delta.seat.api.vo.SeatAnalyticsSengEvtMsg;

public interface SeatAnalyticsService {
	 public SeatAnalyticsResponse getFlightAnalyticsData(SeatAnalyticsRequest seatAnalyticsRequest);
	 public void processResponseAndUnseatedPaxPNRList(List<SeatAnalytics> seatEvtMsgList);
	 public int getTotalSinglePNRByRecommendationType(List<SeatAnalytics> seatEvtMsgList, int recommendationType);
	 public void getTotalMultiPNRByRecommendationType(List<SeatAnalytics> seatEvtMsgList, int recommendationType,SeatAnalyticsResponse seatAnalyticsResponse);
	 public void getTotalFamilyPNRByRecommendationType(List<SeatAnalytics> seatEvtMsgList, int recommendationType,SeatAnalyticsResponse seatAnalyticsResponse);
	 public int getTotalMedalationByMedalationType(List<SeatAnalytics> seatEvtMsgList, SeatAnalyticsRequest seatAnalyticsRequest);
	 public int getNonLoyalityProgramTireCodePassengers(List<SeatAnalytics> seatEvtMsgList, SeatAnalyticsRequest seatAnalyticsRequest);
	 public List<SeatAnalyticsSengEvtMsg> generateSeatAnalyticsEvtMsgList(List<SeatAnalytics> seatEvtMsgList);
	 public List<Passenger> getPassengersByTransactionId(String transactionId);
}
