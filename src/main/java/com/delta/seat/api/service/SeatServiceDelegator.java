package com.delta.seat.api.service;

import com.delta.seat.api.vo.SeatMap;
import com.delta.seat.api.vo.SeatSelectionInput;

public interface SeatServiceDelegator {
    public void checkValuesInRequestAndIgnore(SeatSelectionInput seatSelectionInput, String transactionId);
    public void sortSeatMapData(SeatMap seatMap, String transactionId);
}
