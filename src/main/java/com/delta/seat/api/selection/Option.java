package com.delta.seat.api.selection;

import java.util.Map;

public class Option implements Comparable<Option> {
    private Psgr[] psgrs;
    private PsgrSeat[] psgrSeats;
    private double seatScore;
    private double adjScore;
    private double score;

    public Option(Psgr psgr, PsgrSeat psgrSeat) {
        psgrs = new Psgr[1];
        psgrs[0] = psgr;
        psgrSeats = new PsgrSeat[1];
        psgrSeats[0] = psgrSeat;
        this.seatScore = psgrSeat.getScore();
        this.score = psgrSeat.getScore();
    }

    public Option(Psgr[] psgrs, PsgrSeat[] psgrSeats) {
        this.psgrs = psgrs;
        this.psgrSeats = psgrSeats;
        for (int i = 0; i < psgrSeats.length; i++) {
            seatScore += psgrSeats[i].getScore();
            score += psgrSeats[i].getScore();
        }
    }
    public Option(Psgr[] psgrs, PsgrSeat[] psgrSeats, double adjScore, Policy policy) {
        this.psgrs = psgrs;
        this.psgrSeats = psgrSeats;
        this.adjScore = adjScore;
        if (!isValid())
            return;
        for (int i = 0; i < psgrSeats.length; i++) {
            seatScore += psgrSeats[i].getScore();
            score += psgrSeats[i].getScore();
        }
        score = score + policy.getCoefficientAdjacentScore() * adjScore;
    }

    public static PsgrSeat[] bestCombination(Psgr[] psgrs, AdjacentOption adjacentOption, Policy policy) {
        if (psgrs.length == 2 && adjacentOption.size() == 2) {
            return bestCombination2Pax(psgrs, adjacentOption, policy);
        } else if (psgrs.length == 3 && adjacentOption.size() == 3) {
            return bestCombination3Pax(psgrs, adjacentOption, policy);
        }
        return new PsgrSeat[0];
    }

    public static PsgrSeat[] bestCombination2Pax(Psgr[] psgrs, AdjacentOption adjacentOption, Policy policy) {
        PsgrSeat[] psgrSeats1 = new PsgrSeat[2];
        Seat[] adjSeats = adjacentOption.getSeats();
        psgrSeats1[0] = psgrs[0].getPsgrSeat(adjSeats[0]);
        psgrSeats1[1] = psgrs[1].getPsgrSeat(adjSeats[1]);
        Option bestOption = new Option(psgrs, psgrSeats1, adjacentOption.getScore(), policy);
        PsgrSeat[] psgrSeats2 = new PsgrSeat[2];
        psgrSeats2[0] = psgrs[0].getPsgrSeat(adjSeats[1]);
        psgrSeats2[1] = psgrs[1].getPsgrSeat(adjSeats[0]);
        Option option = new Option(psgrs, psgrSeats2, adjacentOption.getScore(), policy);
        if (bestOption.isWorse(option))
            bestOption = option;
        if (bestOption.isValid())
            return bestOption.getPsgrSeats();
        return new PsgrSeat[0];
    }

    public static PsgrSeat[] bestCombination3Pax(Psgr[] psgrs, AdjacentOption adjacentOption, Policy policy) {
        Seat[] adjSeats = adjacentOption.getSeats();
        PsgrSeat[] psgrSeats1 = new PsgrSeat[3];
        psgrSeats1[0] = psgrs[0].getPsgrSeat(adjSeats[0]);
        psgrSeats1[1] = psgrs[1].getPsgrSeat(adjSeats[1]);
        psgrSeats1[2] = psgrs[2].getPsgrSeat(adjSeats[2]);
        Option bestOption = new Option(psgrs, psgrSeats1, adjacentOption.getScore(), policy);
        PsgrSeat[] psgrSeats2 = new PsgrSeat[3];
        psgrSeats2[0] = psgrs[0].getPsgrSeat(adjSeats[0]);
        psgrSeats2[1] = psgrs[1].getPsgrSeat(adjSeats[2]);
        psgrSeats2[2] = psgrs[2].getPsgrSeat(adjSeats[1]);
        Option option = new Option(psgrs, psgrSeats2, adjacentOption.getScore(), policy);
        if (bestOption.isWorse(option))
            bestOption = option;
        PsgrSeat[] psgrSeats3 = new PsgrSeat[3];
        psgrSeats3[0] = psgrs[0].getPsgrSeat(adjSeats[1]);
        psgrSeats3[1] = psgrs[1].getPsgrSeat(adjSeats[0]);
        psgrSeats3[2] = psgrs[2].getPsgrSeat(adjSeats[2]);
        option = new Option(psgrs, psgrSeats3, adjacentOption.getScore(), policy);
        if (bestOption.isWorse(option))
            bestOption = option;
        PsgrSeat[] psgrSeats4 = new PsgrSeat[3];
        psgrSeats4[0] = psgrs[0].getPsgrSeat(adjSeats[1]);
        psgrSeats4[1] = psgrs[1].getPsgrSeat(adjSeats[2]);
        psgrSeats4[2] = psgrs[2].getPsgrSeat(adjSeats[0]);
        option = new Option(psgrs, psgrSeats4, adjacentOption.getScore(), policy);
        if (bestOption.isWorse(option))
            bestOption = option;
        PsgrSeat[] psgrSeats5 = new PsgrSeat[3];
        psgrSeats5[0] = psgrs[0].getPsgrSeat(adjSeats[2]);
        psgrSeats5[1] = psgrs[1].getPsgrSeat(adjSeats[0]);
        psgrSeats5[2] = psgrs[2].getPsgrSeat(adjSeats[1]);
        option = new Option(psgrs, psgrSeats5, adjacentOption.getScore(), policy);
        if (bestOption.isWorse(option))
            bestOption = option;
        PsgrSeat[] psgrSeats6 = new PsgrSeat[3];
        psgrSeats6[0] = psgrs[0].getPsgrSeat(adjSeats[2]);
        psgrSeats6[1] = psgrs[1].getPsgrSeat(adjSeats[1]);
        psgrSeats6[2] = psgrs[2].getPsgrSeat(adjSeats[0]);
        option = new Option(psgrs, psgrSeats6, adjacentOption.getScore(), policy);
        if (bestOption.isWorse(option))
            bestOption = option;
        if (bestOption.isValid())
            return bestOption.getPsgrSeats();
        else
            return new PsgrSeat[0];
    }

    public static void createOptions3Pax2AdjSeats(Psgr[] psgrs, AdjacentOption adjacentOption, Map<String, Option> optionMap, Policy policy) {
        Psgr[] psgrs01 = {psgrs[0], psgrs[1]};
        PsgrSeat[] psgrSeats2 = Option.bestCombination(psgrs01, adjacentOption, policy);
        if (psgrSeats2.length == 0)
            return;
        PsgrSeat[] psgrSeats = psgrs[2].getPsgrSeats();
        for (int j = 0; j < psgrSeats.length; j++) {
            if (adjacentOption.pairSeat(psgrSeats[j].getSeat())) {
                PsgrSeat[] psgrSeats01j = {psgrSeats2[0], psgrSeats2[1], psgrSeats[j]};
                Option option = new Option(psgrs, psgrSeats01j, adjacentOption.getScore(), policy);
                Option.putOption(optionMap, option);
            }
        }
        Psgr[] psgrs02 = {psgrs[0], psgrs[2]};
        psgrSeats2 = Option.bestCombination(psgrs02, adjacentOption, policy);
        if (psgrSeats2.length == 0)
            return;
        psgrSeats = psgrs[1].getPsgrSeats();
        for (int j = 0; j < psgrSeats.length; j++) {
            if (adjacentOption.pairSeat(psgrSeats[j].getSeat())) {
                PsgrSeat[] psgrSeats0j2 = {psgrSeats2[0], psgrSeats[j], psgrSeats2[1]};
                Option option = new Option(psgrs, psgrSeats0j2, adjacentOption.getScore(), policy);
                Option.putOption(optionMap, option);
            }
        }
        Psgr[] psgrs12 = {psgrs[1], psgrs[2]};
        psgrSeats2 = Option.bestCombination(psgrs12, adjacentOption, policy);
        if (psgrSeats2.length == 0)
            return;
        psgrSeats = psgrs[0].getPsgrSeats();
        for (int j = 0; j < psgrSeats.length; j++) {
            if (adjacentOption.pairSeat(psgrSeats[j].getSeat())) {
                PsgrSeat[] psgrSeatsj12 = {psgrSeats[j], psgrSeats2[0], psgrSeats2[1]};
                Option option = new Option(psgrs, psgrSeatsj12, adjacentOption.getScore(), policy);
                Option.putOption(optionMap, option);
            }
        }
    }

    public boolean isValid() {
        for (int i = 0; i < psgrSeats.length; i++) {
            if (psgrSeats[i] == null)
                return false;
        }
        return true;
    }
    /**
     * Is this option worse than input option
     */
    public boolean isWorse(Option option) {
        if (option.isValid()) {
            if (!isValid())
                return true;
            if (Math.abs(option.score - score) < 0.0001)
                return option.getSeatIds().compareTo(getSeatIds()) < 0;
            else if (option.score > score)
                return true;
        }
        return false;
    }
    public String getSeatIds() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < psgrSeats.length; i++)
            str.append(psgrSeats[i].getSeat().getSeatId());
        return str.toString();
    }
    public String getSortedSeatIds() {
        Seat[] seats = new Seat[psgrSeats.length];
        for (int i = 0; i < seats.length; i++)
            seats[i] = psgrSeats[i].getSeat();
        return Seat.sortSeatIds(seats);
    }

    public static void putOption(Map<String, Option> optionMap, Option option) {
        String sortedSeatIds = option.getSortedSeatIds();
        Option oldOpt = optionMap.get(sortedSeatIds);
        if (oldOpt == null) {
            optionMap.put(sortedSeatIds, option);
        } else if (Math.abs(oldOpt.score - option.score) < 0.001) {
            if (oldOpt.getSeatIds().compareTo(option.getSeatIds()) > 0)
                optionMap.put(sortedSeatIds, option);
        } else if (oldOpt.score < option.score)
            optionMap.put(sortedSeatIds, option);
    }

    public int compareTo(Option obj) {
        if (Math.abs(score - obj.score) < 0.001)
            return getSortedSeatIds().compareTo(obj.getSortedSeatIds());
        else if (score < obj.score)
            return 1;
        else
            return -1;
    }

    public PsgrSeat[] getPsgrSeats() {
        return psgrSeats;
    }
    public void markSelected() {
        for (int i = 0; i < psgrSeats.length; i++) {
            psgrSeats[i].getSeat().setAvailable(false);
        }
    }
    public double getSeatScore() { return seatScore; }
    public double getAdjScore() { return adjScore; }
    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < psgrs.length; i++) {
            str.append(psgrs[i].getName()).append(":").append(psgrSeats[i].getSeat().getSeatId()).append(" ");
        }
        str.append(String.format("%.1f", seatScore)).append("  ");
        str.append(String.format("%.1f", adjScore)).append("  ");
        str.append(String.format("%.1f", score)).append("  ");
        return str.toString();
    }
}
