package com.delta.seat.api.selection;

import com.delta.seat.api.service.AdjacentSeatPairsCalculator;
import com.delta.seat.api.service.AdjacentSeatPairsCalculatorImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class Cabin {
    private static final Logger log = LogManager.getLogger(Cabin.class);
    private String cabinCd; //CFWY
    private int inventoryCnt;
    private Seat[] seats;
    private HashMap<String, Seat> seatMap;  //key: seatId
    private AdjacentOption[] adjacentOptions;
    private AdjacentSeatPairsCalculator adjacentSeatPairsCalculator = new AdjacentSeatPairsCalculatorImpl();

    public Cabin(String cabinCode) {
        cabinCd = cabinCode;
    }
    public Cabin(com.delta.seat.api.vo.Compartment voCabin) {
        seatMap = new HashMap<>();
        cabinCd = voCabin.getCompartmentCode();
        if (voCabin.getAvilableInventoryCnt() != null)
            inventoryCnt = Integer.parseInt(voCabin.getAvilableInventoryCnt());
        int frontRowNum = 100;
        int backRowNum = 0;
        List<Seat> seatList = new ArrayList<>();
        List<com.delta.seat.api.vo.SeatRow> voRowList = voCabin.getSeatRows();
        int aisleCount = SeatAttributes.computeAisleCount(voRowList);
        boolean computeAttributes = false;
        for (com.delta.seat.api.vo.SeatRow voRow: voRowList) {
            int rowNum = Integer.parseInt(voRow.getCabinRowSequenceNum());
            if (frontRowNum > rowNum)
                frontRowNum = rowNum;
            if (backRowNum < rowNum)
                backRowNum = rowNum;
            List<com.delta.seat.api.vo.Seat> voSeatList = voRow.getSeats();
            if (voSeatList == null)
                continue;
            for (com.delta.seat.api.vo.Seat voSeat: voSeatList) {
                if (voSeat.getSeatId()==null)
                    continue;
                Seat seat = new Seat(voSeat);
                seat.setCabin(this);
                computeAttributes = (seat.getAircraftSide()=='\u0000');
                seatMap.put(seat.getSeatId(), seat);
                seat.setRowNumber(rowNum);
                seatList.add(seat);
            }
            if (computeAttributes)
                SeatAttributes.computeAttributes(voSeatList, seatMap, aisleCount);
        }
        seats = seatList.toArray(new Seat[0]);
        for (int i = 0; i < seats.length; i++) {
            if (computeAttributes)
                seats[i].setCabinRowNum(seats[i].getRowNumber() - frontRowNum + 1);
            seats[i].setCabinRowPct(100*(seats[i].getCabinRowNum()-1.0)/(backRowNum-frontRowNum));
        }
    }

    public Seat[] getValidSeats() {
        List<Seat> seatList = new ArrayList<>();
        for (int i = 0; i<seats.length; i++) {
            if (seats[i].isAvailable() && seats[i].isOffered())
                seatList.add(seats[i]);
        }
        return seatList.toArray(new Seat[0]);
    }
    public String getCabinCd() {
        return cabinCd;
    }
    public int getInventoryCnt() { return inventoryCnt; }
    public void decreaseInventoryCnt(int decreaseCnt) { inventoryCnt = inventoryCnt - decreaseCnt; }
    public Seat[] getSeats() {
        return seats;
    }
    public void setSeats(List<Seat> seatList) {
        seats = seatList.toArray(new Seat[0]);
        seatMap = new HashMap<>();
        for (int i = 0; i < seats.length; i++)
            seatMap.put(seats[i].getSeatId(), seats[i]);
    }
    public Map<String, Seat> getSeatMap() {
        return seatMap;
    }

    public void createAdjacentSeats() {
        Policy policy = Policy.getInstance();
        adjacentSeatPairsCalculator.createAdjacentSeatsWithScore(getValidSeats(), policy, seatMap);
    }

    public void createAdjacentOptions() {
        adjacentOptions = AdjacentOption.createAdjacentOptions(getValidSeats());
    }


    public void createOptions(Psgr[] psgrs, Map<String, Option> optionMap, Policy policy) {
        for (int i = 0; i < adjacentOptions.length; i++) {
            if (adjacentOptions[i].size() == psgrs.length) {
                PsgrSeat[] bestPsgrSeats = Option.bestCombination(psgrs, adjacentOptions[i], policy);
                if (bestPsgrSeats.length != 0) {
                    Option option = new Option(psgrs, bestPsgrSeats, adjacentOptions[i].getScore(), policy);
                    optionMap.put(option.getSortedSeatIds(), option);
                }
            }
        }
    }

    public void createOptionsMultiSeats(Psgr[] psgrs, Map<String, Option> optionMap, Policy policy) {
        if (psgrs.length == 3) {
            for (int i = 0; i < adjacentOptions.length; i++) {
                if (adjacentOptions[i].size() == 2) {
                    Option.createOptions3Pax2AdjSeats(psgrs, adjacentOptions[i], optionMap, policy);
                }
            }
        }
    }


    public AdjacentOption[] getAdjacentOption() {
        return adjacentOptions;
    }

    public void debug(String transactionId) {
        log.debug(transactionId + " - Cabin " + cabinCd);
        for (int i = 0; i < seats.length; i++) {
            if (seats[i].isAvailable() && seats[i].isOffered())
                log.debug(transactionId + " - " + seats[i].toString());
        }
    }

    public void debugAdjacentSeats() {
        StringBuilder str = new StringBuilder();
        str.append("Cabin ").append(cabinCd);
        log.debug(str);
        for (int i = 0; i < seats.length; i++) {
            seats[i].debug();
        }
    }
    public void debugAdjacentOptions(String transactionId) {
        log.debug(transactionId + " - Cabin " + cabinCd + " adjacency options, score");
        if (adjacentOptions == null || adjacentOptions.length == 0)
            return;
        int size = adjacentOptions.length > 30?30:adjacentOptions.length;
        for (int i = 0; i < size; i++) {
            log.debug(transactionId + " - " + adjacentOptions[i].toString());
        }
    }
}
