package com.delta.seat.api.selection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.*;
import java.io.File;

public class Request {
    private static final Logger log = LogManager.getLogger(Request.class);
    private String transactionId;
    private FlightLeg fltLeg;
    private Pnr[] pnrs;

    /**
     * These are jvm parameters needed to run the class:
     * -Dseat.logdir=C:\logs\seat -Dseat.logfile=SEAT.log -Ddatadir=C:\work\seating\data
     */
    public static void main(String[] args) {
        com.delta.seat.api.vo.SeatSelectionInput voReq = getDataFromJson("request28_new.txt");
        run(voReq, "12345");
    }

    public static com.delta.seat.api.vo.SeatSelectionInput getDataFromJson(String fileName) {
        String fName = System.getProperty("datadir");
        fName = fName + "\\" + fileName;
        File reqFile = new File(fName);
        ObjectMapper objMapper = new ObjectMapper();
        com.delta.seat.api.vo.SeatSelectionInput voReq = null;
        try {
            voReq = objMapper.readValue(reqFile, com.delta.seat.api.vo.SeatSelectionInput.class);
        } catch (Exception e) {
            log.error(e);
        }
        return voReq;
    }

    public Request() {}
    public Request(com.delta.seat.api.vo.SeatSelectionInput voRequest, String transactionId) {
        this.transactionId = transactionId;
        List<com.delta.seat.api.vo.FlightSegmentInput> voFltInfoRqstList = voRequest.getFlightSegments();
        com.delta.seat.api.vo.FlightSegmentInput voFltInfoRqst = voFltInfoRqstList.get(0);
        fltLeg = new FlightLeg(voFltInfoRqst);
        Map<String, Seat> seatMap = fltLeg.getSeatMap();
        List<com.delta.seat.api.vo.Offer> voOfferList = voFltInfoRqst.getOffers();
        pnrs = new Pnr[voOfferList.size()];
        int index = 0;
        for (com.delta.seat.api.vo.Offer voOffer: voOfferList) {
            pnrs[index++] = new Pnr(voOffer, seatMap, transactionId);
        }
    }

    public static com.delta.seat.api.vo.SeatSelectionOutput run(com.delta.seat.api.vo.SeatSelectionInput voReq, String transactionId) {
        Request req = new Request(voReq, transactionId);
        com.delta.seat.api.vo.SeatSelectionOutput voSeatSelectionResp = new com.delta.seat.api.vo.SeatSelectionOutput();
        voSeatSelectionResp.setTransactionId(transactionId);
        List<com.delta.seat.api.vo.FlightSegmentOutput> voFltInfoRespList = new ArrayList<>();
        voSeatSelectionResp.setFlightSegmentOutputs(voFltInfoRespList);
        com.delta.seat.api.vo.FlightSegmentOutput voFltInfoResp = req.fltLeg.getFlightInfoRspnsVO();
        voFltInfoRespList.add(voFltInfoResp);

        List<com.delta.seat.api.vo.SeatSelection> voSeatSelectionList = new ArrayList<>();
        long start = System.currentTimeMillis();
        req.debug();
        req.createAdjacentOptions();
        req.createOptions(0, transactionId);
        req.pnrs[0].debugOption(transactionId);
        log.info(transactionId + " - created " + req.pnrs[0].getOptions().length + " options for " + req.pnrs[0].getPnr() + " in " + (System.currentTimeMillis() - start) + " ms.");
        if (req.pnrs[0].selectOption(false) == null)
            return null;
        List<com.delta.seat.api.vo.SeatSelection> voSelectionList = req.pnrs[0].getSeatSelectionListVO(voReq.getFlightSegments().get(0).getFlightIdRefText());
        voSeatSelectionList.addAll(voSelectionList);
        req.pnrs[0].markSelection();
        log.info(transactionId + " - " + req.pnrs[0].getPnr() + " selection: " + req.pnrs[0].getSelection());
        voFltInfoResp.setSeatSelections(voSeatSelectionList);
        return voSeatSelectionResp;
    }


    public AdjacentOption[] createAdjacentOptions() {
        fltLeg.createAdjacentSeats();
        fltLeg.createAdjacentOptions(transactionId);
        List<AdjacentOption> adjOptList = new ArrayList<>();
        Cabin[] cabins = fltLeg.getCabins();
        for (int i = 0; i < cabins.length; i++) {
            AdjacentOption[] adjOptions = cabins[i].getAdjacentOption();
            if (adjOptions.length != 0) {
                for (int j = 0; j < adjOptions.length; j++) {
                    if (adjOptions[j].size() == 3) {
                        adjOptList.add(adjOptions[j]);
                    }
                }
            }
        }
        for (int i = 0; i < cabins.length; i++) {
            AdjacentOption[] adjOptions = cabins[i].getAdjacentOption();
            if (adjOptions.length != 0) {
                for (int j = 0; j < adjOptions.length; j++) {
                    if (adjOptions[j].size() == 2) {
                        adjOptList.add(adjOptions[j]);
                    }
                }
            }
        }
        return adjOptList.toArray(new AdjacentOption[0]);
    }

    public Option[] createOptions(int index, String transactionId) {
        pnrs[index].createPreferenceScore();
        pnrs[index].createOptions(fltLeg, transactionId);
//            pnrs[index].calibrateScore();
        return pnrs[index].getOptions();

    }
    public FlightLeg getFltLeg() { return fltLeg; }

    public Pnr[] getPnrs() { return pnrs; }

    public void debug() {
        fltLeg.debug(transactionId);
        for (int i = 0;i < pnrs.length; i++) {
            pnrs[i].debug(transactionId);
        }
    }

}
