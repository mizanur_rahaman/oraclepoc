package com.delta.seat.api.selection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class Seat implements Comparator<Seat> {
    private static final Logger log = LogManager.getLogger(Seat.class);
    private String seatId;  //01A, 12A, etc.
    private int rowNumber;
    private int cabinRowNum;
    private double cabinRowPct;
    private int colNumber;
    private boolean aisle;
    private boolean window;
    private boolean middle;
    private boolean available;
    private boolean offered;    //this is one of the offered seats
    private boolean preferred;
    private char rowSection;  //S-side, C-center
    private char aircraftSide;    //L-left, R-right
    private boolean exit;
    private Cabin cabin;
    private double[] seatScores;    //seat scores by reference type, Aisle, Window, None, length = 3
    private AdjacentSeat[] adjacentSeats;
    private HashMap<String, AdjacentSeat> adjSeatMap = new HashMap<>(); //key: seatId

    public Seat() {}
    public Seat(String seatid) { seatId = seatid; }
    public Seat(com.delta.seat.api.vo.Seat voSeat) {
        seatId = voSeat.getSeatId();
        seatId = (seatId.length()==2)?"0"+seatId : seatId;
        colNumber = Integer.parseInt(voSeat.getIndexNum()) + 1;
        aisle = voSeat.getAisle()==null?false:voSeat.getAisle();
        window = voSeat.getWindow()==null?false:voSeat.getWindow();
        middle = voSeat.getMiddle()==null?false:voSeat.getMiddle();
        available = "true".equalsIgnoreCase(voSeat.getSeatAvailableCode());
        preferred = "true".equalsIgnoreCase(voSeat.getPreferredText());
        String temp = voSeat.getCompartmentRowNum();
        if (temp != null)
            cabinRowNum = (int)Float.parseFloat(temp);
        temp = voSeat.getRowSectionCode();
        if (temp != null)
            rowSection = temp.charAt(0);
        temp = voSeat.getAircraftSideCode();
        if (temp != null)
            aircraftSide = temp.charAt(0);
        exit = voSeat.getExitRowSeat()==null?false:voSeat.getExitRowSeat();
    }
    public void setSeatScores(String seatScoreNum, String preferenceCd) {
        if (seatScoreNum == null)
            return;
        if (seatScores == null)
            seatScores = new double[3];
        preferenceCd = preferenceCd==null?"":preferenceCd;
        switch (preferenceCd) {
            case "A": seatScores[0] = Double.parseDouble(seatScoreNum);
                break;
            case "W": seatScores[1] = Double.parseDouble(seatScoreNum);
                break;
            default: seatScores[2] = Double.parseDouble(seatScoreNum);
        }
    }
    public double getSeatScore(String preferenceCd) {
        if (seatScores == null)
            return 0;
        preferenceCd = preferenceCd==null?"":preferenceCd;
        switch (preferenceCd) {
            case "A": return seatScores[0];
            case "W": return seatScores[1];
            default: return seatScores[2];
        }
    }
    /**
     * Return a list of ordered seat ids.
     * @param seats
     * @return
     */
    public static String sortSeatIds(Seat[] seats) {
        String[] seatIds = new String[seats.length];
        for (int j = 0; j < seats.length; j++)
            seatIds[j] = seats[j].getSeatId();
        Arrays.sort(seatIds);
        StringBuilder seatIdKey = new StringBuilder();
        for (int j = 0; j < seatIds.length; j++)
            seatIdKey.append(seatIds[j]);
        return seatIdKey.toString();
    }
    public String getSeatId() {
        return seatId;
    }
    public boolean isSame(Seat seat) {
        return seatId.equals(seat.seatId);
    }
    public int getRowNumber() {
        return rowNumber;
    }
    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }
    public void setCabinRowNum(int cabinRowNum) {
        this.cabinRowNum = cabinRowNum;
    }

    public int getCabinRowNum() {
        return cabinRowNum;
    }

    public void setCabinRowPct(double cabinRowPct) {
        this.cabinRowPct = cabinRowPct;
    }

    public double getCabinRowPct() {
        return cabinRowPct;
    }

    public int getColNumber() {
        return colNumber;
    }
    public void setColNumber(int colNum) {colNumber = colNum; }
    public boolean isAisle() {
        return aisle;
    }
    public void setAisle(boolean aisle) { this.aisle = aisle; }
    public boolean isWindow() {
        return window;
    }
    public void setWindow(boolean window) { this.window = window; }

    public void setMiddle(boolean middle) { this.middle = middle; }
    public boolean isPreferred() {
        return preferred;
    }
    public boolean isAvailable() {
        if (aisle==false && window==false && middle==false)
            return false;
        return available;
    }
    public void setAvailable(boolean available) { this.available = available; }
    public boolean isOffered() { return offered; }
    public void setOffered(boolean offered) { this.offered = offered; }
    public void setRowSection(char rowSection) {
        this.rowSection = rowSection;
    }
    public char getRowSection() { return rowSection; }
    public void setAircraftSide(char aircraftSide) {
        this.aircraftSide = aircraftSide;
    }
    public char getAircraftSide() { return aircraftSide; }
    public boolean isExit() { return exit; }
    public boolean isSameCabin(Seat seat) {
        return cabin.getCabinCd().equals(seat.cabin.getCabinCd());
    }

    public void setCabin(Cabin cabin) {
        this.cabin = cabin;
    }
    public Cabin getCabin() { return cabin; }

    public boolean isSameSection(Seat seat) {
        boolean flag = false;
        if (rowSection == seat.rowSection && (aircraftSide == seat.aircraftSide || rowSection == 'C'))
            flag = true;
        return flag;
    }

    public void setAdjacentSeats(AdjacentSeat[] adjSeatArray) {
        adjacentSeats = adjSeatArray;
        adjSeatMap = new HashMap<>((int)(adjacentSeats.length*1.3));
        for (int i = 0; i < adjacentSeats.length; i++)
            adjSeatMap.put(adjacentSeats[i].getSeat().getSeatId(), adjacentSeats[i]);
    }

    public AdjacentSeat getAdjacentSeat(Seat seat) {
        return adjSeatMap.get(seat.getSeatId());
    }
    public AdjacentSeat getAdjacentSeat(String seatId) { return adjSeatMap.get(seatId); }

    public int compare(Seat o1, Seat o2) {
        return o1.colNumber - o2.colNumber;
    }
    public String toString() {
        if (!isAvailable())
            return "";
        StringBuilder str = new StringBuilder();
        str.append(seatId).append(" ").append(rowNumber).append(" ").append(colNumber).append(" ");
        if (aisle)
            str.append("aisle").append(" ");
        if (window)
            str.append("window").append(" ");
        if (middle)
            str.append("middle").append(" ");
        str.append(rowSection).append(' ').append(aircraftSide).append(' ');
        if (seatScores != null)
            str.append(seatScores[0]).append(" ").append(seatScores[1]).append(" ").append(seatScores[2]).append(" ");
        return str.toString();
    }

    public void debug() {
        if (!isAvailable() || !isOffered())
            return;
        StringBuilder str = new StringBuilder();
        str.append(seatId).append("   ");
        if (adjacentSeats != null) {
            for (int i = 0; i < adjacentSeats.length; i++) {
                str.append(adjacentSeats[i].toString());
            }
        }
        log.debug(str);
    }
}
