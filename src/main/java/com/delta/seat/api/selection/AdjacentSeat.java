package com.delta.seat.api.selection;

public class AdjacentSeat {
    public static final String SIDE_BY_SIDE = "DR";   //directly adjacent
    public static final String ACROSS_AISLE = "AA";
    public static final String SAME_SECTION_SAME_ROW = "SS";
    public static final String SAME_SECTION_NEXT_ROW = "NR";

    private Seat seat;
    private double score;
    private String adjacentType;

    public AdjacentSeat(Seat seat, double score, String adjacentType) {
        this.seat = seat;
        this.score = score;
        this.adjacentType = adjacentType;
    }

    public Seat getSeat() {
        return seat;
    }

    public double getScore() {
        return score;
    }

    public String getAdjacentType() {
        return adjacentType;
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(seat.getSeatId()).append(" ").append(adjacentType).append("  ");
        return str.toString();
    }
}
