package com.delta.seat.api.selection;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AdjacentOption implements Comparable<AdjacentOption> {
    private Seat[] seats;
    private double score12;
    private String adjType12;
    private double score13;
    private String adjType13;
    private double score23;
    private String adjType23;
    private double score;	//sum of the 2 highest score


    public AdjacentOption(Seat seat1, Seat seat2) {
        seats = new Seat[2];
        seats[0] = seat1;
        seats[1] = seat2;
        AdjacentSeat adjSeat12 = seat1.getAdjacentSeat(seat2);
        if (adjSeat12 != null) {
            score12 = adjSeat12.getScore();
            adjType12 = adjSeat12.getAdjacentType();
            score = adjSeat12.getScore();
        }
    }
    public AdjacentOption(Seat seat1, Seat seat2, Seat seat3) {
        seats = new Seat[3];
        seats[0] = seat1;
        seats[1] = seat2;
        seats[2] = seat3;
        AdjacentSeat adjSeat12 = seat1.getAdjacentSeat(seat2);
        AdjacentSeat adjSeat13 = seat1.getAdjacentSeat(seat3);
        AdjacentSeat adjSeat23 = seat2.getAdjacentSeat(seat3);
        if (adjSeat12 != null) {
            this.score12 = adjSeat12.getScore();
            this.adjType12 = adjSeat12.getAdjacentType();
        }
        if (adjSeat13 != null) {
            this.score13 = adjSeat13.getScore();
            this.adjType13 = adjSeat13.getAdjacentType();
        }
        if (adjSeat23 != null) {
            this.score23 = adjSeat23.getScore();
            this.adjType23 = adjSeat23.getAdjacentType();
        }
        score = score12 + score13 + score23;
    }


    public static AdjacentOption[] createAdjacentOptions(Seat[] seats) {
        List<AdjacentOption> options = new LinkedList<>();
        for (int i = 0; i < seats.length; i++) {
            Seat seat1 = seats[i];
            for (int j = i + 1; j < seats.length; j++) {
                Seat seat2 = seats[j];
                AdjacentOption opt = new AdjacentOption(seat1, seat2);
                if (opt.isAdjacent())
                    options.add(opt);
                for (int k = j + 1; k < seats.length; k++) {
                    Seat seat3 = seats[k];
                    opt = new AdjacentOption(seat1, seat2, seat3);
                    if (opt.isAdjacent())
                        options.add(opt);
                }
            }
        }
        AdjacentOption[] adjacentOptions = options.toArray(new AdjacentOption[0]);
        Arrays.sort(adjacentOptions);
        return adjacentOptions;
    }

    public boolean isAdjacent() {
        if (seats.length == 2) {
            return (adjType12 != null);
        }
        if (seats.length == 3) {
            return ((adjType12 != null && adjType23 != null) || (adjType12 != null && adjType13 != null) || (adjType13 != null && adjType23 != null));
        }
        return false;
    }

    public int size() { return seats.length; }

    public Seat[] getSeats() {
        return seats;
    }
    /*
    * Pair up with another seat to see if combined could be a valid option, it need to be in the same cabin, but not the same seat.
     */
    public boolean pairSeat(Seat seat) {
        for (int i = 0; i < seats.length; i++) {
            if (!seats[i].isSameCabin(seat))
                return false;
            if (seats[i].isSame(seat))
                return false;
        }
        return true;
    }
    public double getScore() {
        return score;
    }

    public String getAdjType12() { return adjType12; }

    public int compareTo(AdjacentOption obj) {
        if (seats.length < obj.seats.length) {
            return 1;
        } else if (seats.length == obj.seats.length) {
            if (score < obj.score)
                return 1;
            else if (score == obj.score)
                return 0;
            else
                return -1;
        } else {
            return -1;
        }
    }
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < seats.length; i++)
            str.append(seats[i].getSeatId()).append(" ");
        str.append(String.format("%.1f", score)).append(" ");
        return str.toString();
    }
}
