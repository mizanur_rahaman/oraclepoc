package com.delta.seat.api.selection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class FlightLeg {
    private static final Logger log = LogManager.getLogger(FlightLeg.class);
    private String departAirport;
    private String arrivalAirport;
    private String marketCarrierCd;
    private String marketFltNum;
    private String opAsCarrierCd;
    private String opAsFltNum;
    private String departureDate;
    private String cosCode;
    private String associationId;
    private String segmentNum;
    private String idRefText;
    private Cabin[] cabins;
    private HashMap<String, Seat> seatMap = new HashMap<>();

    public FlightLeg() {}
    public FlightLeg(String depAirport, String arrAirport, String marketCrrCd, String marketFltNb, String opasCrrCd, String opasFltNb, String depDate) {
        departAirport = depAirport;
        arrivalAirport = arrAirport;
        marketCarrierCd = marketCrrCd;
        marketFltNum = marketFltNb;
        opAsCarrierCd = opasCrrCd;
        opAsFltNum = opasFltNb;
        departureDate = depDate;
    }
    public FlightLeg(com.delta.seat.api.vo.FlightSegmentInput voFltInfoRqst) {
        com.delta.seat.api.vo.SeatMap voSeatMap = voFltInfoRqst.getSeatMap();
        departAirport = voFltInfoRqst.getOriginAirportCode();
        arrivalAirport = voFltInfoRqst.getDestinationAirportCode();
        marketCarrierCd = voFltInfoRqst.getMarketingCarrierCode();
        marketFltNum = voFltInfoRqst.getMarketingFlightNum();
        opAsCarrierCd = voFltInfoRqst.getOperatingCarrierCode();
        opAsFltNum = voFltInfoRqst.getOperatingFlightNum();
        departureDate = voFltInfoRqst.getScheduledDepartureLocalDate();
        cosCode = voFltInfoRqst.getClassOfServiceCode();
        associationId = voFltInfoRqst.getAssociationId();
        segmentNum = voFltInfoRqst.getFlightSegmentNum();
//        idRefText = voFltInfoRqst.getFlightIdRefText();
        List<com.delta.seat.api.vo.Compartment> voCabinList = voSeatMap.getCompartments();
        cabins = new Cabin[voCabinList.size()];
        int index = 0;
        for (com.delta.seat.api.vo.Compartment voCabin: voCabinList) {
            cabins[index] = new Cabin(voCabin);
            seatMap.putAll(cabins[index].getSeatMap());
            index++;
        }
    }

    public com.delta.seat.api.vo.FlightSegmentOutput getFlightInfoRspnsVO() {
        com.delta.seat.api.vo.FlightSegmentOutput voFltInfoResp = new com.delta.seat.api.vo.FlightSegmentOutput();
        voFltInfoResp.setDestinationAirportCode(arrivalAirport);
        voFltInfoResp.setAssociationId(associationId);
        voFltInfoResp.setClassOfServiceCode(cosCode);
        voFltInfoResp.setOriginAirportCode(departAirport);
        voFltInfoResp.setScheduledDepartureLocalDate(departureDate);
        voFltInfoResp.setMarketingCarrierCode(marketCarrierCd);
        voFltInfoResp.setMarketingFlightNum(marketFltNum);
        voFltInfoResp.setOperatingCarrierCode(opAsCarrierCd);
        voFltInfoResp.setOperatingFlightNum(opAsFltNum);
        voFltInfoResp.setFlightSegmentNum(segmentNum);
        //TODO: Bereket wants this under seat selection block
//        voFltInfoResp.setFlightIdRefText(idRefText);
        return voFltInfoResp;
    }


    public void setCabins(List<Cabin> cabinList) {
        cabins = cabinList.toArray(new Cabin[0]);
        for (int i = 0; i < cabins.length; i++) {
            Seat[] seats = cabins[i].getSeats();
            for (int j = 0; j < seats.length; j++)
                seatMap.put(seats[j].getSeatId(), seats[j]);
        }
    }
    public Cabin[] getCabins() {
        return cabins;
    }
    public Map<String, Seat> getSeatMap() {
        return seatMap;
    }
    public void createAdjacentSeats() {
        if (cabins == null)
            return;
        for (int i = 0; i < cabins.length; i++)
            cabins[i].createAdjacentSeats();
    }

    public void createAdjacentOptions(String transactionId) {
        if (cabins == null)
            return;
        for (int i = 0; i < cabins.length; i++) {
            cabins[i].createAdjacentOptions();
        }
        debugAdjacentOptions(transactionId);
    }

    public void debug(String transactionId) {
        log.debug(transactionId + " - " + marketCarrierCd + marketFltNum + "/" + opAsCarrierCd + opAsFltNum + " " + departureDate + " " + departAirport + "-" + arrivalAirport);
        for (int i = 0; i < cabins.length; i++) {
            cabins[i].debug(transactionId);
        }
    }
    public void debugInventory(String transactionId) {
        StringBuilder str = new StringBuilder();
        str.append(transactionId).append(" - ").append(marketCarrierCd);
        str.append(marketFltNum).append("/").append(opAsCarrierCd).append(opAsFltNum).append(" ");
        str.append(departureDate).append(" ").append(departAirport).append("-").append(arrivalAirport).append("  ");
        for (int i = 0; i < cabins.length; i++) {
            str.append(cabins[i].getCabinCd()).append(" ").append(cabins[i].getInventoryCnt()).append(", ");
        }
        log.info(str.toString());
    }
    public void debugAdjacentSeats() {
        StringBuilder str = new StringBuilder();
        str.append(marketCarrierCd).append(marketFltNum).append("/").append(opAsCarrierCd).append(opAsFltNum);
        log.debug(str);
        for (int i = 0; i < cabins.length; i++) {
            cabins[i].debugAdjacentSeats();
        }
    }
    public void debugAdjacentOptions(String transactionId) {
        log.debug(transactionId + " - " + marketCarrierCd + marketFltNum + "/" + opAsCarrierCd + opAsFltNum);
        for (int i = 0; i < cabins.length; i++) {
            cabins[i].debugAdjacentOptions(transactionId);
        }
    }
}
