package com.delta.seat.api.selection;

import com.delta.seat.api.vo.SeatRow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;

/**
 * Compute seat related attributes
 */
public class SeatAttributes {
    private static final Logger log = LogManager.getLogger(SeatAttributes.class);

    private SeatAttributes() {}
    //row by row
    public static void computeAttributes(List<com.delta.seat.api.vo.Seat> voSeatList, Map<String, Seat> seatMap, int aisleCount) {
        List<Seat> rowSeatList = new ArrayList<>();
        int maxColNumber = 0;
        for (com.delta.seat.api.vo.Seat voSeat: voSeatList) {
            if (voSeat.getSeatId()==null)
                continue;
            String seatId = voSeat.getSeatId();
            seatId = (seatId.length()==2)?"0"+seatId : seatId;
            Seat seat = seatMap.get(seatId);
            if (maxColNumber < seat.getColNumber())
                maxColNumber = seat.getColNumber();
            rowSeatList.add(seat);
        }
        int middle = (int)Math.round((maxColNumber+0.5)/2);
        int aisle1colNum = 0;
        int aisle2colNum = 0;
        if (aisleCount == 2) {
            rowSeatList.sort(new Seat());
            int lastColIndex = 0;
            for (Seat seat: rowSeatList) {
                if (seat.getColNumber()-lastColIndex > 1) {
                    if (aisle1colNum == 0)
                        aisle1colNum = lastColIndex + 1;
                    else if (aisle2colNum == 0)
                        aisle2colNum = lastColIndex + 1;
                    else
                        log.info("Seat " + seat.getSeatId() + " has wrong column data " + seat.getColNumber());
                    lastColIndex = seat.getColNumber();
                }
            }
        }
        for (Seat seat: rowSeatList) {
            if (seat.getColNumber() <= middle)
                seat.setAircraftSide('L');
            else
                seat.setAircraftSide('R');
            if (aisleCount == 1) {
                seat.setRowSection('S');
            } else {
                if (seat.getColNumber() < aisle1colNum || seat.getColNumber() > aisle2colNum)
                    seat.setRowSection('S');
                else
                    seat.setRowSection('C');
            }
        }
    }

    public static int computeAisleCount(List<SeatRow> voRowList) {
        if (voRowList.size() == 0)
            return 0;
        int aisleCount = 0;
        com.delta.seat.api.vo.SeatRow voRow = voRowList.get(0);
        List<com.delta.seat.api.vo.Seat> voSeatList = voRow.getSeats();
        int currIndex = -1;
        if (voSeatList == null)
            return 0;
        for (com.delta.seat.api.vo.Seat voSeat: voSeatList) {
            int indexNum = Integer.parseInt(voSeat.getIndexNum());
            if (currIndex == -1) {
                currIndex = indexNum;
            } else {
                if (indexNum - currIndex > 1) {
                    aisleCount++;
                    currIndex = indexNum;
                } else {
                    currIndex = indexNum;
                }
            }
        }
        return aisleCount;
    }
}
