package com.delta.seat.api.selection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Pnr {
    private static final Logger log = LogManager.getLogger(Pnr.class);
    private String offerId;
    private String pnr;
    private Psgr[] psgrs;
    private Option[] options;
    private Option selection;

    public Pnr(String pnr) {
        this.pnr = pnr;
    }
    public Pnr(com.delta.seat.api.vo.Offer voOffer, Map<String, Seat> seatMap, String transactionId) {
        offerId = voOffer.getOfferId();
        pnr = voOffer.getRecordLocatorId();
        List<com.delta.seat.api.vo.Passenger> voPassengerList = voOffer.getPassengers();
        psgrs = new Psgr[voPassengerList.size()];
        int index = 0;
        for (com.delta.seat.api.vo.Passenger voPassenger: voPassengerList) {
            psgrs[index++] = new Psgr(pnr, voPassenger, seatMap, transactionId);
        }
    }

    public List<com.delta.seat.api.vo.SeatSelection> getSeatSelectionListVO(String fltIdRef) {
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelectionList = new ArrayList<>();
        if (selection == null)
            return voSeatSelectionList;
        PsgrSeat[] psgrSeats = selection.getPsgrSeats();
        for (int i = 0; i < psgrs.length; i++) {
            com.delta.seat.api.vo.SeatSelection voSeatSelection = new com.delta.seat.api.vo.SeatSelection();
            voSeatSelection.setOfferId(offerId);
            psgrs[i].getSeatSelectionVO(voSeatSelection, fltIdRef);
            psgrSeats[i].getSeatSelectionVO(voSeatSelection);
            voSeatSelectionList.add(voSeatSelection);
        }
        return voSeatSelectionList;
    }

    public String getPnr() { return pnr; }
    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
    public Psgr[] getPsgrs() {
        return psgrs;
    }
    public void setPsgrs(List<Psgr> psgrList) {
        psgrs = psgrList.toArray(new Psgr[0]);
    }
    public Psgr getPsgr(String travelerId) {
        for (int i = 0; i < psgrs.length; i++) {
            if (travelerId.equals(psgrs[i].getTravelerId()))
                return psgrs[i];
        }
        return null;
    }
    public void createPreferenceScore() {
        for (int i = 0; i < psgrs.length; i++) {
            psgrs[i].createPreferenceScore();
        }
    }
    public void createSeatScores() {
        for (int i = 0; i < psgrs.length; i++)
            psgrs[i].createSeatScore();
    }

    public void refreshPsgrSeats() {
        for (int i = 0; i < psgrs.length; i++) {
            psgrs[i].refreshPsgrSeats();
        }
    }
    public void createOptions(FlightLeg fltLeg, String transactionId) {
        Policy policy = Policy.getInstance(transactionId.substring(0, 4));
        //key = ordered seat id, eg. 18C18E24D, etc.
        HashMap<String, Option> optionMap = new HashMap<>();
        if (psgrs.length == 1) {
            PsgrSeat[] psgrSeats = psgrs[0].getPsgrSeats();
            for (int i = 0; i < psgrSeats.length; i++) {
                Option opt = new Option(psgrs[0], psgrSeats[i]);
                optionMap.put(opt.getSortedSeatIds(), opt);
            }
        } else if (psgrs.length == 2) {
            //adjacent seat options
            Cabin[] cabins = fltLeg.getCabins();
            for (int i = 0; i < cabins.length; i++) {
                cabins[i].createOptions(psgrs, optionMap, policy);
            }
            //non-adjacent seat options
            createOptionsMultiSeats(optionMap);
        } else if (psgrs.length == 3) {
            //adjacent seat options
            Cabin[] cabins = fltLeg.getCabins();
            for (int i = 0; i < cabins.length; i++) {
                cabins[i].createOptions(psgrs, optionMap, policy);
            }
            //part-adjacent seat options
            for (int i = 0; i < cabins.length; i++) {
                cabins[i].createOptionsMultiSeats(psgrs, optionMap, policy);
            }
            //non-adjacent seat options
            createOptionsMultiSeats(optionMap);
        }
        options = optionMap.values().toArray(new Option[0]);
        Arrays.sort(options);
    }

    private void createOptionsMultiSeats(HashMap<String, Option> optionMap) {
        if (psgrs.length == 2) {
            createOptionsMultiSeats2Pax(optionMap);
        } else if (psgrs.length == 3) {
            createOptionsMultiSeats3Pax(optionMap);
        }
    }

    private void createOptionsMultiSeats2Pax(HashMap<String, Option> optionMap) {
        PsgrSeat[] psgrSeats0 = psgrs[0].getPsgrSeats();
        for (int i = 0; i < psgrSeats0.length; i++) {
            PsgrSeat pSeat0 = psgrSeats0[i];
            PsgrSeat[] psgrSeats1 = psgrs[1].getPsgrSeats();
            for (int j = 0; j < psgrSeats1.length; j++) {
                PsgrSeat pSeat1 = psgrSeats1[j];
                if (!pSeat0.pairSeat(pSeat1))
                    continue;
                PsgrSeat[] psgrSeats = {pSeat0, pSeat1};
                Option opt = new Option(psgrs, psgrSeats);
                Option.putOption(optionMap, opt);
            }
        }
    }

    private void createOptionsMultiSeats3Pax(HashMap<String, Option> optionMap) {
        PsgrSeat[] psgrSeats0 = psgrs[0].getPsgrSeats();
        for (int i = 0; i < psgrSeats0.length; i++) {
            PsgrSeat pSeat0 = psgrSeats0[i];
            PsgrSeat[] psgrSeats1 = psgrs[1].getPsgrSeats();
            for (int j = 0; j < psgrSeats1.length; j++) {
                PsgrSeat pSeat1 = psgrSeats1[j];
                if (!pSeat0.pairSeat(pSeat1))
                    continue;
                PsgrSeat[] psgrSeats2 = psgrs[2].getPsgrSeats();
                for (int k = 0; k < psgrSeats2.length; k++) {
                    PsgrSeat pSeat2 = psgrSeats2[k];
                    if (!pSeat2.pairSeats(pSeat0, pSeat1))
                        continue;
                    PsgrSeat[] psgrSeats = {pSeat0, pSeat1, pSeat2};
                    Option opt = new Option(psgrs, psgrSeats);
                    Option.putOption(optionMap, opt);
                }
            }
        }
    }
    public Option[] getOptions() { return options; }
    public Option selectOption(boolean masterList) {
        if (options.length == 0)
            return null;
        if (masterList) {
            for (int i = 0; i < options.length; i++) {
                Cabin cabin = options[i].getPsgrSeats()[0].getSeat().getCabin();
                if (psgrs.length <= cabin.getInventoryCnt()) {
                    cabin.decreaseInventoryCnt(psgrs.length);
                    selection = options[i];
                    break;
                }
            }
        } else {
            selection = options[0];
        }
        return selection;
    }
    public void markSelection() {
        if (selection != null)
            selection.markSelected();
    }
    public Option getSelection() { return selection; }

    public void debug(String transactionId) {
        log.debug(transactionId + " - " + pnr + " " + offerId + " seat scores:");
        for (int i = 0; i < psgrs.length; i++) {
            psgrs[i].debug(transactionId);
        }
    }

    public void debugOption(String transactionId) {
        log.debug(transactionId + " - pnr: " + pnr + " options:");
        int maxOptNum = 20;
        if (maxOptNum > options.length)
            maxOptNum = options.length;
        for (int i = 0; i < maxOptNum; i++) {
            log.debug(transactionId + " - " + options[i]);
        }
    }
}
