package com.delta.seat.api.selection;

import java.util.*;

public class PsgrSeat implements Comparable<PsgrSeat> {
    private String offerItemNum;
    private Seat seat;
    private double score;

    public PsgrSeat(Seat seat) { this.seat = seat; }
    public PsgrSeat(Seat seat, com.delta.seat.api.vo.Seatoffer voSeatOffer) {
        this.seat = seat;
        offerItemNum = voSeatOffer.getOfferItemNum();
    }

    public void getSeatSelectionVO(com.delta.seat.api.vo.SeatSelection voSeatSelection) {
        voSeatSelection.setOfferItemNum(offerItemNum);
        voSeatSelection.setSelectedSeatId(seat.getSeatId());
    }
    public Seat getSeat() {
        return seat;
    }
    public boolean pairSeat(PsgrSeat psgrSeat) {
        if (seat.isSame(psgrSeat.seat))
            return false;
        if (!seat.isSameCabin(psgrSeat.seat))
            return false;
        return true;
    }
    public boolean pairSeats(PsgrSeat psgrSeat1, PsgrSeat psgrSeat2) {
        return pairSeat(psgrSeat1) && pairSeat(psgrSeat2);
    }
    public void setScore(double score) {
        this.score = score;
    }
    public double getScore() { return score; }
    public int compareTo(PsgrSeat obj) {
        if (score < obj.score)
            return 1;
        else if (score == obj.score)
            return 0;
        else
            return -1;
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(seat.getSeatId()).append(" ");
        str.append(String.format("%.1f", score)).append(" ");
        return str.toString();
    }
}