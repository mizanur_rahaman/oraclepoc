package com.delta.seat.api.selection;

import com.delta.seat.api.dao.PolicyParm;
import com.delta.seat.api.dao.PolicyParmDao;

import java.util.List;

public class Policy {
    public static final String DEFAULT = "DEFAULT";
    public static final String TEST = "TEST";
    private static Policy defaultPolicy;
    private static Policy testPolicy;

    private double coefficientSeatScore = 1;
    private double coefficientAdjacentScore = 10;
    private double adjacencyScoreSideBySide = 6;
    private double adjacencyScoreAcrossAisle = 5;
    private double adjacencyScoreSameSectSameRow = 4;
    private double adjacencyScoreSameSectNextRow = 1;

    private double preserveAisleSeatFactor = 9;
    private double preserveWindowSeatFactor = 9;
    private double newAisleSeatFactor = 5;
    private double newWindowSeatFactor = 4.5;
    private double newPreferredSeatFactor = 8;
    private double preserveRowFactorCpmt3 = 10;
    private double preserveRowSlopeFactor =60; //aisle -> middle

    public static void reload(PolicyParmDao policyParmDao) {
        Policy policy = getInstance();
        policy.reloadWithPolicyParams(policyParmDao);
    }

    public static void reloadDefaultPolicy() {
        defaultPolicy = null;
        getInstance();
    }

    public static Policy getInstance() { return getInstance(DEFAULT);}
    public static Policy getInstance(String policyName) {
        String name = policyName.toUpperCase();
        switch (name) {
            case DEFAULT:
                if (defaultPolicy == null) {
                    defaultPolicy = new Policy();
                }
                return defaultPolicy;
            case TEST:
                if (testPolicy == null) {
                    testPolicy = new Policy();
                    testPolicy.coefficientAdjacentScore = 1;
                }
                return testPolicy;
             default:
                return defaultPolicy;
        }
    }

    private Policy() {}

    private void reloadWithPolicyParams(PolicyParmDao policyParmDao) {
        List<PolicyParm> policyParms = policyParmDao.getPolicyParms();
        for (PolicyParm policyParm: policyParms) {
            String parmName = policyParm.getParmName();
            String value = policyParm.getParmValue();
            switch (parmName) {
                case "coefficientAdjacentScore":
                    coefficientAdjacentScore = Integer.parseInt(value);
                    break;
                case "adjacencyScoreSideBySide":
                    adjacencyScoreSideBySide = Integer.parseInt(value);
                    break;
                case "adjacencyScoreAcrossAisle":
                    adjacencyScoreAcrossAisle = Integer.parseInt(value);
                    break;
                case "adjacencyScoreSameSectSameRow":
                    adjacencyScoreSameSectSameRow = Integer.parseInt(value);
                    break;
                case "adjacencyScoreSameSectNextRow":
                    adjacencyScoreSameSectNextRow = Integer.parseInt(value);
                    break;
            }
        }
    }

    public double getCoefficientSeatScore() {
        return coefficientSeatScore;
    }

    public double getCoefficientAdjacentScore() {
        return coefficientAdjacentScore;
    }
    public void setCoefficientAdjacentScore(double coefficientAdjacentScore) {
        this.coefficientAdjacentScore = coefficientAdjacentScore;
    }
    public double getAdjacencyScoreSideBySide() {
        return adjacencyScoreSideBySide;
    }

    public void setAdjacencyScoreSideBySide(double adjacencyScoreSideBySide) {
        this.adjacencyScoreSideBySide = adjacencyScoreSideBySide;
    }

    public double getAdjacencyScoreAcrossAisle() {
        return adjacencyScoreAcrossAisle;
    }

    public void setAdjacencyScoreAcrossAisle(double adjacencyScoreAcrossAisle) {
        this.adjacencyScoreAcrossAisle = adjacencyScoreAcrossAisle;
    }

    public double getAdjacencyScoreSameSectSameRow() {
        return adjacencyScoreSameSectSameRow;
    }

    public void setAdjacencyScoreSameSectSameRow(double adjacencyScoreSameSectSameRow) {
        this.adjacencyScoreSameSectSameRow = adjacencyScoreSameSectSameRow;
    }

    public double getAdjacencyScoreSameSectNextRow() {
        return adjacencyScoreSameSectNextRow;
    }

    public void setAdjacencyScoreSameSectNextRow(double adjacencyScoreSameSectNextRow) {
        this.adjacencyScoreSameSectNextRow = adjacencyScoreSameSectNextRow;
    }

    public double getPreserveAisleSeatFactor() { return preserveAisleSeatFactor; }

    public double getPreserveWindowSeatFactor() {
        return preserveWindowSeatFactor;
    }

    public double getNewAisleSeatFactor() {
        return newAisleSeatFactor;
    }

    public double getNewWindowSeatFactor() {
        return newWindowSeatFactor;
    }

    public double getNewPreferredSeatFactor() {
        return newPreferredSeatFactor;
    }

    public double getPreserveRowFactorCpmt3() {
        return preserveRowFactorCpmt3;
    }

    public double getPreserveRowSlopeFactor() {
        return preserveRowSlopeFactor;
    }

}
