package com.delta.seat.api.selection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class Psgr {
    private static final Logger log = LogManager.getLogger(Psgr.class);
    public static final String PREFERENCE_AISLE = "A";
    public static final String PREFERENCE_WINDOW = "W";
    public static final String PREFERENCE_NONE = "N";
    private String firstName;
    private String lastName;
    private String travelerId;
    private String recordLocatorId;
    private String preferenceCd;    //Aisle, Window, None
    private HashMap<String, PsgrSeat> psgrSeatMap = new HashMap<>();
    private PsgrSeat[] psgrSeats;

    public Psgr(String travelerId) {
        this.travelerId = travelerId;
    }
    public Psgr(String pnr, com.delta.seat.api.vo.Passenger voPassenger, Map<String, Seat> seatMap, String transactionId) {
        firstName = voPassenger.getFirstNameNum();
        lastName = voPassenger.getLastNameNum();
        travelerId = voPassenger.getTravelerId();
        recordLocatorId = pnr;
        List<com.delta.seat.api.vo.LoyaltyMemberPreference> voLoyaltyPrefList = voPassenger.getLoyaltyMemberPreferences();
        com.delta.seat.api.vo.LoyaltyMemberPreference voLoyaltyPref = voLoyaltyPrefList.get(0);
        preferenceCd = voLoyaltyPref.getPreferenceCode();
        List<com.delta.seat.api.vo.Seatoffer> voSeatOfferList = voPassenger.getSeatOffers();
        for (com.delta.seat.api.vo.Seatoffer voSeatOffer: voSeatOfferList) {
            double price = voSeatOffer.getPaymentAmt()==null?0.0:Double.parseDouble(voSeatOffer.getPaymentAmt());
            String seatId = voSeatOffer.getSeatId().length()==2?"0"+voSeatOffer.getSeatId():voSeatOffer.getSeatId();
            if (price != 0.0) {
                log.info(transactionId + " - " + firstName + " " + lastName + " seat offer " + seatId + " priceAmt " + price + ", invalid, ignored.");
                continue;
            }
            Seat seat = seatMap.get(seatId);
            if (seat == null) {
                log.info(transactionId + " - " + firstName + " " + lastName + " seat offer " + seatId + " doesn't exist in seat map, ignored.");
                continue;
            }
            if (seat.isExit()) {
                log.info(transactionId + " - " + firstName + " " + lastName + " seat offer " + seatId + " is an exit seat, ignored.");
                continue;
            }
            seat.setOffered(true);
            seat.setSeatScores(voSeatOffer.getSeatScoreNum(), preferenceCd);
            PsgrSeat psgrSeat = new PsgrSeat(seat, voSeatOffer);
            psgrSeatMap.put(seat.getSeatId(), psgrSeat);
        }
        psgrSeats = psgrSeatMap.values().toArray(new PsgrSeat[0]);
        Arrays.sort(psgrSeats);
    }


    public String getName() {
        return firstName +  " " + lastName;
    }
    public String getTravelerId() { return travelerId; }
    public Map<String, PsgrSeat> getPsgrSeatMap() { return psgrSeatMap; }

    public void getSeatSelectionVO(com.delta.seat.api.vo.SeatSelection voSeatSelection, String fltIdRef) {
        voSeatSelection.setFirstNameNum(firstName);
        voSeatSelection.setLastNameNum(lastName);
        voSeatSelection.setTravelerId(travelerId);
        voSeatSelection.setRecordLocatorId(recordLocatorId);
        voSeatSelection.setFlightIdRefText(fltIdRef);
    }
    public void createPreferenceScore() {
        Policy policy = Policy.getInstance();
        for (int i = 0; i < psgrSeats.length; i++) {
            Seat seat = psgrSeats[i].getSeat();
            double windowAisleFactor = 0;
            if (PREFERENCE_AISLE.equals(preferenceCd)) {
                if (seat.isAisle())
                    windowAisleFactor = policy.getPreserveAisleSeatFactor();
                else if (seat.isWindow())
                    windowAisleFactor = policy.getNewWindowSeatFactor();
            } else if (PREFERENCE_WINDOW.equals(preferenceCd)) {
                if (seat.isWindow())
                    windowAisleFactor = policy.getPreserveWindowSeatFactor();
                else if (seat.isAisle())
                    windowAisleFactor = policy.getNewAisleSeatFactor();
            } else {
                if (seat.isAisle())
                    windowAisleFactor = policy.getNewAisleSeatFactor();
                else if (seat.isWindow())
                    windowAisleFactor = policy.getNewWindowSeatFactor();
            }
            double preferredSeatFactor = 0;
            if (seat.isPreferred())
                preferredSeatFactor = policy.getNewPreferredSeatFactor();
            double rowDiffFactor = -policy.getPreserveRowFactorCpmt3() * (Math.exp(seat.getCabinRowPct()/policy.getPreserveRowSlopeFactor())-1)/(Math.exp(100/ policy.getPreserveRowSlopeFactor())-1);

            double score = windowAisleFactor + preferredSeatFactor + rowDiffFactor;
            psgrSeats[i].setScore(score);
        }
    }

    public void createSeatScore() {
        for (int i = 0; i < psgrSeats.length; i++) {
            Seat seat = psgrSeats[i].getSeat();
            double seatScore = seat.getSeatScore(preferenceCd);
            psgrSeats[i].setScore(seatScore);
        }
    }

    public PsgrSeat getPsgrSeat(Seat seat) {
        return psgrSeatMap.get(seat.getSeatId());
    }
    public PsgrSeat[] getPsgrSeats() { return psgrSeats; }
    public void refreshPsgrSeats() {
        List<PsgrSeat> psgrSeatList = new ArrayList<PsgrSeat>();
        for (int i = 0; i < psgrSeats.length; i++) {
            if (psgrSeats[i].getSeat().isAvailable())
                psgrSeatList.add(psgrSeats[i]);
        }
        psgrSeats = psgrSeatList.toArray(new PsgrSeat[0]);
    }
    public void debug(String transactionId) {
        log.debug(transactionId + " - " + firstName + " " + lastName + " " + preferenceCd);
        for (int i = 0; i < psgrSeats.length; i++) {
            log.debug(transactionId + " -   " + psgrSeats[i].getSeat().getSeatId() + " " + psgrSeats[i].getScore());
        }
    }
}
