package com.delta.seat.api.config;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

import com.delta.seat.api.controller.SeatApiController;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class RuntimeEnvironmentPropertiesConfigurer extends
        PropertyPlaceholderConfigurer implements InitializingBean {

    private static final Logger log = LogManager.getLogger(RuntimeEnvironmentPropertiesConfigurer.class);

    public static final String PROP_FILE_LOCATION = "prop.file";

    public void afterPropertiesSet() throws IOException {

        String propfileLocation = System.getProperty(PROP_FILE_LOCATION);

        Properties existingProperties = System.getProperties();

        if (propfileLocation == null || propfileLocation.isEmpty()){
            System.out.println("propfileLocation [prop.file] wasn't set. So, use the default ST environment");
            if (existingProperties == null ){
                existingProperties = new Properties();
            }
            existingProperties.load(RuntimeEnvironmentPropertiesConfigurer.class.getResourceAsStream("/st/prop.properties"));
        }
        else {
            System.out.println("propfileLocation [prop.file] was set. Default prop file location is: " + propfileLocation);
            File file = new File(propfileLocation);
            Scanner scanner = null;
            try {
                scanner = new Scanner(file, "UTF-8");
                existingProperties = System.getProperties();
                while (scanner.hasNextLine()) {
                    String readLine = scanner.nextLine();
                    if (!readLine.startsWith("#")) {
                        if (readLine.contains("=")) {
                            String[] split = readLine.split("=");
                            if (split[0] != null && split[1] != null) {
                                existingProperties.setProperty(split[0], split[1]);
                            }

                        }
                    }
                    System.setProperties(existingProperties);
                }

            } catch (FileNotFoundException e) {
                throw new RuntimeException(String.format(
                        "Trouble reading property file [%s] during start up.",
                        propfileLocation));

            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        }
    }
}
