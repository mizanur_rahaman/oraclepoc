package com.delta.seat.api.dao;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SeatTransactionRowMapper implements RowMapper<SeatTransaction>{
    public SeatTransaction mapRow(ResultSet rs, int rowNum) throws SQLException {
        SeatTransaction seatTransaction = new SeatTransaction();
        seatTransaction.setTransactionId(rs.getString("transactionId"));
        seatTransaction.setOperatingFlightNum(rs.getString("operatingFlightNum"));
        seatTransaction.setScheduledDepartureLocalDate(rs.getString("scheduledDepartureLocalDate"));
        Clob requestClob = rs.getClob("offers");
        seatTransaction.setOffers(requestClob.getSubString(1, (int)requestClob.length()));
        System.out.println(rowNum + " d " +seatTransaction);
        return seatTransaction;
    }
}
