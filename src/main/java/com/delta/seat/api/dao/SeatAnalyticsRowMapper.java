package com.delta.seat.api.dao;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SeatAnalyticsRowMapper implements RowMapper<SeatAnalytics>{
    public SeatAnalytics mapRow(ResultSet rs, int rowNum) throws SQLException {
        SeatAnalytics seatAnalytics = new SeatAnalytics();
        seatAnalytics.setTransactionId(rs.getString("transactionId"));
        seatAnalytics.setFlightNo(rs.getString("flightNo"));
        seatAnalytics.setFlightOrigin(rs.getString("flightOrigin"));
        seatAnalytics.setFlightDestination(rs.getString("flightDestination"));
        Clob requestClob = rs.getClob("requestMessage");
        seatAnalytics.setRequestMessage(requestClob.getSubString(1, (int)requestClob.length()));
        Clob responseClob = rs.getClob("responseMessage");
        seatAnalytics.setResponseMessage(responseClob.getSubString(1, (int)responseClob.length()));
        return seatAnalytics;
    }
}