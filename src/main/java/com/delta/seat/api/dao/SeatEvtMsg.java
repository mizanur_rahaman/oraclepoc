package com.delta.seat.api.dao;

import java.sql.Timestamp;
public class SeatEvtMsg {

    private String transactionid;
    private Timestamp crtnGts;
    private String requestClob;
    private String responseClob;
    private String serverName;
    private long policyId;
    private long startTime;
    private long endTime;
    private String sengEventTypeCode;
    private String appId;

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public Timestamp getCrtnGts() {
        return crtnGts;
    }

    public void setCrtnGts(Timestamp crtnGts) {
        this.crtnGts = crtnGts;
    }

    public String getRequestClob() {
        return requestClob;
    }

    public void setRequestClob(String requestClob) {
        this.requestClob = requestClob;
    }

    public String getResponseClob() {
        return responseClob;
    }

    public void setResponseClob(String responseClob) {
        this.responseClob = responseClob;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(long policyId) {
        this.policyId = policyId;
    }



    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getSengEventTypeCode() {
        return sengEventTypeCode;
    }

    public void setSengEventTypeCode(String sengEventTypeCode) {
        this.sengEventTypeCode = sengEventTypeCode;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
