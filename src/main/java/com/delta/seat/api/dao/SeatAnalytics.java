package com.delta.seat.api.dao;

import java.util.List;

public class SeatAnalytics {

	private String transactionId;
	private int singlePartyPNRs;
	private int  multiPartyPNRs;
	
	/*
	*(PNR with >1 pax under the age of 14yrs)
	*/
	private int familyPNRs;
	
	/*
	 * (loyaltyTierCode)
	 */
	private String skyMilesStatus; 
	private String flightOrigin;
	private String flightDestination;
	private String flightNo;
	private String requestMessage;
	private String responseMessage;
	private List<String> responsePNRList;
	private List<String> unseatedPaxPNRList;
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public int getSinglePartyPNRs() {
		return singlePartyPNRs;
	}
	public void setSinglePartyPNRs(int singlePartyPNRs) {
		this.singlePartyPNRs = singlePartyPNRs;
	}
	public int getMultiPartyPNRs() {
		return multiPartyPNRs;
	}
	public void setMultiPartyPNRs(int multiPartyPNRs) {
		this.multiPartyPNRs = multiPartyPNRs;
	}
	public int getFamilyPNRs() {
		return familyPNRs;
	}
	public void setFamilyPNRs(int familyPNRs) {
		this.familyPNRs = familyPNRs;
	}
	public String getSkyMilesStatus() {
		return skyMilesStatus;
	}
	public void setSkyMilesStatus(String skyMilesStatus) {
		this.skyMilesStatus = skyMilesStatus;
	}
	public String getFlightOrigin() {
		return flightOrigin;
	}
	public void setFlightOrigin(String flightOrigin) {
		this.flightOrigin = flightOrigin;
	}
	public String getFlightDestination() {
		return flightDestination;
	}
	public void setFlightDestination(String flightDestination) {
		this.flightDestination = flightDestination;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public String getRequestMessage() {
		return this.requestMessage;
	}
	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}
	public String getResponseMessage() {
		return this.responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public List<String> getResponsePNRList() {
		return responsePNRList;
	}
	public void setResponsePNRList(List<String> responsePNRList) {
		this.responsePNRList = responsePNRList;
	}
	public List<String> getUnseatedPaxPNRList() {
		return unseatedPaxPNRList;
	}
	public void setUnseatedPaxPNRList(List<String> unseatedPaxPNRList) {
		this.unseatedPaxPNRList = unseatedPaxPNRList;
	}
}
