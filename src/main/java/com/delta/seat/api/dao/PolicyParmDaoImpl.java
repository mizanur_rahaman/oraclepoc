package com.delta.seat.api.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import com.delta.seat.aspect.RetryOnFailure;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PolicyParmDaoImpl implements PolicyParmDao {

    @Resource(name = "seatdbJdbcTemplate")
    private JdbcTemplate seatdbJdbcTemplate;

    private static final String getSeatEnginePolicy = "select * from seng_plcy_parm where seng_plcy_id=(select max(SENG_PLCY_ID) LATEST_SENG_PLCY_ID from seng_plcy_parm)";

    private static final String getMaxPolicyId ="select max(SENG_PLCY_ID) LATEST_SENG_PLCY_ID from seng_plcy_parm";

    @Override
    @RetryOnFailure
    public List<PolicyParm> getPolicyParms() {
        List<PolicyParm> policyParms = new ArrayList<PolicyParm>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection= null;
        try {
            connection = seatdbJdbcTemplate.getDataSource().getConnection();
            stmt = connection.prepareStatement(getSeatEnginePolicy);
            rs = stmt.executeQuery();

            while (rs.next()){
                PolicyParm policyParm = new PolicyParm();
                policyParm.setParmName(rs.getString("SENG_PARM_NM"));
                policyParm.setParmValue(rs.getString("SENG_PARM_VAL_TXT"));
                policyParms.add(policyParm);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            if (rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (stmt != null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return policyParms;
    }

    @Override
    public long getMaxPolicyId() {
        long maxPolicyId = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection= null;
        try {
            connection = seatdbJdbcTemplate.getDataSource().getConnection();
            stmt = connection.prepareStatement(getMaxPolicyId);
            rs = stmt.executeQuery();

            while (rs.next()){
                maxPolicyId = rs.getLong("LATEST_SENG_PLCY_ID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            if (rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (stmt != null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return maxPolicyId;
    }

}
