package com.delta.seat.api.dao;
import java.util.List;

public interface PolicyParmDao {

    public List<PolicyParm> getPolicyParms();

    public long getMaxPolicyId();
}
