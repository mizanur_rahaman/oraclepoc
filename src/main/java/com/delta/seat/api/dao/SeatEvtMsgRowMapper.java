package com.delta.seat.api.dao;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class SeatEvtMsgRowMapper implements RowMapper<SeatEvtMsg>
{
    public SeatEvtMsg mapRow(ResultSet rs, int rowNum) throws SQLException {
        SeatEvtMsg seatEvtMsg = new SeatEvtMsg();
        seatEvtMsg.setTransactionid(rs.getString("SENG_REQ_TRNS_ID"));
        seatEvtMsg.setCrtnGts(rs.getTimestamp("CRTN_GTS"));
        Clob requestClob = rs.getClob("SENG_REQ_MSG");
        seatEvtMsg.setRequestClob(requestClob.getSubString(1, (int)requestClob.length()));
        Clob responseClob = rs.getClob("SENG_RSP_MSG");
        seatEvtMsg.setResponseClob(responseClob.getSubString(1, (int)responseClob.length()));
        return seatEvtMsg;
    }

}