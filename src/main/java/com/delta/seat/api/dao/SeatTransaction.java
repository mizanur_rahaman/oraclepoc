package com.delta.seat.api.dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class SeatTransaction {
    private String transactionId;
    private String operatingFlightNum;
    private String scheduledDepartureLocalDate;
    private int totalPnrs;
    private String offers;
    private String offerId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOperatingFlightNum() {
        return operatingFlightNum;
    }

    public void setOperatingFlightNum(String operatingFlightNum) {
        this.operatingFlightNum = operatingFlightNum;
    }

    public String getScheduledDepartureLocalDate() {
        return scheduledDepartureLocalDate;
    }

    public void setScheduledDepartureLocalDate(String scheduledDepartureLocalDate) {
        this.scheduledDepartureLocalDate = scheduledDepartureLocalDate;
    }

    public int getTotalPnrs() {
        int totalPnrCount = 0;
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode obj = mapper.readTree(this.offers);
            totalPnrCount = obj.get("flightSegments").get(0).get("offers").size();
        } catch (Exception e) {
        }
        return totalPnrCount;
    }

    public void setTotalPnrs(int totalPnrs) {

        this.totalPnrs = totalPnrs;
    }

    public String getOffers() {
        return null;
    }

    public void setOffers(String offers) {
        this.offers = offers;
    }

    public String getOfferId() {
        String offerId = "";
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            JsonNode obj = mapper.readTree(this.offers);
            offerId = obj.get("flightSegments").get(0).get("offers").get(0).get("offerId").asText();
        }
        catch (Exception e) {}
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

}
