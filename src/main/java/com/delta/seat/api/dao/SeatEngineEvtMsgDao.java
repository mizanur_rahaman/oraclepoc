package com.delta.seat.api.dao;

import java.io.IOException;
import java.util.List;

import com.delta.seat.api.vo.SeatAnalyticsRequest;
import com.delta.seat.api.vo.SeatSelection;
import com.delta.seat.api.vo.TransactionSelection;

public interface SeatEngineEvtMsgDao {

    public List<String> getSeatEngReqMsgBasedOnOpasFltNumber(String opasFlightNum);

    public void insertSeatEvtMsgRequestResponseJsonData(SeatEvtMsg seatEvtMsg) throws IOException;

    public int updateSeatEvtMsgResponseJsonData(SeatEvtMsg seatEvtMsg) throws IOException;
    public SeatEvtMsg getSeatEvgMsgBasedOntransactionId(String transactionId) throws IOException;

    public List<SeatTransaction> getTransactionsByResourceLocatorId(TransactionSelection transactionSelection);
    public List<SeatAnalytics> getFlightAnalyticsData(SeatAnalyticsRequest seatAnalyticsRequest);
    public List<String> getSeatEngineEventType();

}
