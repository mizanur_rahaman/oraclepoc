package com.delta.seat.api.dao;

import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.delta.seat.api.util.SeatUtil;
import com.delta.seat.api.vo.SeatAnalyticsRequest;
import com.delta.seat.api.vo.TransactionSelection;

@Service
public class SeatEngineEvtMsgDaoImpl implements SeatEngineEvtMsgDao {	

    @Resource(name = "seatdbJdbcTemplate")
    private JdbcTemplate seatdbJdbcTemplate;

    private static final Logger log = LogManager.getLogger(SeatEngineEvtMsgDaoImpl.class);
    
    private static final String getReqMsgBasedOnOpasFltNum = "select  * from SENG_EVT_MSG se where JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].operatingFlightNum')=?";

    private static final String getSeatTransactionSql = "select SENG_REQ_TRNS_ID as transactionId,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].operatingFlightNum') as operatingFlightNum,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].scheduledDepartureLocalDate') as scheduledDepartureLocalDate,SENG_REQ_MSG as offers from SENG_EVT_MSG se";
    private static final String basedByRecordLocatorIdSql = " json_textcontains(SENG_REQ_MSG, '$.flightSegments.offers.recordLocatorId', ?)";
    private static final String basedByOperatingFlightNoSql = " json_textcontains(SENG_REQ_MSG, '$.flightSegments.operatingFlightNum', ?)";
    private static final String basedByScheduleDepartureDateSql = " regexp_like (JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].scheduledDepartureLocalDate' RETURNING VARCHAR2(200) NULL ON ERROR), '[0-9]{4}-[0-1][0-9]-[0-3][0-9]') and to_date(JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].scheduledDepartureLocalDate'   RETURNING VARCHAR2(200) NULL ON ERROR),'YYYY-MM-DD') between  to_date(?,'YYYY-MM-DD') and to_date(?,'YYYY-MM-DD')";
    private static final String getReqMsgBasedTransactionId = "select  * from SENG_EVT_MSG where SENG_REQ_TRNS_ID=?";
    
    private static final String getSeatanalyticsSql = "select SENG_REQ_TRNS_ID as transactionId,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].originAirportCode') as flightOrigin,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].destinationAirportCode') as flightDestination ,JSON_VALUE(SENG_REQ_MSG, '$.flightSegments[0].marketingFlightNum') as flightNo, SENG_REQ_MSG as requestMessage,SENG_RSP_MSG as responseMessage from SENG_EVT_MSG se";

    private static final String insertSengEvtMsgSql = "INSERT INTO SENG_EVT_MSG (SENG_REQ_TRNS_ID,CRTN_GTS,SENG_REQ_MSG,SENG_RSP_MSG,SENG_PLCY_ID,SRVR_NM,SENG_EVT_TYP_CD) VALUES(?,?,?,?,?,?,?)";

    private static final String updateSengEvtMsgSql = "UPDATE SENG_EVT_MSG SET SENG_RSP_MSG = ?, LST_UPDT_GTS = ? WHERE SENG_REQ_TRNS_ID = ?";

    private static final String getSeatEngineEventTypeSql = "SELECT DISTINCT SENG_EVT_TYP_CD FROM SENG_EVT_MSG WHERE SENG_EVT_TYP_CD IS NOT NULL";

    @Override
    public List<String> getSeatEngReqMsgBasedOnOpasFltNumber(String opasFlightNum) {
        return seatdbJdbcTemplate.query(this.getReqMsgBasedOnOpasFltNum, new Object[]{opasFlightNum}, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return SeatUtil.getStringFromClob(rs.getClob("SENG_REQ_MSG"));
            }
        });
    }

    @Override
    public void insertSeatEvtMsgRequestResponseJsonData(SeatEvtMsg seatEvtMsg) throws IOException {

        if (seatEvtMsg == null ){
            throw new RuntimeException("seatEvtMsg cannot be null");
        }

        Connection connection = null;
        try {
            connection = seatdbJdbcTemplate.getDataSource().getConnection();

            Clob requestClob = connection.createClob();
            if (seatEvtMsg.getRequestClob() != null && !seatEvtMsg.getRequestClob().isEmpty()) {
                requestClob.setString(1, SeatUtil.compressedJsonData(seatEvtMsg.getRequestClob().replaceAll("(\\r|\\n)", "")));
            }else {
                requestClob.setString(1, null);
            }

            Clob responseClob = connection.createClob();
            if (seatEvtMsg.getResponseClob() != null && !seatEvtMsg.getResponseClob().isEmpty()) {
                responseClob.setString(1, SeatUtil.compressedJsonData(seatEvtMsg.getResponseClob().replaceAll("(\\r|\\n)", "")));
            }else {
                responseClob.setString(1, null);
            }

            PreparedStatement pstmt = null;
            try {
                pstmt = connection.prepareStatement(this.insertSengEvtMsgSql);
                pstmt.setString(1, seatEvtMsg.getTransactionid());
                pstmt.setTimestamp(2, seatEvtMsg.getCrtnGts());
                pstmt.setClob(3, requestClob);
                pstmt.setClob(4, responseClob);
                pstmt.setLong(5, seatEvtMsg.getPolicyId());
                pstmt.setString(6,seatEvtMsg.getServerName());
                pstmt.setString(7,seatEvtMsg.getSengEventTypeCode());
                pstmt.executeUpdate();
            }catch (Exception e){

            }finally{
                if (pstmt != null){
                    pstmt.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            if (connection !=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int updateSeatEvtMsgResponseJsonData(SeatEvtMsg seatEvtMsg) throws IOException {

        int executeUpdate = 0;

        if (seatEvtMsg == null ){
            throw new RuntimeException("seatEvtMsg cannot be null");
        }

        Connection connection = null;
        try {
            connection = seatdbJdbcTemplate.getDataSource().getConnection();


            Clob responseClob = connection.createClob();
            if (seatEvtMsg.getResponseClob() != null && !seatEvtMsg.getResponseClob().isEmpty()) {
                responseClob.setString(1, SeatUtil.compressedJsonData(seatEvtMsg.getResponseClob().replaceAll("(\\r|\\n)", "")));
            }else {
                responseClob.setString(1, null);
            }

            PreparedStatement pstmt = null;
            try {
                pstmt = connection.prepareStatement(this.updateSengEvtMsgSql);
                pstmt.setClob(1, responseClob);
                pstmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                pstmt.setString(3, seatEvtMsg.getTransactionid());
                executeUpdate = pstmt.executeUpdate();
            }catch (Exception e){

            }finally{
                if (pstmt != null){
                    pstmt.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            if (connection !=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return executeUpdate;
    }

    @Override
    public SeatEvtMsg getSeatEvgMsgBasedOntransactionId(String transactionId) throws IOException {
        SeatEvtMsg seatEvtMsg = (SeatEvtMsg)seatdbJdbcTemplate.queryForObject(
                this.getReqMsgBasedTransactionId, new Object[] { transactionId }, (RowMapper<SeatEvtMsg>) new SeatEvtMsgRowMapper());
        return seatEvtMsg;
    }

    @Override
    public List<SeatTransaction> getTransactionsByResourceLocatorId(TransactionSelection transactionSelection)
    {
    	List<SeatTransaction> seatTransactionMsgList = new ArrayList<SeatTransaction>();
    	try{
    		Object[] paramObject = new Object[2];
            int i = 0;

            String selectSql = this.getSeatTransactionSql;
            i = 0;
            if(transactionSelection.getRecordLocatorId() != null && transactionSelection.getRecordLocatorId().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                selectSql += " json_textcontains(SENG_REQ_MSG, '$.flightSegments.offers.recordLocatorId', '"+transactionSelection.getRecordLocatorId()+"')";
                i++;
            }
            if(transactionSelection.getFlightNo() != null && transactionSelection.getFlightNo().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                else
                    selectSql += " and ";
                selectSql += " json_textcontains(SENG_REQ_MSG, '$.flightSegments.operatingFlightNum', '"+transactionSelection.getFlightNo()+"')";
                i++;
            }
            if(transactionSelection.getScheduleDepartureFromDate() != null && transactionSelection.getScheduleDepartureFromDate().length() > 0
                    && transactionSelection.getScheduleDepartureToDate() != null && transactionSelection.getScheduleDepartureToDate().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                else
                    selectSql += " and ";
                selectSql += " "+this.basedByScheduleDepartureDateSql;
                paramObject[0] = transactionSelection.getScheduleDepartureFromDate();
                paramObject[1] = transactionSelection.getScheduleDepartureToDate();
            }
            else
            {
                paramObject = new Object[0];

            }

            log.debug(selectSql);
            seatTransactionMsgList = (List<SeatTransaction>)seatdbJdbcTemplate.query(selectSql,paramObject, (RowMapper<SeatTransaction>) new SeatTransactionRowMapper());

    	}
    	catch(Exception ex)
    	{
    		log.debug(ex);
    	}
        return seatTransactionMsgList;
    }
    
    public List<SeatAnalytics> getFlightAnalyticsData(SeatAnalyticsRequest seatAnalyticsRequest)
    {
    	List<SeatAnalytics>  seatTransactionMsgList = new ArrayList<SeatAnalytics>();
    	try{
    		Object[] paramObject = new Object[2];
            int i = 0;

            String selectSql = this.getSeatanalyticsSql;
            i = 0;
            if(seatAnalyticsRequest.getSeatEngineEventType() != null && seatAnalyticsRequest.getSeatEngineEventType().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                selectSql += " SENG_EVT_TYP_CD = '"+seatAnalyticsRequest.getSeatEngineEventType() +"'";
                i++;
            }
            if(seatAnalyticsRequest.getFlightOrigin() != null && seatAnalyticsRequest.getFlightOrigin().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                else
                    selectSql += " and ";
                selectSql += " json_textcontains(SENG_REQ_MSG, '$.flightSegments.originAirportCode', '"+seatAnalyticsRequest.getFlightOrigin()+"')";
                i++;
            }
            if(seatAnalyticsRequest.getFlightDestination() != null && seatAnalyticsRequest.getFlightDestination().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                else
                    selectSql += " and ";
                selectSql += " json_textcontains(SENG_REQ_MSG, '$.flightSegments.destinationAirportCode', '"+seatAnalyticsRequest.getFlightDestination()+"')";
                i++;
            }
            
            if(seatAnalyticsRequest.getSeatAnalyticsFlightNo() != null && seatAnalyticsRequest.getSeatAnalyticsFlightNo().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                else
                    selectSql += " and ";
                selectSql += " json_textcontains(SENG_REQ_MSG, '$.flightSegments.operatingFlightNum', '"+seatAnalyticsRequest.getSeatAnalyticsFlightNo()+"')";
                i++;
            }
            
            if(seatAnalyticsRequest.getSeatAnalyticsDepartureFromDate() != null && seatAnalyticsRequest.getSeatAnalyticsDepartureFromDate().length() > 0
                    && seatAnalyticsRequest.getSeatAnalyticsDepartureToDate() != null && seatAnalyticsRequest.getSeatAnalyticsDepartureToDate().length() > 0)
            {
                if(i==0)
                    selectSql += " where ";
                else
                    selectSql += " and ";
                selectSql += " "+this.basedByScheduleDepartureDateSql;
                paramObject[0] = seatAnalyticsRequest.getSeatAnalyticsDepartureFromDate();
                paramObject[1] = seatAnalyticsRequest.getSeatAnalyticsDepartureToDate();
            }
            else
            {
                paramObject = new Object[0];

            }

            log.debug(selectSql);

            seatTransactionMsgList = (List<SeatAnalytics>)seatdbJdbcTemplate.query(selectSql,paramObject, (RowMapper<SeatAnalytics>) new SeatAnalyticsRowMapper());
    	}
    	catch(Exception ex)
    	{
    		log.debug(ex);
    	}
    
        return seatTransactionMsgList;
    }
    public List<String> getSeatEngineEventType()
    {
    	List<String> seatEngineTypes = new ArrayList<String>();
    	try{
        	seatEngineTypes = seatdbJdbcTemplate.queryForList(getSeatEngineEventTypeSql, String.class);
    	}
    	catch(Exception ex)
    	{
    		
    	}
    	return seatEngineTypes;
    	
    }

}
