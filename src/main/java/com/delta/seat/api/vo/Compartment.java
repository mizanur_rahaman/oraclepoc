/*
 * Seat Engine Services Api
 * This API will use to select a seat. Seat Enghine API will be used for selecting an option based on offers.
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.delta.seat.api.vo;

import java.util.Objects;
import com.delta.seat.api.vo.SeatRow;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Compartment represents a carrier&#39;s grouping of seats on its transportation vehicle that receive like services. Compartment is the corporate standard. Examples- F &#x3D; First, C &#x3D; Business, Y &#x3D; Coach, W &#x3D; Premium Coach.
 */
@ApiModel(description = "Compartment represents a carrier's grouping of seats on its transportation vehicle that receive like services. Compartment is the corporate standard. Examples- F = First, C = Business, Y = Coach, W = Premium Coach.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-14T15:51:23.460-05:00")
public class Compartment {
  @JsonProperty("compartmentCode")
  private String compartmentCode = null;

  @JsonProperty("avilableInventoryCnt")
  private String avilableInventoryCnt = null;

  @JsonProperty("locationText")
  private String locationText = null;

  @JsonProperty("seatConfigurationCode")
  private String seatConfigurationCode = null;

  @JsonProperty("seatRows")
  private List<SeatRow> seatRows = null;

  public Compartment compartmentCode(String compartmentCode) {
    this.compartmentCode = compartmentCode;
    return this;
  }

   /**
   * Compartment Code denotes the a physical section of an aircraft .Examples- F &#x3D; First, C &#x3D; Business, Y &#x3D; Coach, W &#x3D; Premium Coach
   * @return compartmentCode
  **/
  @ApiModelProperty(value = "Compartment Code denotes the a physical section of an aircraft .Examples- F = First, C = Business, Y = Coach, W = Premium Coach")
  public String getCompartmentCode() {
    return compartmentCode;
  }

  public void setCompartmentCode(String compartmentCode) {
    this.compartmentCode = compartmentCode;
  }

  public Compartment avilableInventoryCnt(String avilableInventoryCnt) {
    this.avilableInventoryCnt = avilableInventoryCnt;
    return this;
  }

   /**
   * available inventory count for that given compartment. E.g., compartment W has inventory count of 5.
   * @return avilableInventoryCnt
  **/
  @ApiModelProperty(value = "available inventory count for that given compartment. E.g., compartment W has inventory count of 5.")
  public String getAvilableInventoryCnt() {
    return avilableInventoryCnt;
  }

  public void setAvilableInventoryCnt(String avilableInventoryCnt) {
    this.avilableInventoryCnt = avilableInventoryCnt;
  }

  public Compartment locationText(String locationText) {
    this.locationText = locationText;
    return this;
  }

   /**
   * Location
   * @return locationText
  **/
  @ApiModelProperty(value = "Location")
  public String getLocationText() {
    return locationText;
  }

  public void setLocationText(String locationText) {
    this.locationText = locationText;
  }

  public Compartment seatConfigurationCode(String seatConfigurationCode) {
    this.seatConfigurationCode = seatConfigurationCode;
    return this;
  }

   /**
   * Seat Configuration Example: AB|C|DE
   * @return seatConfigurationCode
  **/
  @ApiModelProperty(value = "Seat Configuration Example: AB|C|DE")
  public String getSeatConfigurationCode() {
    return seatConfigurationCode;
  }

  public void setSeatConfigurationCode(String seatConfigurationCode) {
    this.seatConfigurationCode = seatConfigurationCode;
  }

  public Compartment seatRows(List<SeatRow> seatRows) {
    this.seatRows = seatRows;
    return this;
  }

  public Compartment addSeatRowsItem(SeatRow seatRowsItem) {
    if (this.seatRows == null) {
      this.seatRows = new ArrayList<SeatRow>();
    }
    this.seatRows.add(seatRowsItem);
    return this;
  }

   /**
   * Row List is a list of rows in the compartment.
   * @return seatRows
  **/
  @ApiModelProperty(value = "Row List is a list of rows in the compartment.")
  public List<SeatRow> getSeatRows() {
    return seatRows;
  }

  public void setSeatRows(List<SeatRow> seatRows) {
    this.seatRows = seatRows;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Compartment compartment = (Compartment) o;
    return Objects.equals(this.compartmentCode, compartment.compartmentCode) &&
        Objects.equals(this.avilableInventoryCnt, compartment.avilableInventoryCnt) &&
        Objects.equals(this.locationText, compartment.locationText) &&
        Objects.equals(this.seatConfigurationCode, compartment.seatConfigurationCode) &&
        Objects.equals(this.seatRows, compartment.seatRows);
  }

  @Override
  public int hashCode() {
    return Objects.hash(compartmentCode, avilableInventoryCnt, locationText, seatConfigurationCode, seatRows);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Compartment {\n");
    
    sb.append("    compartmentCode: ").append(toIndentedString(compartmentCode)).append("\n");
    sb.append("    avilableInventoryCnt: ").append(toIndentedString(avilableInventoryCnt)).append("\n");
    sb.append("    locationText: ").append(toIndentedString(locationText)).append("\n");
    sb.append("    seatConfigurationCode: ").append(toIndentedString(seatConfigurationCode)).append("\n");
    sb.append("    seatRows: ").append(toIndentedString(seatRows)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

