/*
 * Seat Engine Services Api
 * This API will use to select a seat. Seat Enghine API will be used for selecting an option based on offers.
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.delta.seat.api.vo;

import java.util.Objects;
import com.delta.seat.api.vo.FlightSegmentOutput;
import com.delta.seat.api.vo.SeatEngineWarning;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Seat Selection Output represents the seat Id recommended by the seat engine.
 */
@ApiModel(description = "Seat Selection Output represents the seat Id recommended by the seat engine.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-14T15:51:23.460-05:00")
public class SeatSelectionOutput {
  @JsonProperty("flightSegmentOutputs")
  private List<FlightSegmentOutput> flightSegmentOutputs = null;

  @JsonProperty("transactionId")
  private String transactionId = null;

  @JsonProperty("responseCode")
  private String responseCode = null;

  @JsonProperty("responseMessageText")
  private String responseMessageText = null;

  @JsonProperty("warnings")
  private List<SeatEngineWarning> warnings = null;

  public SeatSelectionOutput flightSegmentOutputs(List<FlightSegmentOutput> flightSegmentOutputs) {
    this.flightSegmentOutputs = flightSegmentOutputs;
    return this;
  }

  public SeatSelectionOutput addFlightSegmentOutputsItem(FlightSegmentOutput flightSegmentOutputsItem) {
    if (this.flightSegmentOutputs == null) {
      this.flightSegmentOutputs = new ArrayList<FlightSegmentOutput>();
    }
    this.flightSegmentOutputs.add(flightSegmentOutputsItem);
    return this;
  }

   /**
   * Flight Segment List is a list of flight information in the response.
   * @return flightSegmentOutputs
  **/
  @ApiModelProperty(value = "Flight Segment List is a list of flight information in the response.")
  public List<FlightSegmentOutput> getFlightSegmentOutputs() {
    return flightSegmentOutputs;
  }

  public void setFlightSegmentOutputs(List<FlightSegmentOutput> flightSegmentOutputs) {
    this.flightSegmentOutputs = flightSegmentOutputs;
  }

  public SeatSelectionOutput transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

   /**
   * Transaction Identifier is a unique id generated for each request.
   * @return transactionId
  **/
  @ApiModelProperty(value = "Transaction Identifier is a unique id generated for each request.")
  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public SeatSelectionOutput responseCode(String responseCode) {
    this.responseCode = responseCode;
    return this;
  }

   /**
   * Response Code
   * @return responseCode
  **/
  @ApiModelProperty(value = "Response Code")
  public String getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }

  public SeatSelectionOutput responseMessageText(String responseMessageText) {
    this.responseMessageText = responseMessageText;
    return this;
  }

   /**
   * Response Message
   * @return responseMessageText
  **/
  @ApiModelProperty(value = "Response Message")
  public String getResponseMessageText() {
    return responseMessageText;
  }

  public void setResponseMessageText(String responseMessageText) {
    this.responseMessageText = responseMessageText;
  }

  public SeatSelectionOutput warnings(List<SeatEngineWarning> warnings) {
    this.warnings = warnings;
    return this;
  }

  public SeatSelectionOutput addWarningsItem(SeatEngineWarning warningsItem) {
    if (this.warnings == null) {
      this.warnings = new ArrayList<SeatEngineWarning>();
    }
    this.warnings.add(warningsItem);
    return this;
  }

   /**
   * Warnings is an array of warning.
   * @return warnings
  **/
  @ApiModelProperty(value = "Warnings is an array of warning.")
  public List<SeatEngineWarning> getWarnings() {
    return warnings;
  }

  public void setWarnings(List<SeatEngineWarning> warnings) {
    this.warnings = warnings;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SeatSelectionOutput seatSelectionOutput = (SeatSelectionOutput) o;
    return Objects.equals(this.flightSegmentOutputs, seatSelectionOutput.flightSegmentOutputs) &&
        Objects.equals(this.transactionId, seatSelectionOutput.transactionId) &&
        Objects.equals(this.responseCode, seatSelectionOutput.responseCode) &&
        Objects.equals(this.responseMessageText, seatSelectionOutput.responseMessageText) &&
        Objects.equals(this.warnings, seatSelectionOutput.warnings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(flightSegmentOutputs, transactionId, responseCode, responseMessageText, warnings);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SeatSelectionOutput {\n");
    
    sb.append("    flightSegmentOutputs: ").append(toIndentedString(flightSegmentOutputs)).append("\n");
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    responseCode: ").append(toIndentedString(responseCode)).append("\n");
    sb.append("    responseMessageText: ").append(toIndentedString(responseMessageText)).append("\n");
    sb.append("    warnings: ").append(toIndentedString(warnings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

