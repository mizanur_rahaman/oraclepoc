package com.delta.seat.api.vo;

public class SeatAnalyticsSengEvtMsg {
	private String transactionId;
	private String flightNo;
	private String departureDate;
	private String arrivalDate;
	private String origin;
	private String destination;
	private int TotalPNR;
	private int TotalPassenger;
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getTotalPNR() {
		return TotalPNR;
	}
	public void setTotalPNR(int totalPNR) {
		TotalPNR = totalPNR;
	}
	public int getTotalPassenger() {
		return TotalPassenger;
	}
	public void setTotalPassenger(int totalPassenger) {
		TotalPassenger = totalPassenger;
	}
}
