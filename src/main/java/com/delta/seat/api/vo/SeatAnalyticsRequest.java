package com.delta.seat.api.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "seat analytics search criteria details")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-08-16T16:55:23.307-05:00")
public class SeatAnalyticsRequest 
{
	@JsonProperty("seatEngineEventType")
    private String seatEngineEventType;
	
	@JsonProperty("flightOrigin")
    private String flightOrigin;
	
	@JsonProperty("flightDestination")
    private String flightDestination;
	
	@JsonProperty("seatAnalyticsFlightNo")
	private String seatAnalyticsFlightNo;
	
	@JsonProperty("seatAnalyticsDepartureFromDate")
	private String seatAnalyticsDepartureFromDate;
	
	@JsonProperty("seatAnalyticsDepartureToDate")
	private String seatAnalyticsDepartureToDate;
	
	@JsonProperty("seatRecommendation")
    private String seatRecommendation;
	
	@JsonProperty("seatCriteria")
    private String seatCriteria;
	
	public String getSeatEngineEventType() {
		return seatEngineEventType;
	}
	public void setSeatEngineEventType(String seatEngineEventType) {
		this.seatEngineEventType = seatEngineEventType;
	}
	public String getFlightOrigin() {
		return flightOrigin;
	}
	public void setFlightOrigin(String flightOrigin) {
		this.flightOrigin = flightOrigin;
	}
	public String getFlightDestination() {
		return flightDestination;
	}
	public void setFlightDestination(String flightDestination) {
		this.flightDestination = flightDestination;
	}
	public String getSeatAnalyticsFlightNo() {
		return seatAnalyticsFlightNo;
	}
	public void setSeatAnalyticsFlightNo(String seatAnalyticsFlightNo) {
		this.seatAnalyticsFlightNo = seatAnalyticsFlightNo;
	}
	public String getSeatAnalyticsDepartureFromDate() {
		return seatAnalyticsDepartureFromDate;
	}
	public void setSeatAnalyticsDepartureFromDate(String seatAnalyticsDepartureFromDate) {
		this.seatAnalyticsDepartureFromDate = seatAnalyticsDepartureFromDate;
	}
	public String getSeatAnalyticsDepartureToDate() {
		return seatAnalyticsDepartureToDate;
	}
	public void setSeatAnalyticsDepartureToDate(String seatAnalyticsDepartureToDate) {
		this.seatAnalyticsDepartureToDate = seatAnalyticsDepartureToDate;
	}
	public String getSeatRecommendation() {
		return seatRecommendation;
	}
	public void setSeatRecommendation(String seatRecommendation) {
		this.seatRecommendation = seatRecommendation;
	}
	public String getSeatCriteria() {
		return seatCriteria;
	}
	public void setSeatCriteria(String seatCriteria) {
		this.seatCriteria = seatCriteria;
	}
}
