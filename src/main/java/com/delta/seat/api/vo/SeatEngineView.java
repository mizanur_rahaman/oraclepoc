package com.delta.seat.api.vo;

import java.util.ArrayList;
import java.util.List;

public class SeatEngineView {

	private List<String> seatEngineEventTypes = new ArrayList<String>();
	public List<String> getSeatEngineEventTypes() {
		return seatEngineEventTypes;
	}
	public void setSeatEngineEventTypes(List<String> seatEngineEventTypes) {
		this.seatEngineEventTypes = seatEngineEventTypes;
	}

}
