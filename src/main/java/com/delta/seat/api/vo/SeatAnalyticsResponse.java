package com.delta.seat.api.vo;

import java.util.ArrayList;
import java.util.List;

public class SeatAnalyticsResponse {
	private boolean isSuccess;
	private String message;
	private List<SeatAnalyticsSengEvtMsg> dataList = new ArrayList<>();
	private boolean isShowTransaction;
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<SeatAnalyticsSengEvtMsg> getDataList() {
		return dataList;
	}
	public void setDataList(List<SeatAnalyticsSengEvtMsg> dataList) {
		this.dataList = dataList;
	}
	public boolean isShowTransaction() {
		return isShowTransaction;
	}
	public void setShowTransaction(boolean isShowTransaction) {
		this.isShowTransaction = isShowTransaction;
	}
}
