package com.delta.seat.api.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "seat selection details")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-08-16T16:55:23.307-05:00")
public class TransactionSelection {

    @JsonProperty("recordLocatorId")
    private String recordLocatorId;

    @JsonProperty("flightNo")
    private String flightNo;

    @JsonProperty("scheduleDepartureFromDate")
    private String scheduleDepartureFromDate;

    @JsonProperty("scheduleDepartureToDate")
    private String scheduleDepartureToDate;

    public String getRecordLocatorId() {
        return recordLocatorId;
    }

    public void setRecordLocatorId(String recordLocatorId) {
        this.recordLocatorId = recordLocatorId;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getScheduleDepartureFromDate() {
        return scheduleDepartureFromDate;
    }

    public void setScheduleDepartureFromDate(String scheduleDepartureFromDate) {
        this.scheduleDepartureFromDate = scheduleDepartureFromDate;
    }

    public String getScheduleDepartureToDate() {
        return scheduleDepartureToDate;
    }

    public void setScheduleDepartureToDate(String scheduleDepartureToDate) {
        this.scheduleDepartureToDate = scheduleDepartureToDate;
    }
}
