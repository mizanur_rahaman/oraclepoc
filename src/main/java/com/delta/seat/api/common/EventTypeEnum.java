package com.delta.seat.api.common;

public enum EventTypeEnum {

    ASAScoringAdjacency("ASAScoringAdjacency"),
    TwentyFiveHrBatchJob("25HrBatchJob"),
    MasterList("MasterList");

    private String value;

    private EventTypeEnum(final String value) {
        this.value = value;
    }
    public String value() {
        return value;
    }

}
