package com.delta.seat.api.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Clob;
import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.delta.seat.constants.SeatEngineConstants.SEAT_HOST_NAME;

public class SeatUtil {

    public static String compressedJsonData(String jsonTxt) throws JsonParseException, JsonMappingException, IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(jsonTxt, JsonNode.class);
        return jsonNode.toString();
    }


    public static String getSeatEngineServerName(){

        return System.getProperty(SEAT_HOST_NAME);

    }

    public static String readAllBytesToString(String filePath)
    {
        String content = "";
        try
        {
            content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return content;
    }

    public static String getStringFromClob(Clob data) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader reader = data.getCharacterStream();
            BufferedReader br = new BufferedReader(reader);

            String line;
            while(null != (line = br.readLine())) {
                sb.append(line);
            }
            br.close();
        } catch (SQLException e) {

        } catch (IOException e) {
            // handle this exception
        }
        return sb.toString();
    }
}
