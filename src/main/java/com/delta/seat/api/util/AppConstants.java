package com.delta.seat.api.util;

public class AppConstants {
	public class SeatEngineSearchCriteria {
		public static final int SeatRecommendationbySeatEngineValue = 1;
		public static final int NoSeatRecommendationbySeatEngineValue = 2;
		public static final int UnassignedPaxByTpfValue = 3;
		public static final int UnseatedPaxValue = 4;
		public static final int SeatRecommendationForUnseatedPAXValue = 5;
		
		public static final String SeatRecommendationbySeatEngineText = "Seat Recommendation by Seat Engine";
		public static final String NoSeatRecommendationbySeatEngineText = "No Seat Recommendation by Seat Engine";
		public static final String UnassignedPaxByTpfText = "Unassigned PAX by TPF";
		public static final String UnseatedPaxText = "Unseated Pax";
		public static final String SeatRecommendationForUnseatedPAXText = "Seat recommendation # for Unseated PAX"; 
	}
	public class SeatEngineSearchCriteriaType {

		public static final String SinglePartyPNRValue = "SPP";
		public static final String MultiPartyPNRValue = "MPP";
		public static final String FamilyPNRsValue = "FPP";
		public static final String DiamondMedallionValue = "DM";
		public static final String PlatinumMedallionValue = "PM";
		public static final String GoldMedallionValue = "GM";
		public static final String SilverMedallionValue = "FO";
		public static final String BaseMedallionValue = "FF";
		public static final String NonLoyalityProgramPassengerValue = "NLPP";
		
		public static final String SinglePartyPNRText = "Single-Party PNR";
		public static final String MultiPartyPNRText = "Multi-Party PNR";
		public static final String FamilyPNRsText = "Family PNRs with Pax < 14yrs";
		public static final String DiamondMedallionText = "Diamond Medallion";
		public static final String PlatinumMedallionText = "Platinum Medallion";
		public static final String GoldMedallionText = "Gold Medallion";
		public static final String SilverMedallionText = "Silver Medallion";
		public static final String BaseMedallionText = "Base Medallion";
		public static final String NonLoyalityProgramPassengerText = "Non-Member";

	}
	
	public class FlightInfoJSONFieldText {
		public static final String AgeRange = "ageRangeText";
		public static final String loyaltyProgramTierCode = "loyaltyProgramTierCode";
		public static final String flightSegments = "flightSegments";
		public static final String flightSegmentOutputs = "flightSegmentOutputs";
		public static final String seatSelections = "seatSelections";
		public static final String isAssigedByTPFText = "isAssigedByTPFText";
		public static final String recordLocatorId = "recordLocatorId";
		public static final String passengers = "passengers";
		public static final String offers = "offers";
		public static final String scheduledDepartureLocalDate = "scheduledDepartureLocalDate";
		public static final String flightDepartureTime = "flightDepartureTime";
		public static final String ssrs = "ssrs";
		public static final String loyaltyMemberPreferences = "loyaltyMemberPreferences";
		public static final String seatId = "seatId";
		public static final String firstNameNum = "firstNameNum";
		public static final String lastNameNum = "lastNameNum";
		public static final String travelerId = "travelerId";
		public static final String ssrCode = "ssrCode";
		public static final String preferenceCode = "preferenceCode";
		public static final String preferenceCodeDesc = "preferenceCodeDesc";
	}
	
	
	public static final String familyPnrRangeText = "0-14";
	public static final String defaultSelectedBatchJob = "25HrBatchJob";
	public static final String passengerText = "Passenger";
	public static final String noRecordFoundText = "No Record found";
	public static final String TotalText = "Total";
	public static final String infantAgeRangeText = "0-2";
}
