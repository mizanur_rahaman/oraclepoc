package com.delta.skdchange.api.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.weaver.patterns.ThisOrTargetAnnotationPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.delta.seat.api.controller.SeatApiController;
import com.delta.skdchange.api.dao.FlightLegs;
import com.delta.skdchange.api.dao.Flights;
import com.delta.skdchange.api.dao.RunSSIMText;
import com.delta.skdchange.api.dao.SkdChangeDao;
import com.delta.skdchange.api.dao.SkdChangeDaoImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SkdChangeServiceImpl implements SkdChangeService{

    private static final Logger log = LogManager.getLogger(SkdChangeServiceImpl.class);

	@Autowired SkdChangeDao skdChangeDao;
	private String SSIMTextFile = null;
	private String OutputCSVFile = null;
    public String getSSIMTextFile() {
		return SSIMTextFile;
	}
	public void setSSIMTextFile(String sSIMTextFile) {
		SSIMTextFile = sSIMTextFile;
	}
	public String getOutputCSVFile() {
		return OutputCSVFile;
	}
	public void setOutputCSVFile(String outputCSVFile) {
		OutputCSVFile = outputCSVFile;
	}

	public void generateFlightLegs(MultipartFile oldFile, MultipartFile newFile)
	{
		try
		{
			if(oldFile!=null)
			{
				InputStream is = oldFile.getInputStream();
		        BufferedReader br = new BufferedReader(new InputStreamReader(is));
		        ArrayList<RunSSIMText> sSIMRows = getRunSSIMData(br);
		        ArrayList<Flights> flightList = getFlightData(sSIMRows);
				int flightSize = flightList != null ? flightList.size() : 0;
				ArrayList<FlightLegs> flightLegsList = new ArrayList<FlightLegs>();
				for(int i=0;i<flightSize;i++)
				{
					 formatObjectToJsonString(flightList.get(i));
					 ArrayList<FlightLegs> flightLegs = getFlightLegsData(flightList.get(i));
					 flightLegsList.addAll(flightLegs);
				}
				//writeToCSV(flightLegsList); 
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	public void readSSIMFile() 
	{	
		log.info("readSSIMFile");
		try 
		{
			//skdChangeService.setSSIMTextFile("E:\\JAVA Development\\POCUI_doc\\SkdChange\\SSIM.txt");
			//skdChangeService.setOutputCSVFile("E:\\JAVA Development\\POCUI_doc\\SkdChange\\FlightLegs.csv");
			BufferedReader br = new BufferedReader(new FileReader(this.getSSIMTextFile()));
		    ArrayList<RunSSIMText> sSIMRows = getRunSSIMData(br);;
			ArrayList<Flights> flightList = getFlightData(sSIMRows);
			int flightSize = flightList != null ? flightList.size() : 0;
			ArrayList<FlightLegs> flightLegsList = new ArrayList<FlightLegs>();
			for(int i=0;i<flightSize;i++)
			{
				 formatObjectToJsonString(flightList.get(i));
				 ArrayList<FlightLegs> flightLegs = getFlightLegsData(flightList.get(i));
				 flightLegsList.addAll(flightLegs);
			}
			writeToCSV(flightLegsList); 
		}
		catch (FileNotFoundException ex) 
	 	{
	        ex.printStackTrace();
	    } 
	}
	public ArrayList<RunSSIMText> getRunSSIMData( BufferedReader br) 
	{
		ArrayList<RunSSIMText> sSIMRows = new ArrayList<RunSSIMText>();
		try
		{
			if(br==null)
				return sSIMRows;
			int totalEntryParsedCount = 0;
			String line;
	        while ((line = br.readLine()) != null) 
	        {
	        	try
	        	{
	        		if(line.trim().length()==0)
		        	{
		        		log.error("Empty line. Cannot be parsed");
		        		System.out.println("Empty line. Cannot be parsed");
		        		continue;
		        	}
		        	int lineLength = line.length();
		        	if(!line.substring(0,1).equals("3") || !line.substring(2, 4).equalsIgnoreCase("DL"))
		        	{
		        		log.error("Entry cannot be parsed because of entry does not contain 3 DL");
		        		System.out.println("Entry cannot be parsed because of entry does not contain 3 DL");
		        		continue;
		        	}
		        	RunSSIMText runSSIMText = new RunSSIMText();
		        	runSSIMText.setRunId(2);
		        	runSSIMText.setScheduleId(1);
		        	runSSIMText.setSchdVersionSeqNum(1);
		        	runSSIMText.setRowId(Integer.parseInt(line.substring(lineLength-6, lineLength)));
		        	runSSIMText.setRowText(line);
		        	sSIMRows.add(runSSIMText);
		        	
		        	// insert into RunSSIM
		        	//skdChangeDao.insertSSIMText(runSSIMText);
		        	totalEntryParsedCount++;
	        	}
	        	catch(Exception ex)
	        	{
	        		 log.info("SSIM Entry contain invalid data: "+ex.getMessage());
	     	        System.out.println("SSIM Entry contain invalid data: " + ex.getMessage());
	        	}
	        }
	        log.info("Total SSIM Entry parsed: "+totalEntryParsedCount);
	        System.out.println("Total SSIM Entry parsed: "+totalEntryParsedCount);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return sSIMRows;
	}
	public ArrayList<Flights> getFlightData(ArrayList<RunSSIMText> sSIMRows)
	{
		ArrayList<Flights> flightList = new ArrayList<Flights>();
		try 
		{
			int flightCount = 0;
			String line = null;
			int sSIMTextsSize = sSIMRows != null ? sSIMRows.size() : 0;
			System.out.println(sSIMTextsSize);
	        for(int i=0;i<sSIMTextsSize;i++)
	        {     
	        	try
	        	{
	        		line = sSIMRows.get(i).getRowText();
		        	int lineLength = line.length();
		        	Flights flights = new Flights();
		        	flights.setScheduleId(sSIMRows.get(i).getScheduleId());
		        	flights.setFlightId(Integer.parseInt(line.substring(lineLength-6, lineLength).trim()));
		        	flights.setEqpTypeCd(line.substring(72,75));
		        	flights.setSegSeqNum(Integer.parseInt((line.substring(127,128)+line.substring(9,11)).trim()));
		        	flights.setLegSeqNum(Integer.parseInt(line.substring(11,13).trim()));
		        	flights.setLegTypeCde(line.substring(13,14));
		        	flights.setACOwner(line.substring(128,130));
		        	flights.setOpAirline(line.substring(2,4));
		        	flights.setOpFltNum(Integer.parseInt(line.substring(5,9).trim()));
		        	flights.setOrig(line.substring(36,39));
		        	flights.setDest(line.substring(54,57));
		        	flights.setDepTime(line.substring(39,41) + ':' + line.substring(41,43));
		        	if(line.substring(57,59).equals("24"))
		        		flights.setArrTime("00"+ ':' + line.substring(59,61));
		        	else
		        		flights.setArrTime(line.substring(57,59)+ ':' + line.substring(59,61));
		        	
		        	flights.setDepTimeGmtOffset(Integer.parseInt(line.substring(47,48)+(Integer.parseInt(line.substring(48,50))*60 + Integer.parseInt(line.substring(50,52)))));
		        	flights.setArrTimeGmtOffset(Integer.parseInt(line.substring(65,66)+(Integer.parseInt(line.substring(66,68))*60 + Integer.parseInt(line.substring(68,70)))));

		        	if(line.substring(14,21).equalsIgnoreCase("00XXX00"))
		        		flights.setFromDate(null);
		        	else
		        		flights.setFromDate(line.substring(16,19)+' ' + line.substring(14,16)+", 20"+line.substring(19,21));
		        	
		        	if(line.substring(21,28).equalsIgnoreCase("00XXX00"))
		        		flights.setToDate(null);
		        	else
		        		flights.setToDate(line.substring(23,26)+' ' + line.substring(21,23)+", 20"+line.substring(26,28));
		        	flights.setFrequency(line.substring(28,35));
		        	flights.setCxnAirline(line.substring(137,139));
		        	if(line.substring(140,144).trim().length() == 0)
		        		flights.setCxnFltNum(0);
		        	else
		        		flights.setCxnFltNum(Integer.parseInt(line.substring(140,144).trim()));
		        	if(line.substring(144,145).trim().length()==0)
						flights.setLayoverDays(0);
					else
						flights.setLayoverDays(Integer.parseInt(line.substring(144,145).trim()));
		        	flightList.add(flights);
		        	
		        	//insert flight table
		        	//skdChangeDao.insertFlight(flights);
		        	flightCount++;
	        	
		        }
	        	catch(Exception ex)
	        	{
	        		ex.printStackTrace();
	        		 log.info("Flight Entry contain invalid data: "+ex.getMessage());
	     	        System.out.println("Flight Entry contain invalid data: " + ex.getMessage());
	        	}
	        	
	        }
	        log.info("Total flight Entry parsed: "+flightCount);
	        System.out.println("Total flight Entry parsed: "+flightCount);
		}

		catch (Exception ex) 
	 	{
			ex.printStackTrace();
	    } 
		return flightList;
	}
	private void sqlText()
	{
		//    	/*
		//   	 ALTER PROCEDURE [dbo].[sp_InsertFlights]-- @RunMode varchar(10)	/*  {'Original', 'Solution'} */
		//AS
		//
		//
		//DECLARE @RunMode varchar(10)
		//SET @RunMode = 'Original'
		//
		//DECLARE @runId INT
		//DECLARE @ScheduleId INT
		//DECLARE @SchdVersionSeqNum INT
		//DECLARE @EqpTypeCd CHAR(3)
		//
		//SELECT 
		//	@runId = RunId 
		//	, @ScheduleId = ScheduleId
		//	, @SchdVersionSeqNum = CASE WHEN @RunMode = 'Original' THEN FromSchdVersionSeqNum ELSE ToSchdVersionSeqNum END
		//	, @EqpTypeCd = EqpTypeCd
		//FROM vw_ActiveRun
		//
		//DELETE FROM Flights 
		//WHERE ScheduleId = @ScheduleId
		//	AND EqpTypeCd = @EqpTypeCd
		//
		//INSERT INTO Flights
		//SELECT
		//	@ScheduleId as ScheduleId
		//	, CONVERT(INT, RIGHT(RowText, 6)) AS FlightId
		//	, @EqpTypeCd
		//	, CONVERT(INT, SUBSTRING(RowText, 128, 1) + SUBSTRING(RowText, 10, 2)) AS SegSeqNum
		//	, CONVERT(INT, SUBSTRING(RowText, 12, 2)) AS LegSeqNum
		//	, CONVERT(CHAR(1), SUBSTRING(RowText, 14, 1)) AS LegTypeCde
		//	
		//	, CONVERT(CHAR(2), SUBSTRING(RowText, 129, 2)) AS ACOwner
		//	, CONVERT(CHAR(2), SUBSTRING(RowText, 3, 2)) AS OpAirline
		//	, CONVERT(INT, SUBSTRING(RowText, 6, 4)) AS OpFltNum
		//
		//	, CONVERT(CHAR(3), SUBSTRING(RowText, 37, 3)) AS Orig
		//	, CONVERT(CHAR(3), SUBSTRING(RowText, 55, 3)) AS Dest
		//
		//	, CONVERT(CHAR(5), SUBSTRING(RowText, 40, 2) + ':' + SUBSTRING(RowText, 42, 2)) AS DepTime
		//	, CONVERT(CHAR(5), 
		//		CASE WHEN SUBSTRING(RowText, 58, 2) = '24' THEN '00' ELSE SUBSTRING(RowText, 58, 2) END
		//		+ ':' + SUBSTRING(RowText, 60, 2)) AS ArrTime
		//	
		//	, CONVERT(INTEGER,
		//		SUBSTRING(RowText, 48, 1) 
		//		+ CONVERT(VARCHAR, CONVERT(INT, SUBSTRING(RowText, 49, 2)) * 60) + CONVERT(INT, SUBSTRING(RowText, 51, 2))) DepTimeGmtOffset
		//
		//	, CONVERT(INTEGER,
		//		SUBSTRING(RowText, 66, 1) 
		//		+ CONVERT(VARCHAR, CONVERT(INT, SUBSTRING(RowText, 67, 2) ) * 60) + CONVERT(INT, SUBSTRING(RowText, 69, 2) )) ArrTimeGmtOffset
		//	
		//	, CASE WHEN SUBSTRING(RowText, 15, 7) = '00XXX00' THEN CONVERT(DATETIME, NULL)
		//		ELSE CONVERT(DATETIME, SUBSTRING(RowText, 17, 3) + ' ' + SUBSTRING(RowText, 15, 2) + ', 20' + SUBSTRING(RowText, 20, 2))
		//		END AS FromDate
		//	
		//	, CASE WHEN SUBSTRING(RowText, 22, 7) = '00XXX00' THEN CONVERT(DATETIME, NULL)
		//		ELSE CONVERT(DATETIME, SUBSTRING(RowText, 24, 3) + ' ' + SUBSTRING(RowText, 22, 2) + ', 20' + SUBSTRING(RowText, 27, 2))
		//		END AS ToDate
		//
		//
		//	, SUBSTRING(RowText, 29, 7) AS Frequency
		//	
		//	, SUBSTRING(RowText, 138, 2) AS CxnAirline
		//	, CONVERT(INT, SUBSTRING(RowText, 141, 4)) AS CxnFltNum
		//	, CONVERT(INT, SUBSTRING(RowText, 145, 1)) AS LayoverDays
		//
		//FROM RunSSIMText
		//WHERE LEFT(RowText, 1) = '3'
		//	AND RunId = @RunId
		//	AND SchdVersionSeqNum = @SchdVersionSeqNum
		//ORDER BY RowId
		//
		//DECLARE @rowCount INT
		//SELECT @rowCount = COUNT(*) FROM Flights 
		//WHERE ScheduleId = @ScheduleId
		//	AND EqpTypeCd = @EqpTypeCd
		//   	 */
	}
	public ArrayList<FlightLegs> getFlightLegsData(Flights flights)
	{ 
		ArrayList<FlightLegs> flightLegList  = new ArrayList<FlightLegs>();
		try
		{
            int totalFlightLegs = 0;
			String frequency = flights.getFrequency();
			List<Date> DateList = getDatesBetween(new Date(flights.getFromDate()), new Date(flights.getToDate()));
			int size = DateList.size();
			System.out.println("size "+size);
			for(int i=0;i<size;i++)
			{
				try
				{
					Date date = DateList.get(i);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					int day = calendar.get(Calendar.DAY_OF_WEEK);
					day = day == 1? 7 : day-1;
					
				    if(frequency.contains(day+""))
				    {
				    	FlightLegs flightLegs = new FlightLegs();
				    	flightLegs.setFlightId(flights.getFlightId());
				    	flightLegs.setScheduleId(flights.getScheduleId());
				    	flightLegs.setEqpTypeCd(flights.getEqpTypeCd());
				    	flightLegs.setACOwner(flights.getACOwner());
				    	flightLegs.setOpAirline(flights.getOpAirline());
				    	flightLegs.setOpFltNum(flights.getOpFltNum());
				    	flightLegs.setOrig(flights.getOrig());
				    	flightLegs.setDest(flights.getDest());
				    	flightLegs.setLegTypeCde(flights.getLegTypeCde());
				    	flightLegs.setRedEyeInd("N");
				    	flightLegs.setAdjLocalDepDate(null);
				    	flightLegs.setAdjLocalArrDate(null);
				    	SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
				    	SimpleDateFormat sdfDateTillMilisecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
				    	flightLegs.setDepartDate(sdfDate.format(date));
				    	SimpleDateFormat sdfOnlyDate = new SimpleDateFormat("yyyy-MM-dd");
				    	flightLegs.setDepTime(sdfOnlyDate.format(date)+ " "+flights.getDepTime()+":00:000");
				    	flightLegs.setArrTime(sdfOnlyDate.format(date)+ " "+flights.getArrTime()+":00:000");
				    	calendar = Calendar.getInstance();

				    	Date fromDateGMT;
						try {
							fromDateGMT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").parse(flightLegs.DepTime);
							calendar.setTime(fromDateGMT);
					        calendar.add(Calendar.MINUTE, (-1)*flights.getDepTimeGmtOffset());
					    	flightLegs.setDepTimeGmt(sdfDateTillMilisecond.format(calendar.getTime()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						Date toDateGMT;
						try {
							toDateGMT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").parse(flightLegs.ArrTime);
							calendar.setTime(toDateGMT);
					        calendar.add(Calendar.MINUTE, (-1)*flights.getArrTimeGmtOffset());
					    	flightLegs.setArrTimeGmt(sdfDateTillMilisecond.format(calendar.getTime()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						flightLegList.add(flightLegs);
				    	skdChangeDao.insertFlightLegs(flightLegs);
				    	totalFlightLegs++;
				    }
				}
				catch(Exception ex)
	        	{
	        		 log.info("Flight Legs Entry contain invalid data: "+ex.getMessage());
	     	        System.out.println("Flight Legs Entry contain invalid data: " + ex.getMessage());
	        	}
			}
			 log.info("Total flight Legs Entry parsed: "+totalFlightLegs);
		     System.out.println("Total flight Legs Entry parsed: "+totalFlightLegs);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return flightLegList;
		
	}
	
	 public static List<Date> getDatesBetween(
			  Date startDate, Date endDate) { 
			  
		 List<Date> dates = new ArrayList<Date>();
		    Calendar calendar = new GregorianCalendar();
		    calendar.setTime(startDate);

		    while (calendar.getTime().before(endDate))
		    {
		        Date result = calendar.getTime();
		        dates.add(result);
		        calendar.add(Calendar.DATE, 1);
		    }
		    dates.add(endDate);
		    return dates;
	}

	 
	 public String formatDateTime(Date date)
	 {
		 SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
		  String strDate = sdfDate.format(date);
		  System.out.println(strDate);
		  return strDate;
	 }
	 public String formatObjectToJsonString(Flights flights)
	 {
		 String jsonString = "";
		 ObjectMapper Obj = new ObjectMapper(); 
		  
	        try { 
	  
	            // get Oraganisation object as a json string 
	        	jsonString = Obj.writeValueAsString(flights); 
	  
	            // Displaying JSON String 
	            System.out.println(jsonString); 
	        } 
	  
	        catch (IOException e) { 
	            e.printStackTrace(); 
	        }
	        return jsonString;
	 }
	
	   private static final String CSV_SEPARATOR = ",";
	   private static StringBuffer getHeaderofCSV()
	    {
           StringBuffer oneLine = new StringBuffer();

	        try
	        {
	                oneLine.append("FlightId");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("ScheduleId");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("EqpTypeCd");
	                oneLine.append(CSV_SEPARATOR);
	            	oneLine.append("DepartDate");
			    	oneLine.append(CSV_SEPARATOR);
	                oneLine.append("ACOwner");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("OpAirline");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("OpFltNum");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("Orig");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("Dest");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("DepTime");
			    	oneLine.append(CSV_SEPARATOR);
			    	oneLine.append("ArrTime");
			    	oneLine.append(CSV_SEPARATOR);
			        oneLine.append("DepTimeGmt");
			        oneLine.append(CSV_SEPARATOR);
			        oneLine.append("ArrTimeGmt");
			    	oneLine.append(CSV_SEPARATOR);
	                oneLine.append("LegTypeCde");
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append("RedEyeInd");
			    	oneLine.append(CSV_SEPARATOR);
			    	oneLine.append("AdjLocalDepDate");
			    	oneLine.append(CSV_SEPARATOR);
			    	oneLine.append("AdjLocalArrDate");
	        }
	        catch(Exception ex){}
           return oneLine;
	    }
	    public void writeToCSV(ArrayList<FlightLegs> flightLegsList)
	    {
	        try
	        {
	            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.OutputCSVFile), "UTF-8"));
	            bw.write(getHeaderofCSV().toString());
	            bw.newLine();
	            for (FlightLegs flightLegs: flightLegsList)
	            {
	                StringBuffer oneLine = new StringBuffer();

	                oneLine.append(flightLegs.getFlightId());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getScheduleId());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getEqpTypeCd());
	                oneLine.append(CSV_SEPARATOR);
	            	oneLine.append(flightLegs.getDepartDate());
			    	oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getACOwner());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getOpAirline());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getOpFltNum());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getOrig());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getDest());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getDepTime());
			    	oneLine.append(CSV_SEPARATOR);
			    	oneLine.append(flightLegs.getArrTime());
			    	oneLine.append(CSV_SEPARATOR);
			        oneLine.append(flightLegs.getDepTimeGmt());
			        oneLine.append(CSV_SEPARATOR);
			        oneLine.append(flightLegs.getArrTimeGmt());
			    	oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getLegTypeCde());
	                oneLine.append(CSV_SEPARATOR);
	                oneLine.append(flightLegs.getRedEyeInd());
			    	oneLine.append(CSV_SEPARATOR);
			    	oneLine.append(flightLegs.getAdjLocalDepDate());
			    	oneLine.append(CSV_SEPARATOR);
			    	oneLine.append(flightLegs.getAdjLocalArrDate());
	                bw.write(oneLine.toString());
	                bw.newLine();
	            }
	            bw.flush();
	            bw.close();
	        }
	        catch (UnsupportedEncodingException e) {}
	        catch (FileNotFoundException e){}
	        catch (IOException e){}
	    }
		public static void main (String gargs[]) {
			SkdChangeServiceImpl skdChangeService = new SkdChangeServiceImpl();
			skdChangeService.setSSIMTextFile("E:\\JAVA Development\\POCUI_doc\\SkdChange\\SSIM.txt");
			skdChangeService.setOutputCSVFile("E:\\JAVA Development\\POCUI_doc\\SkdChange\\FlightLegs.csv");
			skdChangeService.readSSIMFile();

		 }	
		public void uploadFiles( byte[] oldFile, byte[]newFile)
		{
			try 
			{
				byte[] compressedOldFile = compress(oldFile);
				byte[] compressedNewFile = compress(newFile);
				skdChangeDao.uploadFiles(compressedOldFile,compressedNewFile);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static byte[] compress(byte[] data) throws IOException 
		{  

			   Deflater deflater = new Deflater();  

			   deflater.setInput(data);  

			   ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);   

			   deflater.finish();  

			   byte[] buffer = new byte[1024];   

			   while (!deflater.finished()) {  

			    int count = deflater.deflate(buffer); // returns the generated code... index  

			    outputStream.write(buffer, 0, count);   

			   }  

			   outputStream.close();  

			   byte[] output = outputStream.toByteArray();  

			   System.out.println("Original: " + data.length *1.0 / 1024 + " Kb");  

			   System.out.println("Compressed: " + output.length *1.0 / 1024 + " Kb");  
			   

			   return output;  

		}  

	    public static byte[] decompress(byte[] data) throws IOException, DataFormatException 
	    {  

			   Inflater inflater = new Inflater();   

			   inflater.setInput(data);  

			   ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);  

			   byte[] buffer = new byte[1024];  

			   while (!inflater.finished()) {  

			    int count = inflater.inflate(buffer);  

			    outputStream.write(buffer, 0, count);  

			   }  

			   outputStream.close();  

			   byte[] output = outputStream.toByteArray();  

			   System.out.println("Original: " + data.length);  

			   System.out.println("Compressed: " + output.length);  

			   return output;  
		}  
}
