package com.delta.skdchange.api.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.delta.skdchange.api.dao.FlightLegs;
import com.delta.skdchange.api.dao.Flights;
import com.delta.skdchange.api.dao.RunSSIMText;

@Service
public interface SkdChangeService {
	public void generateFlightLegs(MultipartFile oldFile, MultipartFile newFile);
	public ArrayList<Flights> getFlightData(ArrayList<RunSSIMText> sSIMRows);
	public void readSSIMFile(); 
	public String formatObjectToJsonString(Flights flights);
	public String formatDateTime(Date date);
	public ArrayList<FlightLegs> getFlightLegsData(Flights flights);
	public void writeToCSV(ArrayList<FlightLegs> flightLegsList);
	public void uploadFiles( byte[] oldFile, byte[]newFile);
}
