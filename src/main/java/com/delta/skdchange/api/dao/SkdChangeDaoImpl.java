package com.delta.skdchange.api.dao;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import javax.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class SkdChangeDaoImpl implements SkdChangeDao {
	@Resource(name = "seatdbJdbcTemplate")
	private JdbcTemplate seatdbJdbcTemplate;

	/* sql query */
	private static final String insertRunSSIMTextSql = "INSERT INTO RunSSIMText(RunId,ScheduleId,SchdVersionSeqNum,RunRowId,RowText)VALUES(?,?,?,?,?)";
    private static final String insertFlightSql = "INSERT INTO Flights(ScheduleId,FlightId,EqpTypeCd,SegSeqNum,LegSeqNum,LegTypeCde,ACOwner,OpAirline,OpFltNum,Orig"
    											+ ",Dest,DepTime,ArrTime,DepTimeGmtOffset,ArrTimeGmtOffset,FromDate,ToDate,Frequency,CxnAirline,CxnFltNum,LayoverDays)"
    											+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";	
    private static final String insertFlightLegsSql = "INSERT INTO FlightLegs(FlightId,ScheduleId,EqpTypeCd,DepartDate,ACOwner,OpAirline,OpFltNum"
													+ ",Orig,Dest,DepTime,ArrTime,DepTimeGmt,ArrTimeGmt,LegTypeCde,RedEyeInd,AdjLocalDepDate,AdjLocalArrDate)"
												    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public void insertSSIMText(RunSSIMText runSSIMText) {
		if (runSSIMText == null) {
			System.out.println("runSSIMText cannot be null");
			return;
		}
        System.out.println("seatdbJdbcTemplate: "+seatdbJdbcTemplate);
		Connection connection = null;
		try {
			connection = seatdbJdbcTemplate.getDataSource().getConnection();
			PreparedStatement pstmt = null;
			try {
				pstmt = connection.prepareStatement(this.insertRunSSIMTextSql);
				pstmt.setInt(1, runSSIMText.getRunId());
				pstmt.setInt(2, runSSIMText.getScheduleId());
				pstmt.setInt(3, runSSIMText.getSchdVersionSeqNum());
				pstmt.setInt(4, runSSIMText.getRowId());
				pstmt.setString(5, runSSIMText.getRowText());
				pstmt.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (pstmt != null) {
					pstmt.close();
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void insertFlight(Flights flights) {
		if (flights == null) {
			System.out.print("flights cannot be null");
			return;
		}

		Connection connection = null;
		try {
			connection = seatdbJdbcTemplate.getDataSource().getConnection();
			PreparedStatement pstmt = null;
			try {
				pstmt = connection.prepareStatement(this.insertFlightSql);
				pstmt.setInt(1, flights.getScheduleId());
				pstmt.setInt(2, flights.getFlightId());
				pstmt.setString(3, flights.getEqpTypeCd());
				pstmt.setInt(4, flights.getSegSeqNum());
				pstmt.setInt(5, flights.getLegSeqNum());
				pstmt.setString(6, flights.getLegTypeCde());
				pstmt.setString(7, flights.getACOwner());
				pstmt.setString(8, flights.getOpAirline());
				pstmt.setInt(9, flights.getOpFltNum());
				pstmt.setString(10, flights.getOrig());
				pstmt.setString(11, flights.getDest());
				pstmt.setString(12, flights.getDepTime());
				pstmt.setString(13, flights.getArrTime());
				pstmt.setInt(14, flights.getDepTimeGmtOffset());
				pstmt.setInt(15, flights.getArrTimeGmtOffset());
				pstmt.setString(16, flights.getFromDate());
				pstmt.setString(17, flights.getToDate());
				pstmt.setString(18, flights.getFrequency());
				pstmt.setString(19, flights.getCxnAirline());
				pstmt.setInt(20, flights.getCxnFltNum());
				pstmt.setInt(21, flights.getLayoverDays());
				pstmt.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (pstmt != null) {
					pstmt.close();
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void insertFlightLegs(FlightLegs flightLegs) {
		if (flightLegs == null) {
			System.out.println("flights cannot be null");
			return;
		}

		Connection connection = null;
		try {
			connection = seatdbJdbcTemplate.getDataSource().getConnection();
			PreparedStatement pstmt = null;
			try {
				pstmt = connection.prepareStatement(this.insertFlightLegsSql);
				pstmt.setInt(1, flightLegs.getFlightId());
				pstmt.setInt(2, flightLegs.getScheduleId());
				pstmt.setString(3, flightLegs.getEqpTypeCd());
				pstmt.setString(4, flightLegs.getDepartDate());
				pstmt.setString(5, flightLegs.getACOwner());
				pstmt.setString(6, flightLegs.getOpAirline());
				pstmt.setInt(7, flightLegs.getOpFltNum());
				pstmt.setString(8, flightLegs.getOrig());
				pstmt.setString(9, flightLegs.getDest());
				pstmt.setString(10, flightLegs.getDepTime());
				pstmt.setString(11, flightLegs.getArrTime());
				pstmt.setString(12, flightLegs.getDepTimeGmt());
				pstmt.setString(13, flightLegs.getArrTimeGmt());
				pstmt.setString(14, flightLegs.getLegTypeCde());
				pstmt.setString(15, flightLegs.getRedEyeInd());
				pstmt.setString(16, flightLegs.getAdjLocalDepDate());
				pstmt.setString(17, flightLegs.getAdjLocalArrDate());
				pstmt.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (pstmt != null) {
					pstmt.close();
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void uploadFiles( byte[] oldFile, byte[]newFile)
	{
		Connection connection = null;
		try {
			connection = seatdbJdbcTemplate.getDataSource().getConnection();

		   	Blob oldFileBlobData = connection.createBlob();
		   	oldFileBlobData.setBytes(1, oldFile);
		   	Blob newFileBlobData = connection.createBlob();
		   	newFileBlobData.setBytes(1, newFile);
		   	PreparedStatement pstmt = connection.prepareStatement("insert into RunFlightSchedule(runId,oldFlightSchedule,newFlightSchedule)values(?,?,?)");
		   	String uniqueID = UUID.randomUUID().toString();
		   	pstmt.setString(1, uniqueID);
		   	pstmt.setBlob(2, oldFileBlobData);
		   	pstmt.setBlob(3, newFileBlobData);
		   	pstmt.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
}
