package com.delta.skdchange.api.dao;

public class Run {
	private int RunId;
	private String RunName;
	private int ScheduleId;
	private int FromSchdVersionSeqNum;
	private int ToSchdVersionSeqNum;
	private String SchedulePeriod;
	private String ACOwner;
	private String EqpTypeCd;
	private String RunType;
	private String ModelType  ;
	private String RunCreationTime;
	private String RunLastUpdtTime;
	private String Notes;
	public int getRunId() {
		return RunId;
	}
	public void setRunId(int runId) {
		RunId = runId;
	}
	public String getRunName() {
		return RunName;
	}
	public void setRunName(String runName) {
		RunName = runName;
	}
	public int getScheduleId() {
		return ScheduleId;
	}
	public void setScheduleId(int scheduleId) {
		ScheduleId = scheduleId;
	}
	public int getFromSchdVersionSeqNum() {
		return FromSchdVersionSeqNum;
	}
	public void setFromSchdVersionSeqNum(int fromSchdVersionSeqNum) {
		FromSchdVersionSeqNum = fromSchdVersionSeqNum;
	}
	public int getToSchdVersionSeqNum() {
		return ToSchdVersionSeqNum;
	}
	public void setToSchdVersionSeqNum(int toSchdVersionSeqNum) {
		ToSchdVersionSeqNum = toSchdVersionSeqNum;
	}
	public String getSchedulePeriod() {
		return SchedulePeriod;
	}
	public void setSchedulePeriod(String schedulePeriod) {
		SchedulePeriod = schedulePeriod;
	}
	public String getACOwner() {
		return ACOwner;
	}
	public void setACOwner(String aCOwner) {
		ACOwner = aCOwner;
	}
	public String getEqpTypeCd() {
		return EqpTypeCd;
	}
	public void setEqpTypeCd(String eqpTypeCd) {
		EqpTypeCd = eqpTypeCd;
	}
	public String getRunType() {
		return RunType;
	}
	public void setRunType(String runType) {
		RunType = runType;
	}
	public String getModelType() {
		return ModelType;
	}
	public void setModelType(String modelType) {
		ModelType = modelType;
	}
	public String getRunCreationTime() {
		return RunCreationTime;
	}
	public void setRunCreationTime(String runCreationTime) {
		RunCreationTime = runCreationTime;
	}
	public String getRunLastUpdtTime() {
		return RunLastUpdtTime;
	}
	public void setRunLastUpdtTime(String runLastUpdtTime) {
		RunLastUpdtTime = runLastUpdtTime;
	}
	public String getNotes() {
		return Notes;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}  
}
