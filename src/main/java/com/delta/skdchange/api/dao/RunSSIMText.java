package com.delta.skdchange.api.dao;

public class RunSSIMText {
	private int runId;
	private int scheduleId;
	private int schdVersionSeqNum;
	private int rowId;
	private String rowText;
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public int getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}
	public int getSchdVersionSeqNum() {
		return schdVersionSeqNum;
	}
	public void setSchdVersionSeqNum(int schdVersionSeqNum) {
		this.schdVersionSeqNum = schdVersionSeqNum;
	}
	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public String getRowText() {
		return rowText;
	}
	public void setRowText(String rowText) {
		this.rowText = rowText;
	}

}
