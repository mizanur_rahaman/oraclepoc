package com.delta.skdchange.api.dao;

import java.util.Date;

public class FlightLegs {
	public int FlightId;
	 public int ScheduleId;
	 public String EqpTypeCd;
	 public String DepartDate;
	 public String ACOwner;
	 public String OpAirline;
	 public int OpFltNum;
	 public String Orig;
	 public String Dest;
	 public String DepTime;
	 public String ArrTime;
	 public String DepTimeGmt;
	 public String ArrTimeGmt;
	 public String LegTypeCde;
	 public String RedEyeInd ;
	 public String AdjLocalDepDate;
	 public String AdjLocalArrDate;
	 public int getFlightId() {
		return FlightId;
	}
	public void setFlightId(int flightId) {
		FlightId = flightId;
	}
	public int getScheduleId() {
		return ScheduleId;
	}
	public void setScheduleId(int scheduleId) {
		ScheduleId = scheduleId;
	}
	public String getEqpTypeCd() {
		return EqpTypeCd;
	}
	public void setEqpTypeCd(String eqpTypeCd) {
		EqpTypeCd = eqpTypeCd;
	}
	public String getDepartDate() {
		return DepartDate;
	}
	public void setDepartDate(String departDate) {
		DepartDate = departDate;
	}
	public String getACOwner() {
		return ACOwner;
	}
	public void setACOwner(String aCOwner) {
		ACOwner = aCOwner;
	}
	public String getOpAirline() {
		return OpAirline;
	}
	public void setOpAirline(String opAirline) {
		OpAirline = opAirline;
	}
	public int getOpFltNum() {
		return OpFltNum;
	}
	public void setOpFltNum(int opFltNum) {
		OpFltNum = opFltNum;
	}
	public String getOrig() {
		return Orig;
	}
	public void setOrig(String orig) {
		Orig = orig;
	}
	public String getDest() {
		return Dest;
	}
	public void setDest(String dest) {
		Dest = dest;
	}
	public String getDepTime() {
		return DepTime;
	}
	public void setDepTime(String depTime) {
		DepTime = depTime;
	}
	public String getArrTime() {
		return ArrTime;
	}
	public void setArrTime(String arrTime) {
		ArrTime = arrTime;
	}
	public String getDepTimeGmt() {
		return DepTimeGmt;
	}
	public void setDepTimeGmt(String depTimeGmt) {
		DepTimeGmt = depTimeGmt;
	}
	public String getArrTimeGmt() {
		return ArrTimeGmt;
	}
	public void setArrTimeGmt(String arrTimeGmt) {
		ArrTimeGmt = arrTimeGmt;
	}
	public String getLegTypeCde() {
		return LegTypeCde;
	}
	public void setLegTypeCde(String legTypeCde) {
		LegTypeCde = legTypeCde;
	}
	public String getRedEyeInd() {
		return RedEyeInd;
	}
	public void setRedEyeInd(String redEyeInd) {
		RedEyeInd = redEyeInd;
	}
	public String getAdjLocalDepDate() {
		return AdjLocalDepDate;
	}
	public void setAdjLocalDepDate(String adjLocalDepDate) {
		AdjLocalDepDate = adjLocalDepDate;
	}
	public String getAdjLocalArrDate() {
		return AdjLocalArrDate;
	}
	public void setAdjLocalArrDate(String adjLocalArrDate) {
		AdjLocalArrDate = adjLocalArrDate;
	}

}
