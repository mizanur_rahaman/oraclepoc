package com.delta.skdchange.api.dao;

public interface SkdChangeDao {
   public void insertSSIMText(RunSSIMText runSSIMText);
   public void insertFlight(Flights flights);
   public void insertFlightLegs(FlightLegs flightLegs);
   public void uploadFiles( byte[] oldFile, byte[]newFile);
}
