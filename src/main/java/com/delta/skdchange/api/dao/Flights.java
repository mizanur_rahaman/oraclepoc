package com.delta.skdchange.api.dao;

import java.util.Date;

public class Flights {
	 public int ScheduleId;
	 public int FlightId;
	 public String  EqpTypeCd;
	 public int SegSeqNum;
	 public int LegSeqNum;
	 public String LegTypeCde;
	 public String ACOwner;
	 public String OpAirline;
	 public int OpFltNum;
	 public String Orig;
	 public String Dest;
	 public String DepTime;
	 public String ArrTime;
	 public int DepTimeGmtOffset;
	 public int ArrTimeGmtOffset;
	 public String FromDate;
	 public String ToDate;
	 public String Frequency;
	 public String CxnAirline;
	 public int CxnFltNum;
	 public int LayoverDays;
	public int getScheduleId() {
		return ScheduleId;
	}
	public void setScheduleId(int scheduleId) {
		ScheduleId = scheduleId;
	}
	public int getFlightId() {
		return FlightId;
	}
	public void setFlightId(int flightId) {
		FlightId = flightId;
	}
	public String getEqpTypeCd() {
		return EqpTypeCd;
	}
	public void setEqpTypeCd(String eqpTypeCd) {
		EqpTypeCd = eqpTypeCd;
	}
	public int getSegSeqNum() {
		return SegSeqNum;
	}
	public void setSegSeqNum(int segSeqNum) {
		SegSeqNum = segSeqNum;
	}
	public int getLegSeqNum() {
		return LegSeqNum;
	}
	public void setLegSeqNum(int legSeqNum) {
		LegSeqNum = legSeqNum;
	}
	public String getLegTypeCde() {
		return LegTypeCde;
	}
	public void setLegTypeCde(String legTypeCde) {
		LegTypeCde = legTypeCde;
	}
	public String getACOwner() {
		return ACOwner;
	}
	public void setACOwner(String aCOwner) {
		ACOwner = aCOwner;
	}
	public String getOpAirline() {
		return OpAirline;
	}
	public void setOpAirline(String opAirline) {
		OpAirline = opAirline;
	}
	public int getOpFltNum() {
		return OpFltNum;
	}
	public void setOpFltNum(int opFltNum) {
		OpFltNum = opFltNum;
	}
	public String getOrig() {
		return Orig;
	}
	public void setOrig(String orig) {
		Orig = orig;
	}
	public String getDest() {
		return Dest;
	}
	public void setDest(String dest) {
		Dest = dest;
	}
	public String getDepTime() {
		return DepTime;
	}
	public void setDepTime(String depTime) {
		DepTime = depTime;
	}
	public String getArrTime() {
		return ArrTime;
	}
	public void setArrTime(String arrTime) {
		ArrTime = arrTime;
	}
	public int getDepTimeGmtOffset() {
		return DepTimeGmtOffset;
	}
	public void setDepTimeGmtOffset(int depTimeGmtOffset) {
		DepTimeGmtOffset = depTimeGmtOffset;
	}
	public int getArrTimeGmtOffset() {
		return ArrTimeGmtOffset;
	}
	public void setArrTimeGmtOffset(int arrTimeGmtOffset) {
		ArrTimeGmtOffset = arrTimeGmtOffset;
	}
	public String getFromDate() {
		return FromDate;
	}
	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}
	public String getToDate() {
		return ToDate;
	}
	public void setToDate(String toDate) {
		ToDate = toDate;
	}
	public String getFrequency() {
		return Frequency;
	}
	public void setFrequency(String frequency) {
		Frequency = frequency;
	}
	public String getCxnAirline() {
		return CxnAirline;
	}
	public void setCxnAirline(String cxnAirline) {
		CxnAirline = cxnAirline;
	}
	public int getCxnFltNum() {
		return CxnFltNum;
	}
	public void setCxnFltNum(int cxnFltNum) {
		CxnFltNum = cxnFltNum;
	}
	public int getLayoverDays() {
		return LayoverDays;
	}
	public void setLayoverDays(int layoverDays) {
		LayoverDays = layoverDays;
	}
}
