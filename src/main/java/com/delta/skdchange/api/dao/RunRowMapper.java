package com.delta.skdchange.api.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RunRowMapper implements RowMapper<Run>
{
    public Run mapRow(ResultSet rs, int rowNum) throws SQLException {
        Run run = new Run();
        run.setRunId(rs.getInt("RunId"));
        run.setScheduleId(rs.getInt("ScheduleId"));
        return run;
    }
}

