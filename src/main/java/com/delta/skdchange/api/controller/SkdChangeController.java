package com.delta.skdchange.api.controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.delta.skdchange.api.dao.FlightLegs;
import com.delta.skdchange.api.dao.Flights;
import com.delta.skdchange.api.dao.RunSSIMText;
import com.delta.skdchange.api.service.SkdChangeServiceImpl;

@RestController
public class SkdChangeController {
	@Autowired SkdChangeServiceImpl skdChangeService = new SkdChangeServiceImpl();
	@RequestMapping(value = "/SkdChange", method = RequestMethod.GET)
	public ModelAndView SkdChange(HttpServletRequest request, HttpServletResponse response)
	{
		//readSSIMFile(); 
		 return new ModelAndView("SkdChange");
	}

	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
    public @ResponseBody String myService(@RequestParam(value = "newFile") MultipartFile newFile,
    		@RequestParam(value = "oldFile") MultipartFile oldFile) 
	{
		try
		{
			System.out.println(oldFile.getContentType() + " " + oldFile.getName() + " " + oldFile.getSize());
			System.out.println(newFile.getContentType() + " " + newFile.getName() + " " + newFile.getSize());
		   	skdChangeService.generateFlightLegs(oldFile,newFile);
		   	skdChangeService.uploadFiles(oldFile.getBytes(),newFile.getBytes());  
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return "Failed";
		}
	    return "SUCCESS";
	}
}
