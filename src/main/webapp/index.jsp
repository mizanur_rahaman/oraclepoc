<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.delta.seat.api.util.AppConstants,java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Delta Airlines</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="static/css/bootstrap.min.css">
<link rel="stylesheet" href="static/css/jquery-ui.min.css">
<link rel="stylesheet" href="static/css/style.css">

<script src="static/js/jquery-3.3.1.min.js"></script>
<script src="static/js/jquery-ui.min.js"></script>
<script src="static/js/jquery.validate.min.js"></script>
<script src="static/js/jquery.blockUI.js"></script>
<script src="static/js/bootstrap-3.3.7.min.js"></script>
<script src="static/js/app/app-visualize-script.js"></script>



<style>
</style>

<script type="text/javascript">
var base_url = "<%=request.getContextPath().toString()%>";
var defaultSelectedBatchJob = "<%=AppConstants.defaultSelectedBatchJob%>";

	$(document).ready(function() {
		bindVisualizerEvent();
		getSeatAnalyticsBatchJobs();
	});
</script>
</head>
<body>
	<div class="container" style="width: 1230px !important;">
		<div class="">
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px">
					<button class="btn btn-primary" id="seat_assignment_btn">Seat
						Assignment</button>
					<button class="btn btn-default" id="seat_score_btn">Seat
						Score</button>
					<button class="btn btn-default" id="analytics_btn">Analytics</button>
				</div>
			</div>
		</div>
		<div id="airline_seat_view">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8 panel" style="padding-top: 15px">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-md-3" for="email">PNR:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="pnr"
										placeholder="Enter PNR" name="pnr">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3"
									for="scheduleDepartureFromDate">Flight Deprt From Date:</label>
								<div class="col-md-3">
									<input type="text" class="form-control"
										id="scheduleDepartureFromDate" placeholder=""
										name="scheduleDepartureFromDate">
								</div>
								<label class="control-label col-md-3"
									for="scheduleDepartureToDate">Flight Deprt End Date:</label>
								<div class="col-md-3">
									<input type="text" class="form-control"
										id="scheduleDepartureToDate" placeholder=""
										name="scheduleDepartureToDate">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="flightNo">Flight#:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="flightNo"
										placeholder="Enter Flight No" name="flightNo">
								</div>
								<div class="col-md-3 text-right">
									<button type="button" class="btn btn-primary"
										onclick="getEvtEventTransactionIds()">Get Evt IDs</button>
								</div>
							</div>

							<div class="form-group" id="seatPassengerPreferenceCheckBoxArea">
								<div class="col-sm-offset-3 col-sm-10">
									<div class="checkbox magic-checkbox">
										<label><input type="checkbox" name="remember"
											id="seatPassengerPreferenceCheckBox"> Show Passenger
											Preference</label>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-3 col-md-offset-1 panel"
						style="padding-bottom: 10px; padding-top: 10px">
						<div class="row">
							<div class="col-md-8 text-left">Comfort + Seats</div>
							<div class="well comfort-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left">Offered Seats</div>
							<div class="well  offered-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left">Comfort + Offered Seats</div>
							<div class="well  confort-offered-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left ">Exit Row Seats</div>
							<div class="well exit-row-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left ">Bulkhead Seats</div>
							<div class="well bulkhead-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class=" col-md-8 text-left">Blocked Seats</div>
							<div class="well blocked-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left">Infant restricted Seats</div>
							<div class="well infant-restricted-seat col-md-3 legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left">Limited Recline Seats</div>
							<div class="well col-md-3  limited-recline-seat legend-well"></div>
						</div>
						<div class="row">
							<div class="col-md-8 text-left">Unavailable Seats</div>
							<div class="well col-md-3  unavailable-seat legend-well"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="row" id="transactionInfoDiv"
							style="padding-bottom: 5px"></div>
						<div class="row" id="responseMessageDiv"
							style="padding-bottom: 5px"></div>
					</div>
					<div class="col-md-4 panel" id="inventoryConuntTableDiv"></div>
				</div>

				<div class="row panel" id="SeatScorePassengerListViewDiv"
					style="padding-bottom: 10px; display: none"></div>

				<div class="row panel" id="flightInfoDiv" style="display: none"></div>
			</div>
		</div>
		<div id="airline_analytics_view" style="display: none">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8 panel" style="padding-top: 15px">
						<form class="form-horizontal" id="seat_analytics_form">
							<div class="form-group">
								<label class="control-label col-md-3" for="seatEngineEventType">Event Type:</label>
								<div class="col-md-6">
									<select id="seatEngineEventType" name="seatEngineEventType" class="form-control">									 
										
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3" for="flightOrigin">Origin:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="flightOrigin"
										placeholder="" name="flightOrigin">
								</div>

							</div>
							<div class="form-group">

								<label class="control-label col-md-3" for="flightDestination">Destination:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="flightDestination"
										placeholder="" name="flightDestination">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="seatAnalyticsFlightNo">Flight#:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="seatAnalyticsFlightNo"
										placeholder="Enter Flight No" name="seatAnalyticsFlightNo">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"
									for="seatAnalyticsDepartureFromDate">Flight Deprt From Date:</label>
								<div class="col-md-6">
									<input type="text" class="form-control"
										id="seatAnalyticsDepartureFromDate" placeholder=""
										name="seatAnalyticsDepartureFromDate">
								</div>
							</div>
							<div class="form-group">
															<label class="control-label col-md-3"
									for="seatAnalyticsDepartureToDate">Flight Deprt End Date:</label>
								<div class="col-md-6">
									<input type="text" class="form-control"
										id="seatAnalyticsDepartureToDate" placeholder=""
										name="seatAnalyticsDepartureToDate">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-3" for="seatRecommendation">Seat
									Recommendation</label>
								<div class="col-md-6">
									<select id="seatRecommendation" name="seatRecommendation"
										class="form-control">
										<option
											value="<%=AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineValue%>"><%=AppConstants.SeatEngineSearchCriteria.SeatRecommendationbySeatEngineText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineValue%>"><%=AppConstants.SeatEngineSearchCriteria.NoSeatRecommendationbySeatEngineText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfValue%>"><%=AppConstants.SeatEngineSearchCriteria.UnassignedPaxByTpfText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteria.UnseatedPaxValue%>"><%=AppConstants.SeatEngineSearchCriteria.UnseatedPaxText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXValue%>"><%=AppConstants.SeatEngineSearchCriteria.SeatRecommendationForUnseatedPAXText%></option>	
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="seatCriteria">Seat
									Criteria</label>
								<div class="col-md-6">
									<select id="seatCriteria" name="seatCriteria"
										class="form-control">
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.SinglePartyPNRValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.SinglePartyPNRText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.MultiPartyPNRValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.MultiPartyPNRText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.FamilyPNRsValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.FamilyPNRsText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.DiamondMedallionValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.DiamondMedallionText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.PlatinumMedallionValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.PlatinumMedallionText%></option>	
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.GoldMedallionValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.GoldMedallionText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.SilverMedallionValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.SilverMedallionText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.BaseMedallionValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.BaseMedallionText%></option>
										<option
											value="<%=AppConstants.SeatEngineSearchCriteriaType.NonLoyalityProgramPassengerValue%>"><%=AppConstants.SeatEngineSearchCriteriaType.NonLoyalityProgramPassengerText%></option>

									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3 text-left">
									<button type="submit" class="btn btn-primary" >Search</button>
								</div>
							</div>
						</form>
						<div id="showAnalyticsDataDiv" style="margin-top:15px">
					 
						</div>
					</div>
				</div>
                <div class="row" id="seat_analytics_transaction_list_view" style="display:none">
	                <div class="col-md-12 panel panel-primary" style="padding-top: 15px">
	                	<div class="panel-heading">Transactions</div>
  						<div class="panel-body">
  						<table class="table table-striped">
						    <thead>
						      <tr>
						        <th>Flight No</th>
						        <th>Origin</th>
						        <th>Destination</th>
						        <th>Departure Date</th>
						        <th>Total PNR</th>
						        <th>Total Passenger</th>
						        <th>Show Passengers</th>
						      </tr>
						    </thead>
						    <tbody id="seat_analytics_transaction_list_view_table_data">
						     
						    </tbody>
					    </table>
  						</div>
	                	
	                </div>
                </div>
			</div>
		</div>

<!-- Modal passenger -->
		<div id="seat_analytics_passenger_list_modal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Passengers</h4>
					</div>
					<div class="modal-body">
	                	<table class="table table-striped">
						    <thead>
						      <tr>
						        <th>First Name</th>
						        <th>Last Name</th>
						        <th>Traveler Id</th>
						        <th>Min Age Range</th>
						        <th>Loyalty Tier Code</th>
						        <th>Seat Id</th>
						        <th>SSRs</th>
						        <th>Loyalty Member Preferences</th>
						       
						      </tr>
						    </thead>
						    <tbody id="seat_analytics_passenger_list_view_table_data">
						     
						    </tbody>
					    </table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		<!-- Modal transaction -->
		<div id="transactionModal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Select transaction</h4>
					</div>
					<div class="modal-body" id="evtTransactionListDiv"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	</div>

</body>
</html>

