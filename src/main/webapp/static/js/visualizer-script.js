String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}
var clear_forms = function () {
    $('#begin_date, #end_date, #event_id_text, #flight_num_text, #dprt_date_text, #origin_text').val("");
    $("#eventId, #flights").html('<option value="0">Select</option>');
}

    function processSingleEventId(eventIdText) {
        $('#begin_date, #end_date, #flight_num_text, #dprt_date_text, #origin_text').val("");
        $("#eventId, #flights").html('<option value="0">Select</option>');
        $("#source_table, #destination_table, #source_seats, #destination_seats, #title_div").html("");
        $("#legend_table, #pnr_div").hide();
        var event_id = $("#event_id_text").val();
        if (event_id.trim() == "") {
            alert("Please Enter a Valid Event ID");
        }
        var url = base_url + "/eventIdExists/" + event_id;
        $.get(url, function (data) {
            if (data.exists == true) {
                var option = $('<option></option>');
                $(option).attr("value", event_id);
                $(option).html(event_id);
                $("#eventId").append(option);
                $("#eventId").val(event_id);
                $("#eventId").trigger("change");
            } else {
                alert("Invalid Event ID");
                $('#event_id_text').val(' ');
            }
        });
    }
var source_done = false;
$.ui.autocomplete.filter = function (array, term) {
    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
    return $.grep(array, function (value) {
        return matcher.test(value.label || value.value || value);
    });
};
$(function () {
    $("#origin_text").autocomplete({
        source: base_url + "/getDistinctStationCodes",
        minLength: 1
    });
    setEventTypes();
    $("#dprt_date_text, #begin_date, #end_date").datepicker({
        dateFormat: "yy-mm-dd"
    });
    $("#event_id_submit2").click(

    function () {
        $('#begin_date, #end_date, #event_id_text').val("");
        $("#eventId, #flights").html('<option value="0">Select</option>');
        $("#source_table, #destination_table, #source_seats, #destination_seats, #title_div").html("");
        $("#legend_table, #pnr_div").hide();
        var origin_text = "blank";
        var flight_num_text = $("#flight_num_text").val();
        var flt_type = $('input:radio[name=flt_type]:checked').val();
        if (flight_num_text.trim().length < 1) {
            alert("Flight# can not be less than 1 char long!!");
            $("#flight_num_text").val('');
            $("#dprt_date_text").val('');
            $("#origin_text").val('');
            return;
        }
        if ($("#origin_text").val().length >= 1) {
            origin_text = $("#origin_text").val();
            if (origin_text.length < 3) {
                alert("Origin can not be less than 3 char long: [" + origin_text + "]");
                $("#flight_num_text").val('');
                $("#dprt_date_text").val('');
                $("#origin_text").val('');
                return;
            }
        }
        // if user enter flt# without leading zero
        if (flight_num_text.length == 3 && flight_num_text.length != 4) {
            flight_num_text = '0' + flight_num_text;
        } else if (flight_num_text.length == 2 && flight_num_text.length != 4) {
            flight_num_text = '00' + flight_num_text;
        } else if (flight_num_text.length == 1 && flight_num_text.length != 4) {
            flight_num_text = '000' + flight_num_text;
        } else if (flight_num_text.length == 4) {
            // no need to do anything...
        } else {
            alert("Flight# needs to be between 1 and 4 chars long!!!");
        }
        var dprt_date_val = $("#dprt_date_text").val().trim();
        if (dprt_date_val == "") {
            alert("Please Enter a Departure Date");
            $("#flight_num_text").val('');
            $("#dprt_date_text").val('');
            $("#origin_text").val('');
            return;
        }
        var url = base_url + "/getEventIdsFromCriteria/" + flight_num_text + "/" + dprt_date_val + "/" + origin_text + "/" + flt_type;
        $.get(url,

        function (data) {
            if (data.length == 0) {
                alert("No Event ID exists for given criteria");
            } else {
                var div = $("<div></div>");
                /*Code change for W cabin - TCS starts*/
                var table =$('<table class="tbl"><tr><th  width="650px" font-size="15px" >Event Id</th><th  width="250px" font-size="15px">RunTimeStamp</th></tr></table>');
                $(div).append(table);
                /*Code change for W cabin - TCS end*/
                for (i = 0; i < data.length; i++) {
                	  /*Code change for W cabin - TCS starts*/
                	   event_table = $('<table class="tbl"><tr><td align="center" class="event_id_link" width="620px" font-size="12px" >'+data[i].description+'</td><td align="center"  width="260px" font-size="12px">'+ 
                			 data[i].runTimeStamp + ' </td></tr> </table>');
                	 /*var a = $("<a></a>");
                       $(a).html(data[i]);
                       $(a).attr("href", 'javascript:void(0)');
                       $(a).addClass("event_id_link");
                       $(div).append(a);
                       $(div).append("<br />");*/
                	   $(div).append(event_table);
                	   /*Code change for W cabin - TCS end*/
                }
                $(div).dialog({
                    close: function (event, ui) {
                        $(this).dialog('destroy').remove();
                    },
                    /*Code change for W cabin - TCS starts
                    width: 500,*/ 
                    width: 700, 
                    /*Code change for W cabin - TCS end*/
                    modal: true,
                    title: "The Following Event IDs Were Found:"
                });
                $(".event_id_link").click(

                function () {
                    var event_id = $(this).html();
                    $(div).dialog("close");
                    var option = $('<option></option>');
                    $(option).attr("value", event_id);
                    $(option).html(event_id);
                    $("#eventId").append(option);
                    $("#eventId").val(event_id);
                    $("#eventId").trigger("change");
                });
            }
        });
    });
    $("#pnr_submit").click(function () {
        $("#clear_page").trigger('click');
        var ref = $("#pnr_text").val().trim().toUpperCase();
        var found = false;
        $('.pnr_a_destination, .pnr_a_source').each(function () {
            if ($(this).attr('ref') == ref) {
                found = true;
                $(this).addClass('bolded');
                $(this).parent().addClass('bordered');
            }
        }).promise().done(function () {
            if (found == false) {
                alert("PNR does not exist in this window");
            }
        });
    });
    $("#event_id_submit").click(

    function () {
        $('#begin_date, #end_date, #flight_num_text, #dprt_date_text, #origin_text').val("");
        $("#eventId, #flights").html('<option value="0">Select</option>');
        $("#source_table, #destination_table, #source_seats, #destination_seats, #title_div").html("");
        $("#legend_table, #pnr_div").hide();
        var event_id = $("#event_id_text").val();
        if (event_id.trim() == "") {
            alert("Please Enter a Valid Event ID");
        }
        var url = base_url + "/eventIdExists/" + event_id;
        $.get(url, function (data) {
            if (data.exists == true) {
                var option = $('<option></option>');
                $(option).attr("value", event_id);
                $(option).html(event_id);
                $("#eventId").append(option);
                $("#eventId").val(event_id);
                $("#eventId").trigger("change");
            } else {
                alert("Invalid Event ID");
            }
        });
    });
    $("#event_type_submit").click(

    function () {
        $("eventId").empty().append('<option value="0" size="42">Select</option>');
        $('#event_id_text, #flight_num_text, #dprt_date_text, #origin_text, #pnr_text').val("");
        $("#eventId, #flights").html('<option value="0">Select</option>');
        $("#source_table, #destination_table, #source_seats, #destination_seats, #title_div").html("");
        $("#legend_table, #pnr_div").hide();
        event_type_text = $("#eventType").val();
        begin_date_text = $("#begin_date").val();
        end_date_text = $("#end_date").val();
        if (begin_date_text.trim() == "" || end_date_text.trim() == "") {
            alert("Begin and End Date Must Be FIlled");
            return;
        }
        if (event_type_text == "0") {
            alert("Please select Event Type");
            return;
        }
        if (event_type_text != 0 && begin_date_text != "" && end_date_text != "") {
            $("#eventId").html("");
            $("#pnr_div").hide();
            $("#eventId").append('<option value="0">Select</option>');
            var url = base_url + "/getAllDistinctEventids/" + event_type_text + "/" + begin_date_text + "/" + end_date_text;
            if (begin_date_text > end_date_text) {
                alert("From date can't be more than End date!!");
                return;
            }
            $.blockUI({
                // message : '<h6><img
                // src="../image/spinner.gif" />Do NOT click
                // anything on this window.</h6>'
                message: '<img src=' + base_url + '/images/spinner.gif>'
            });
            $.ajax({
                type: "GET",
                url: url,
                // beforeSend : function() {
                // $.blockUI({
                // message : '<h6><img
                // src="../image/spinner.gif" />Do NOT
                // click anything on this window.</h6>'
                // });
                //
                // },
                error: function () {
                    alert("server error!!");
                },
                complete: function () {
                    $(document).ajaxStop($.unblockUI);
                },
                success: function (data) {
                    if (data.length != 0) {
                        if (data.length >= 5000) {
                            alert("Your search criteria brought back more than 5k eventIds. So, putting first 5k eventIds under the drop down.");
                            data.length = 5000;
                        }
                        for (var i = 0; i < data.length; i++) {
                            var option = $('<option></option>');
                            $(option).attr("value", data[i]);
                            $(option).html(data[i]);
                            $("#eventId").append(option);
                        }
                    } else {
                        alert("No Event IDs found between the given dates");
                        $("#flights").html("");
                        $("#flights").append('<option value="0">Select</option>');
                    }
                }
            });
        }
    });
    $("#eventId").change(

    function () {
        $('#begin_date, #end_date, #event_id_text, #flight_num_text, #dprt_date_text, #origin_text,  #pnr_text').val("");
        $("#source_table, #destination_table, #source_seats, #destination_seats, #title_div").html("");
        $("#legend_table, #pnr_div").hide();
        $("#pnr_div").hide();
        var eventId = $("#eventId").val();
        if (eventId.length > 0) {
            $("#flights").html("");
            var url = base_url + "/getDistinctFlights/" + eventId;
            $.get(url, function (data) {
                for (var i = 0; i < data.length; i++) {
                    var option = $('<option></option>');
                    $(option).attr("value", data[i].id);
                    $(option).html(data[i].description);
                    $("#flights").append(option);
                }
                $("#flights").trigger('change');
            });
        }
    });
    $("#flights").change(

    function () {
        if ($("#flights").val() != 0) {
            source_done = false;
            $("#destination_table").css("margin-left", 10 + "px");
            $("#destination_seats").css("margin-left", 10 + "px");
            var split = $("#flights").val().split("/");
            var eventId = $("#eventId").val();
            var flightLegId = split[0];
            var srcFlightOrg = split[1];
            var srcFlightDst = split[2];
            var tgtFlightOrg = split[3];
            var tgtFlightDst = split[4];
            var srcCassCd = split[5];
            var srcFlightNum = split[6];
            var srcFlightDate = split[7];
            $('#source_table').html("");
            var source_table_url = base_url + "/getSourceFlightData/" + eventId + "/" + srcFlightOrg + "/" + srcFlightDst + "/" + flightLegId;
            create_table(source_table_url, '#source_table', 'source');
            $('#destination_table').html("");
            var destination_table_url = base_url + "/getDestFlightData/" + eventId + "/" + tgtFlightOrg + "/" + tgtFlightDst + "/" + flightLegId + "/" + srcFlightOrg + "/" + srcFlightDst;
            create_table(destination_table_url, '#destination_table', 'destination');
            $("#destination_seats").html("");
            var destination_seats_url = base_url + "/getTargetSeatData/" + eventId + "/" + flightLegId + "/" + "TRGT";
            var destination_pnr_url = base_url + "/getDestSeatMapping/" + eventId + "/" + srcFlightOrg + "/" + srcFlightDst + "/" + flightLegId + "/" + srcFlightNum + "/" + srcCassCd + "/" + srcFlightDate;
            var destination_types_url = base_url + "/getTargetSeatTypes/" + eventId + "/" + flightLegId + "/" + "TRGT" + "/" + srcFlightOrg + "/" + srcFlightDst + "/" + srcFlightNum + "/" + srcCassCd + "/" + srcFlightDate;
            var destination_title_url = base_url + "/getDestSeatTitle/" + eventId + "/" + srcFlightOrg + "/" + srcFlightDst + "/" + flightLegId + "/" + srcFlightNum + "/" + srcCassCd + "/" + srcFlightDate;
            create_seats(destination_seats_url, '#destination_seats', 'destination', destination_pnr_url, destination_types_url, destination_title_url);
            $("#source_seats").html("");
            var source_seats_url = base_url + "/getSourceSeatData/" + eventId + "/" + srcCassCd + "/" + "SRC";
            var source_pnr_url = base_url + "/getSourceSeatMapping/" + eventId + "/" + srcFlightOrg + "/" + srcFlightDst + "/" + flightLegId + "/" + srcFlightNum + "/" + srcCassCd + "/" + srcFlightDate;
            var source_types_url = base_url + "/getSourceSeatTypes/" + eventId + "/" + srcCassCd + "/" + "SRC";
            var source_title_url = base_url + "/getSourceSeatTitle/" + eventId + "/" + srcFlightOrg + "/" + srcFlightDst + "/" + flightLegId + "/" + srcFlightNum + "/" + srcCassCd + "/" + srcFlightDate;
            create_seats(source_seats_url, '#source_seats', 'source', source_pnr_url, source_types_url, source_title_url);
        }
    });
    $("#clear_page").click(function () {
        $('.pnr_a').removeClass('bolded');
        $('.pnr_a').parent().removeClass('bordered');
    });
    $('body').click(function () {
        $("#contextmenu").remove();
    });
});
var setEventTypes = function () {
    var url = base_url + "/getAllDistinctEventType";
    $.get(url, function (data) {
        for (var i = 0; i < data.length; i++) {
            var option = $('<option></option>');
            $(option).attr("value", data[i]);
            $(option).html(data[i]);
            $("#eventType").append(option);
        }
    });
};
var alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M'];
var create_table = function (url, div, type) {
    $("#legend_table").show();
    $.get(url,

    function (data) {
        var table;
        var thru_pax = 0;
        var avg_row_change = 0;
        var fcy_total = 0;
        var fcy_occupied = 0;
        if (type == 'source') {
            table = $('<table class="tbl"><tr><th>Type</th><th>Total</th><th>Occupied</th></tr></table>');
        }
        for (var i = 0; i < data.length; i++) {
            if (data[i].type == 'Thru Passengers') {
                thru_pax = data[i].total;
                if (type == 'destination') {
                    if (thru_pax > 0) {
                        table = $('<table class="tbl"><tr><td colspan="5" align="right">Thru Passengers</td><td><a id="thru_pax" class="statistic_links" href="javascript:void(0)" ref="refThruPax">' + thru_pax + '</a></td></tr><tr><th>Type</th><th>Total</th><th>Occupied</th><th>Preserved</th><th>New</th><th>Displaced</th></tr></table>');
                    } else {
                        table = $('<table class="tbl"><tr><td colspan="5" align="right">Thru Passengers</td><td>0</td></tr><tr><th>Type</th><th>Total</th><th>Occupied</th><th>Preserved</th><th>New</th><th>Displaced</th></tr></table>');
                    }
                }
                continue;
            }
            if (data[i].type == 'Average Row Change') {
                avg_row_change = data[i].totalDouble;
                continue;
            }
           /*  Code change for W cabin - TCS starts
            if (type == 'destination' && data[i].type == 'Sum of F/C and Y') {*/
            if (type == 'destination' && data[i].type == 'Sum of F/C, W and Y') {
            // Code change for W cabin - TCS end
                fcy_total = data[i].total;
                fcy_occupied = data[i].occupied;
                continue;
            }
            var tr = $('<tr></tr>');
            $(table).append(tr);
            var seat_type = $('<td></td>');
            $(seat_type).html(data[i].type);
            $(tr).append(seat_type);
            var total = $('<td></td>');
            $(total).html(data[i].total);
            $(tr).append(total);
            var occupied = $('<td></td>');
            $(occupied).html(data[i].occupied);
            $(tr).append(occupied);
            if (type == 'destination') {
                var preservedPaxSeatTd = $('<td></td>');
                var dataType = data[i].type;
                var preservedPaxSeatData = data[i].preservedPaxSeat;
                if (preservedPaxSeatData == '0') {
                    $(preservedPaxSeatTd).html(preservedPaxSeatData);
                    $(tr).append(preservedPaxSeatTd);
                } else {
                    var a = $('<a></a>');
                    $(a).addClass("statistic_links");
                    $(a).attr('href', 'javascript:void(0)');
                    if (dataType == 'First Cabin Seats') {
                        $(a).attr('ref', data[i].refFCbnPresvd);
                        $(a).addClass('FirstCabinPreserved_Seat');
                    } else if (dataType == 'Business Cabin Seats') {
                        $(a).attr('ref', data[i].refBizCbnPresvd);
                        $(a).addClass('BizCabinPreserved_Seat');
                     // Code change for W cabin - TCS starts
                    } else if (dataType == 'C + Cabin Seats') {
                        $(a).attr('ref', data[i].refWCbnPresvd);
                        $(a).addClass('ComfortPlusCabinPreserved_Seat');
                    } else if (dataType == 'Main Cabin Seats') {
                     // Code change for W cabin - TCS end
                        $(a).attr('ref', data[i].refCoachCbnPresvd);
                        $(a).addClass('CoachCabinPreserved_Seat');
                     // Code change for W cabin - TCS starts
                    } else if (dataType == 'Comfort + Seats') {
                     // Code change for W cabin - TCS end
                    	$(a).attr('ref', data[i].refEconPresvd);
                        $(a).addClass('EconCabinPreserved_Seat');
                    } else if (dataType == 'Preferred Seats') {
                        $(a).attr('ref', data[i].refPrefdPresvd);
                        $(a).addClass('PrefdPreserved_Seat');
                    } else if (dataType == 'Exit Seats') {
                        $(a).attr('ref', data[i].refExitPresvd);
                        $(a).addClass('ExitPreserved_Seat');
                    } else if (dataType == 'Window Seats') {
                        $(a).attr('ref', data[i].refWinPresvd);
                        $(a).addClass('WinPreserved_Seat');
                    } else if (dataType == 'Aisle Seats') {
                        $(a).attr('ref', data[i].refAislePresvd);
                        $(a).addClass('AislePreserved_Seat');
                     // Code change for HDA - TCS starts
                    } else if (dataType == 'HDA Seats') {
                        $(a).attr('ref', data[i].refHdcpdPreserved);
                        $(a).addClass('HdcpdPreserved_Seat');
                     // Code change for HDA - TCS end
                    } else if (dataType == 'Blocked Seat') {
                        $(a).attr('ref', data[i].refBlkdPresvd);
                        $(a).addClass('BlkdPreserved_Seat');
                    } else if (dataType == 'Limited Recline') {
                        $(a).attr('ref', data[i].refLtdPresvd);
                        $(a).addClass('LtdPreserved_Seat');
                    }
                    $(a).html(preservedPaxSeatData);
                    $(preservedPaxSeatTd).html(a);
                    $(tr).append(preservedPaxSeatTd);
                }
                var newPaxSeatTd = $('<td></td>');
                var newPaxSeatData = data[i].newPaxSeat;
                if (newPaxSeatData == '0') {
                    $(newPaxSeatTd).html(newPaxSeatData);
                    $(tr).append(newPaxSeatTd);
                } else {
                    var a = $('<a></a>');
                    $(a).attr('href', 'javascript:void(0)');
                    $(a).addClass("statistic_links");
                    if (dataType == 'First Cabin Seats') {
                        $(a).attr('ref', data[i].refFCbnNew);
                        $(a).addClass('FirstCabinNew_Seat');
                    } else if (dataType == 'Business Cabin Seats') {
                        $(a).attr('ref', data[i].refBizCbnNew);
                        $(a).addClass('BusinessCabinNew_Seat');
                     // Code change for W cabin - TCS starts
                    } else if (dataType == 'C + Cabin Seats') {
                        $(a).attr('ref', data[i].refWCbnNew);
                        $(a).addClass('ComfortPlusCabinNew_Seat');
                    } else if (dataType == 'Main Cabin Seats') {
                     // Code change for W cabin - TCS end
                        $(a).attr('ref', data[i].refCoachCbnNew);
                        $(a).addClass('CoachCabinNew_Seat');
                     // Code change for W cabin - TCS starts
                    } else if (dataType == 'Comfort + Seats') {
                   	// Code change for W cabin - TCS end
                    	 $(a).attr('ref', data[i].refEconNew);
                         $(a).addClass('EconNew_Seat');
                    } else if (dataType == 'Preferred Seats') {
                        $(a).attr('ref', data[i].refPrefdNew);
                        $(a).addClass('PrefdNew_Seat');
                    } else if (dataType == 'Exit Seats') {
                        $(a).attr('ref', data[i].refExitNew);
                        $(a).addClass('ExitNew_Seat');
                    } else if (dataType == 'Window Seats') {
                        $(a).attr('ref', data[i].refWinNew);
                        $(a).addClass('WinNew_Seat');
                    } else if (dataType == 'Aisle Seats') {
                        $(a).attr('ref', data[i].refAisleNew);
                        $(a).addClass('AisleNew_Seat');
                    // Code change for HDA - TCS starts
                    } else if (dataType == 'HDA Seats') {
                        $(a).attr('ref', data[i].refHdcpdNew);
                        $(a).addClass('HdcpdNew_Seat');
                    // Code change for HDA - TCS end
                    } else if (dataType == 'Blocked Seat') {
                        $(a).attr('ref', data[i].refBlkdNew);
                        $(a).addClass('BlkdNew_Seat');
                    } else if (dataType == 'Limited Recline') {
                        $(a).attr('ref', data[i].refLtdNew);
                        $(a).addClass('LtdNew_Seat');
                    }
                    $(a).html(newPaxSeatData);
                    $(newPaxSeatTd).html(a);
                    $(tr).append(newPaxSeatTd);
                }
                var displacedPaxSeatTd = $('<td></td>');
                var displacedPaxSeatData = data[i].displacedPaxSeat;
                if (displacedPaxSeatData == '0') {
                    $(displacedPaxSeatTd).html(displacedPaxSeatData);
                    $(tr).append(displacedPaxSeatTd);
                } else {
                    var a = $('<a></a>');
                    $(a).attr('href', 'javascript:void(0)');
                    $(a).addClass("statistic_links");
                    if (dataType == 'First Cabin Seats') {
                        $(a).attr('ref', data[i].refFCbnDspld);
                        $(a).addClass('FirstCabinDisplaced_Seat');
                    } else if (dataType == 'Business Cabin Seats') {
                        $(a).attr('ref', data[i].refBizCbnDspld);
                        $(a).addClass('BusinessCabinDisplaced_Seat');
                    // Code change for W cabin - TCS starts
                    } else if (dataType == 'C + Cabin Seats') {
                        $(a).attr('ref', data[i].refWCbnDspld);
                        $(a).addClass('ComfortPlusCabinDisplaced_Seat');
                    } else if (dataType == 'Main Cabin Seats') {
                    // Code change for W cabin - TCS end
                        $(a).attr('ref', data[i].refCoachCbnDspld);
                        $(a).addClass('CoachCabinDisplaced_Seat');
                    // Code change for W cabin - TCS starts
                    } else if (dataType == 'Comfort + Seats') {
                    // Code change for W cabin - TCS end
                    	 $(a).attr('ref', data[i].refEconDspld);
                         $(a).addClass('EconDisplaced_Seat');
                    } else if (dataType == 'Preferred Seats') {
                        $(a).attr('ref', data[i].refPrefdDspld);
                        $(a).addClass('PrefdDisplaced_Seat');
                    } else if (dataType == 'Exit Seats') {
                        $(a).attr('ref', data[i].refExitDspld);
                        $(a).addClass('ExitDisplaced_Seat');
                    } else if (dataType == 'Window Seats') {
                        $(a).attr('ref', data[i].refWinDspld);
                        $(a).addClass('WinDisplaced_Seat');
                    } else if (dataType == 'Aisle Seats') {
                        $(a).attr('ref', data[i].refAisleDspld);
                        $(a).addClass('AisleDisplaced_Seat');
                     // Code change for HDA - TCS starts
                    } else if (dataType == 'HDA Seats') {
                        $(a).attr('ref', data[i].refHdcpdDspld);
                        $(a).addClass('HdcpdDisplaced_Seat');
                     // Code change for HDA - TCS end
                    } else if (dataType == 'Blocked Seat') {
                        $(a).attr('ref', data[i].refBlkdDspld);
                        $(a).addClass('BlkdDisplaced_Seat');
                    } else if (dataType == 'Limited Recline') {
                        $(a).attr('ref', data[i].refLtdDspld);
                        $(a).addClass('LtdDisplaced_Seat');
                    }
                    $(a).html(displacedPaxSeatData);
                    $(displacedPaxSeatTd).html(a);
                    $(tr).append(displacedPaxSeatTd);
                }
            }
        }
        if (type == 'destination') {
        	// Code change for W cabin - TCS starts
            //$(table).append('<tr><td>Sum of F/C and Y</td><td>' + fcy_total + '</td><td>' + fcy_occupied + '</td><td colspan="2" align="right">Avg Row Change:</td><td id="avg_row_change">' + avg_row_change + '</td></tr>');
        	$(table).append('<tr><td>Sum of F/C, W and Y</td><td>' + fcy_total + '</td><td>' + fcy_occupied + '</td><td colspan="2" align="right">Avg Row Change:</td><td id="avg_row_change">' + avg_row_change + '</td></tr>');
        	// Code change for W cabin - TCS end
        }
        $(div).append(table);
        // $("#thru_pax").html(thru_pax);
        $('.statistic_links').click(

        function () {
            $("#clear_page").trigger('click');
            var seat_type = $(this).attr('ref');
            $('.pnr_a_destination').each(
            function () {
                var seat_types = $(this).attr('type');
                if (seat_types.indexOf(seat_type) != -1) {
                    var pnr = $(this).attr('ref');
                    var pax = $(this).attr('rel');
                    $(this).addClass('bolded');
                    $(this).parent().addClass('bordered');                    
                }
            });
            //new logic
            $('.pnr_a_source').each(
            function () {
                var seat_types = $(this).attr('type');
                if (seat_types.indexOf(seat_type) != -1) {
                    var pnr = $(this).attr('ref');
                    var pax = $(this).attr('rel');
                    $(this).addClass('bolded');
                    $(this).parent().addClass('bordered');                    
                }
            });
        });
    });
};
var create_seats = function (url, div, type, pnr_url, types_url, title_url) {
    $.get(url,

    function (data) {
        if (type == 'source' && data.length == 0) {
            alert("Warning: Seat Chart Data is not available for the source flight");
            return;
        } else if (type == 'destination' && data.length == 0) {
            alert("Warning: Seat Chart Data is not available for the target flight");
            return;
        }
        var total_rows = 0;
        var total_columns = 0;
        for (i = 0; i < data.length; i++) {
            for (j = 0; j < data[i].seats.length; j++) {
                total_rows++;
                for (k = 0; k < data[i].seats[j].columns.length; k++) {
                    if (parseInt(data[i].seats[j].columns[k]) > parseInt(total_columns)) {
                        total_columns = data[i].seats[j].columns[k];
                    }
                }
            }
        }
        total_columns = parseInt(total_columns) + 1;
      //Code change for W cabin - TCS starts
        var table = $('<table cellspacing="5" cellpadding="2px"></table>');
      //Code change for W cabin - TCS end
        for (i = 0; i < data.length; i++) {
            var compartment_wise_max_columns = data[i].allSeats.length;
            var max_column = data[i].allSeats;
            var tr = $('<tr></tr>');
            $(table).append(tr);
            var td = $('<td></td>');
            $(tr).append(td);
            for (p = 0; p < total_columns - 1; p++) {
                var td = $('<td class="alpha"></td>');
                for (q = 0; q < max_column.length; q++) {
                    if (p == max_column[q] - 1) {
                        $(td).html(data[i].seatAlphas[q]);
                    }
                }
                $(tr).append(td);
            }
            for (j = 0; j < data[i].seats.length; j++) {
                var tr = $('<tr></tr>');
                $(table).append(tr);
                if (j == 0 || (j - 1 >= 0 && data[i].seats[j].row - data[i].seats[j - 1].row == 1)) {
                    var td = $('<td class="rownum"></td>');
                    $(td).html(data[i].seats[j].row);
                    $(tr).append(td);
                    for (p = 0; p < total_columns - 1; p++) {
                        var seat = 0;
                        var seat_position = -1;
                        for (m = 0; m < data[i].seats[j].columns.length; m++) {
                            if (p == data[i].seats[j].columns[m] - 1) {
                                seat = 1;
                                seat_position = m;
                            }
                        }
                        if (seat == 1) {
                            var td = $('<td class="seat"></td>');
                            $(td).attr("id", type + "_" + data[i].seats[j].seatIds[seat_position]);
                            $(tr).append(td);
                        } else {
                            var td = $('<td class="blank"></td>');
                            $(tr).append(td);
                        }
                    }
                } else if (j - 1 >= 0 && data[i].seats[j].row - data[i].seats[j - 1].row > 1) {
                    var difference = data[i].seats[j].row - data[i].seats[j - 1].row - 1;
                    for (m = 0; m < difference; m++) {
                        var tr = $('<tr></tr>');
                        $(table).append(tr);
                        for (p = 0; p < total_columns; p++) {
                            var td = $('<td class="blank">&nbsp;</td>');
                            $(tr).append(td);
                        }
                    }
                    var tr = $('<tr></tr>');
                    $(table).append(tr);
                    var td = $('<td class="rownum"></td>');
                    $(td).html(data[i].seats[j].row);
                    $(tr).append(td);
                    for (p = 0; p < total_columns - 1; p++) {
                        var seat = 0;
                        var seat_position = -1;
                        for (m = 0; m < data[i].seats[j].columns.length; m++) {
                            if (p == data[i].seats[j].columns[m] - 1) {
                                seat = 1;
                                seat_position = m;
                            }
                        }
                        if (seat == 1) {
                            var td = $('<td class="seat"></td>');
                            $(td).attr("id", type + "_" + data[i].seats[j].seatIds[seat_position]);
                            $(tr).append(td);
                        } else {
                            var td = $('<td class="blank"></td>');
                            $(tr).append(td);
                        }
                    }
                }
            }
            var tr = $('<tr></tr>');
            $(table).append(tr);
            for (p = 0; p < total_columns - 1; p++) {
                var td = $('<td class="blank">&nbsp;</td>');
                $(tr).append(td);
            }
        }
        $(div).html(table);
        if (type == "source") {
            source_done = true;
        }
        if (source_done == true) {
            source_done = false;
            var left_padding = $("#source_seats").width() - $("#legend_table").width() - $("#source_table").width();
            if (left_padding > 0) {
                $("#destination_table").css("margin-left", (left_padding + 12) + "px");
            } else {
                var left_padding2 = $("#legend_table").width() + $("#source_table").width() - $("#source_seats").width();
                $("#destination_seats").css("margin-left", (left_padding2 + 10) + "px");
            }
        }
        $.get(pnr_url,

        function (pnr_data) {
            if (type == 'source' && pnr_data.length == 0) {
                alert("Warning: Passenger Seat Data is not available for the source flight");
            } else if (type == 'destination' && pnr_data.length == 0) {
                alert("Warning: Passenger Seat Data is not available for the target flight");
            } else {
                $("#pnr_div").show();
            }
            for (i = 0; i < pnr_data.length; i++) {
                var a = $('<a></a>');
                $(a).attr('href', 'javascript:void(0)');
                $(a).attr('ref', pnr_data[i].recourceLocatorId);
                $(a).attr('rel', pnr_data[i].paxNumber);
                $(a).attr('type', pnr_data[i].seatType);
                $(a).attr('title', "Name: " + pnr_data[i].paxNumber + "<br />Medalion: " + pnr_data[i].medalion + "<br />SSR Type: " + pnr_data[i].ssrTypeCode + "<br />Original Cabin Cd:" + pnr_data[i].originalCabinCode + "<br />Effective Cabin Cd:" + pnr_data[i].effectiveCabinCode + "<br />Priority Score: " + pnr_data[i].priorityScore + "<br />Group: " + pnr_data[i].group);
                $(a).html(pnr_data[i].recourceLocatorId);
                $(a).addClass('pnr_a');
                $(a).addClass('pnr_a_' + type);
                $(a).addClass(type + '_' + pnr_data[i].recourceLocatorId);
                $(a).addClass('tooltip_' + type);
                id = "#" + type + "_" + pnr_data[i].seatId;
                $(id).html(a);
                $(id).addClass('pnr_td');
            }
            tooltip(type);
            $(".pnr_a_source, .pnr_a_destination").click(

            function (e) {
                $("#clear_page").trigger('click');
                var ref = $(this).attr('ref');
                $('.pnr_a_destination, .pnr_a_source').each(

                function () {
                    if ($(this).attr('ref') == ref) {
                        $(this).addClass('bolded');
                        $(this).parent().addClass('bordered');
                    }
                });
            });
            $(".pnr_a_source, .pnr_a_destination").rightClick(

            function (e) {
                xOffset = 10;
                yOffset = 20;
                var p = $("<p id='contextmenu'></p>");
                var a = $('<a></a>');
                $(a).attr('href', 'javascript:void(0)');
                $(a).attr('ref', $(this).attr('ref'));
                $(a).attr('rel', $(this).attr('rel'));
                $(a).attr('id', "contextmenulink");
                $(a).html("Show Corresponding Seat");
                var a2 = $('<a></a>');
                $(a2).attr('href', 'javascript:void(0)');
                $(a2).attr('ref', $(this).attr('ref'));
                $(a2).attr('id', "copytoclipboardlink");
                $(a2).html("Copy PNR to clip board");
                $(p).append(a);
                $(p).append("<hr />");
                $(p).append(a2);
                $("body").append(p);
                $("#contextmenu").css("top", (e.pageY - xOffset) + "px").css("left", (e.pageX + yOffset) + "px").fadeIn("fast");
                $('a#copytoclipboardlink').click(

                function () {
                    if ($.browser.msie) {
                        window.clipboardData.setData('Text', $(this).attr('ref'));
                    } else {
                        window.prompt("Copy to clipboard: Ctrl+C, Enter", $(this).attr('ref'));
                    }
                });
                $("#contextmenulink").click(

                function () {
                    $("#clear_page").trigger('click');
                    var ref = $(this).attr('ref');
                    var serial = $(this).attr('rel');
                    $('.pnr_a_destination, .pnr_a_source').each(

                    function () {
                        if ($(this).attr('ref') == ref && $(this).attr('rel') == serial) {
                            $(this).addClass('bolded');
                            $(this).parent().addClass('bordered');
                        }
                    });
                });
            });
            $.get(types_url,

            function (types_data) {
                for (i = 0; i < types_data.length; i++) {
                    var types = types_data[i].type;
                    var seats = types_data[i].seat;
                    var split_types = types.split("/");
                    for (j = 0; j < split_types.length; j++) {
                        var ids = "#" + type + "_" + seats;
                        $(ids).addClass(split_types[j]);
                    }
                }
            });
            if (type == "destination") {
                $.get(title_url,

                function (title_data) {
                    $("#title_div").html("<span class='lbl'>Event ID:" + title_data.eventId + "&nbsp;&nbsp;&nbsp;</span>" + "<span class='lbl'>Run Timestamp:" + title_data.runTimestamp + "&nbsp;&nbsp;&nbsp;</span>" + "<span class='lbl'>Target Flight#" + title_data.targetFlightNum + "&nbsp;&nbsp;&nbsp;</span>" + "<span class='lbl'> Departure Date:" + title_data.departureDate + "&nbsp;&nbsp;&nbsp;</span><br />")
                });
            }
        });
    });
};
this.tooltip = function (type) {
    /* CONFIG */
    xOffset = 10;
    yOffset = 20;
    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result
    /* END CONFIG */
    $("a.tooltip_" + type).hover(

    function (e) {
        this.t = this.title;
        this.title = "";
        $("body").append("<p id='tooltip'>" + this.t + "</p>");
        $("#tooltip").css("top", (e.pageY - xOffset) + "px").css("left", (e.pageX + yOffset) + "px").fadeIn("fast");
    }, function () {
        this.title = this.t;
        $("#tooltip").remove();
    });
    $("a.tooltip").mousemove(

    function (e) {
        $("#tooltip").css("top", (e.pageY - xOffset) + "px").css("left", (e.pageX + yOffset) + "px");
    });
};

