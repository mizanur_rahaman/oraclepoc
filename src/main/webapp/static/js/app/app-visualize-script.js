
/** new code **/
var SengEvgMsgData = null;
var selectedTravelerId = null;
var selectedRecordLocatorId = null;
function getFlightData(transactionId, flightNo, departureDate, totalPnr,offerId){
	$("#transactionModal").modal('hide');
	$.blockUI({ message: '<img src="static/images/ajax-loader.gif" />' });
	$.ajax({
	    type: "GET",
	    url: base_url+'/v1/seats/selection?transactionId='+transactionId,
	    contentType: "application/json",
	    dataType: "json",
	    success: function (data) {
	    	SengEvgMsgData = data;
	    	populateTransactionInfo(transactionId, flightNo, departureDate, totalPnr,offerId);
	    	populateAvailabelInventoryCount(data);
	    	if($("#seat_assignment_btn").hasClass('btn-primary')){
	    		populateSeatAssignmentView(data);
	    	}
	    	else{
	    		populateSeatScoretView(data);
	    	}
	    	showResponseMessage(data);
	    	$.unblockUI();
	    },
	    error: function () {
	    	alert("Something is going wrong. Please try again.");
	    	$.unblockUI();
	        console.log("Error occured");
	    }
	});
}


function getEvtEventTransactionIds(){
	var recordLocatorId = $("#pnr").val();
	var flightNo = $("#flightNo").val();
	var scheduleDepartureFromDate = $("#scheduleDepartureFromDate").val();
	var scheduleDepartureToDate	= $("#scheduleDepartureToDate").val();
	if((scheduleDepartureFromDate != undefined && scheduleDepartureFromDate.length != 0)
	&& ( scheduleDepartureToDate == undefined || scheduleDepartureToDate.length == 0 ))
	{
		alert("Please enter end date");
		return;
	}
	if((scheduleDepartureToDate != undefined && scheduleDepartureToDate.length != 0)
	&& ( scheduleDepartureFromDate == undefined || scheduleDepartureFromDate.length == 0 ))
	{
		alert("Please enter from date");
		return;
	}
	var dataArray = {
			"recordLocatorId" : recordLocatorId,
			"flightNo" : flightNo,
			"scheduleDepartureFromDate" :scheduleDepartureFromDate,
			"scheduleDepartureToDate" :scheduleDepartureToDate
	}

	$.blockUI({ message: '<img src="static/images/ajax-loader.gif" />' });
	$.ajax({
	    type: "POST",
	    url: base_url+'/v1/seats/transactions',
	    contentType: "application/json",
	    dataType: "json",
	    data : JSON.stringify(dataArray),
	    success: function (data) {
	    	populateEvtTransactionList(data);
	    	$.unblockUI();
	    },
	    error: function () {
	    	alert("Something is going wrong. Please try again.");
	        console.log("Error occured");
	        $.unblockUI();
	    }
	});
}

function getFlightAnalyticsData(){
	$.blockUI({ message: '<img src="static/images/ajax-loader.gif" />' });
	var dataArray = {
			"seatEngineEventType" : $("#seatEngineEventType").val(),
			"flightOrigin" :  $("#flightOrigin").val(),
			"flightDestination" : $("#flightDestination").val(),
			"seatAnalyticsFlightNo" :  $("#seatAnalyticsFlightNo").val(),
			"seatAnalyticsDepartureFromDate" : $("#seatAnalyticsDepartureFromDate").val(),
			"seatAnalyticsDepartureToDate" :  $("#seatAnalyticsDepartureToDate").val(),
			"seatRecommendation" : $("#seatRecommendation").val(),
			"seatCriteria" : $("#seatCriteria").val()
	}
	
	$.ajax({
	    type: "POST",
	    url: base_url+'/v1/seats/analytics',
	    contentType: "application/json",
	    dataType: "json",
	    data : JSON.stringify(dataArray),
	    success: function (data) {
	    	populateFlightAnalyticsData(data);
	    	$.unblockUI();
	    },
	    error: function () {
	    	alert("Something is going wrong. Please try again.");
	    	$.unblockUI();
	        console.log("Error occured");
	    }
	});
}

function getPassengerList(transactionId){
	$.blockUI({ message: '<img src="static/images/ajax-loader.gif" />' });
	
	$.ajax({
	    type: "GET",
	    url: base_url+'/v1/seats/passengers?transactionId='+transactionId,
	    contentType: "application/json",
	    dataType: "json",
	    success: function (data) {
	    	populateFlightAnalyticsPassengerData(data,transactionId);
	    	$.unblockUI();
	    },
	    error: function () {
	    	alert("Something is going wrong. Please try again.");
	    	$.unblockUI();
	        console.log("Error occured");
	    }
	});
}

function populateFlightAnalyticsData(response)
{
	var html = "";
	if(response.success==true)
	{
		html = '<div class="alert alert-info"><strong>'+response.message+'</strong></div>';
		if(response.showTransaction)
		{
			var transactionHtml = "";
			var size = 0;
			if(response.dataList != undefined)
				size = response.dataList.length;
			if(size == 0)
			{
			  $("#seat_analytics_transaction_list_view").hide();	
			}
			else
			{
				for(var i=0;i<size;i++)
				{
					transactionHtml += '<tr>';
					transactionHtml += '<td>'+(response.dataList[i].flightNo != undefined? response.dataList[i].flightNo  : "")+ '</td>';
					transactionHtml += '<td>'+(response.dataList[i].origin != undefined? response.dataList[i].origin  : "")+ '</td>';
					transactionHtml += '<td>'+(response.dataList[i].destination != undefined? response.dataList[i].destination  : "")+ '</td>';
					transactionHtml += '<td>'+(response.dataList[i].departureDate != undefined? response.dataList[i].departureDate  : "")+ '</td>';
					transactionHtml += '<td>'+(response.dataList[i].totalPNR != undefined? response.dataList[i].totalPNR   : "")+ '</td>';
					transactionHtml += '<td>'+(response.dataList[i].totalPassenger != undefined? response.dataList[i].totalPassenger  : "")+ '</td>';
					transactionHtml += '<td><button class="btn btn-default analytics-transaction-button" id="btn_'+response.dataList[i].transactionId+'" onclick="getPassengerList(\''+response.dataList[i].transactionId+'\')">Show Passengers</button></td>';
					transactionHtml += '</tr>';
				}
				$("#seat_analytics_transaction_list_view").show();
				$("#seat_analytics_transaction_list_view_table_data").html(transactionHtml);
				
			}
		}
		else
		{
			$("#seat_analytics_transaction_list_view").hide();
		}
		
		
	}
	else
	{
		$("#seat_analytics_transaction_list_view").hide();
		html = '<div class="alert alert-danger"><strong>'+response.message+'</strong></div>';
	}
   $("#showAnalyticsDataDiv").html(html);
}

function populateFlightAnalyticsPassengerData(response, transactionId)
{
        var transactionHtml = "";
		if(response != undefined)
			size = response.length;
		if(size == 0)
		{
		  $("#seat_analytics_passenger_list_view_table_data").html("no passenger found");	
		}
		else
		{
			for(var i=0;i<size;i++)
			{
				transactionHtml += '<tr>';
				transactionHtml += '<td>'+(response[i].firstNameNum != undefined? response[i].firstNameNum  : "")+ '</td>';
				transactionHtml += '<td>'+(response[i].lastNameNum != undefined? response[i].lastNameNum  : "")+ '</td>';
				transactionHtml += '<td>'+(response[i].travelerId != undefined? response[i].travelerId  : "")+ '</td>';
				transactionHtml += '<td>'+(response[i].ageRangeText != undefined? response[i].ageRangeText  : "")+ '</td>';
				transactionHtml += '<td>'+(response[i].loyaltyProgramTierCode != undefined? response[i].loyaltyProgramTierCode : "") + '</td>';
				transactionHtml += '<td>'+(response[i].seatId != undefined? response[i].seatId   : "")+ '</td>';
				var ssrs = "";
				if(response[i].ssrs != undefined)
				{
					var ssrSize = response[i].ssrs.length;
					for(var j=0;j<ssrSize;j++)
					{
						ssrs += j>0?", ": "";
						ssrs += response[i].ssrs[j].ssrCode;
					}
				}
				transactionHtml += '<td>'+ssrs+ '</td>';
				var loyaltyMemberPreferences = "";
				if(response[i].loyaltyMemberPreferences != undefined)
				{
					var loyaltyMemberPreferencesSize = response[i].loyaltyMemberPreferences.length;
					for(var j=0;j<loyaltyMemberPreferencesSize;j++)
					{
						loyaltyMemberPreferences += j>0?", ": "";
						loyaltyMemberPreferences += response[i].loyaltyMemberPreferences[j].preferenceCodeDesc;
					}
				}
				transactionHtml += '<td>'+loyaltyMemberPreferences+ '</td>';
				transactionHtml += '</tr>';
			}
			$("#seat_analytics_passenger_list_view_table_data").html(transactionHtml);
			
		}
		$(".analytics-transaction-button").each(function(){
			$(this).removeClass("btn-primary");
			$(this).addClass("btn-default");
		});
		$("#btn_"+transactionId).removeClass("btn-default");
		$("#btn_"+transactionId).addClass("btn-primary");
		$("#seat_analytics_passenger_list_modal").modal("show");
}

function getSeatAnalyticsBatchJobs(){
	//$.blockUI({ message: '<img src="static/images/ajax-loader.gif" />' });
	$.ajax({
	    type: "GET",
	    url: base_url+'/v1/seats/analytics/batchJobs',
	    contentType: "application/json",
	    dataType: "json",
	    success: function (data) {
	    	var html = "";
	        if(data != undefined)
	        {
	        	var size =data.length;
	        	for(var i=0;i<size;i++)
	        	{
	        		html += '<option value="'+data[i]+'">'+data[i]+'</option>"';
	        	} 	
	        }
  			$("#seatEngineEventType").html(html);
  			$('#seatEngineEventType').val(defaultSelectedBatchJob);
  			bindValidateAnayticsForm();
	    	//$.unblockUI();
	    },
	    error: function () {
	    	alert("Something is going wrong. Please try again.");
	    	//$.unblockUI();
	        console.log("Error occured");
	    }
	});
}

function bindValidateAnayticsForm()
{
	$( "#seat_analytics_form" ).validate({
		  rules: {
			  flightOrigin: {
		      required: function(element){
		            		return $("#flightDestination").val().length > 0;
		        		},
		      minlength: 3
		    },
		    flightDestination: {
		    	required: function(element){
		            		return $("#flightOrigin").val().length > 0;
		        		 },
			    minlength: 3
		    },
		    seatAnalyticsDepartureFromDate : {
		    	required: function(element){
            		return $("#seatAnalyticsDepartureToDate").val().length > 0;
        		 },
         		date : true
		    },
		    seatAnalyticsDepartureToDate : {
		    	required: function(element){
            		return $("#seatAnalyticsDepartureFromDate").val().length > 0;
        		 },
         		 date : true
		    }
		    
		  },
		  messages : {
			  flightOrigin: {
			      required: "Please enter flight origin",
			      minlength: "Origin minimum length must be three characters"
			    },
			    flightDestination: {
			    	required: "Please enter flight destination",
				    minlength: "Destination minimum length must be three characters"
			    },
			    seatAnalyticsDepartureFromDate : {
			    	required: "Please enter flight departure from date"
			    },
			    seatAnalyticsDepartureToDate : {
			    	required: "Please enter flight departure to date"
			    }
		   },
		   submitHandler: function(form){
		    	getFlightAnalyticsData();
		   }
	});
}

function showResponseMessage(data)
{

	var responseObj = JSON.parse(data.responseClob);
	if(responseObj != null && responseObj.responseMessageText != null)
	{
		$("#responseMessageDiv").html('<p style="color:red"><b>'+responseObj.responseMessageText+'</b></p>')
	}
	else
	{
		$("#responseMessageDiv").html('');
	}
}

function populateSeatAssignmentView(data)
{
	$("#flightInfoDiv").show();
	$("#SeatScorePassengerListViewDiv").hide();
	$("#seatPassengerPreferenceCheckBoxArea").show();
	populateSeatView(data);
	populateOfferedSeat(data,null);
	populatePassengerSeating(data,null);

	if($("#seatPassengerPreferenceCheckBox").prop('checked'))
	{
		showOrHidePassengerPreference(data,true);
	}
}

function populateSeatScoretView(data)
{
	selectedTravelerId = null;
	selectedRecordLocatorId = null;
	PopulateSeatScorePassengerListView(data);
	$("#flightInfoDiv").show();
	$("#seatPassengerPreferenceCheckBoxArea").hide();
	$("#list_traveler_"+selectedRecordLocatorId+"_"+selectedTravelerId).removeClass("btn-default");
	$("#list_traveler_"+selectedRecordLocatorId+"_"+selectedTravelerId).addClass("btn-primary");

	bindSeatScoreViewData(data,selectedTravelerId,selectedRecordLocatorId);
	bindPassengerListButtonEvent();

}

function bindSeatScoreViewData(data, travelerId,recordLocatorId)
{
	populateSeatScoreSeatView(data);
	populatePassengerSeating(data,travelerId);
	populateOfferedSeat(data,travelerId,recordLocatorId);
	$("html, body").animate({ scrollTop: $("#SeatScorePassengerListViewDiv").height()+200 }, 100);
}

function bindPassengerListButtonEvent()
{
	$("#SeatScorePassengerListViewDiv").find("button").click(function() {
		$("#SeatScorePassengerListViewDiv").find("button").each(function(element) {
			$(this).removeClass("btn-primary");
			$(this).addClass("btn-default");
		});
		$(this).removeClass("btn-default");
		$(this).addClass("btn-primary");
		var travelerId = $(this).attr("data-travelerid");
		var recordLocatorId = $(this).attr("data-recordlocatorid");
		bindSeatScoreViewData(SengEvgMsgData,travelerId,recordLocatorId);
	});
}

function PopulateSeatScorePassengerListView(data)
{
	var html = '<table class="table table-bordered">'
        html += '<thead>';
		html += '<tr class="row">';
		html += '<th class="col-md-2">PNR</th>';
		html += '<th class="col-md-4" style="padding:0px"><table class="table table-bordered" style="margin-bottom:0px"><thead><tr><th class="col-md-6">Traveler Id</th><th class="col-md-6">Preference</th></tr></thead></table></td>';
		html += '</tr>';
		html += '</thead>';
		html += '<tbody>';

	var requestObj = JSON.parse(data.requestClob);
    var offers = requestObj.flightSegments[0].offers;

	offers.forEach(function(element) {
		var offerItem = element;
		if(selectedRecordLocatorId == null)
			selectedRecordLocatorId = element.recordLocatorId;
		var recordLocatorId = element.recordLocatorId;
		var passengers = offerItem.passengers;
	    html += '<tr class="row">';
	    html += '<td class="col-md-2">'+recordLocatorId+'</td>';
		html += '<td class="col-md-4"  style="padding:0px !important">';
		html += '<table class="table table-bordered" style="margin-bottom:0px!important">'
		passengers.forEach(function(element){
			 if(selectedTravelerId == null)
			 {
				 selectedTravelerId = element.travelerId;
			 }

			  html += '<tr class="row"></tr>';
              html += '<td class="col-md-6 travel-td"><button class="btn btn-default travel-btn" data-recordlocatorid="'+recordLocatorId +'" data-travelerid="'+element.travelerId +'" id="list_traveler_'+recordLocatorId+"_"+element.travelerId+'">'+element.travelerId+'</button></td>';
              var passengerPreference = element.loyaltyMemberPreferences[0];
	  			var preferenceCode = "Generic"
	  			if(passengerPreference.preferenceCode != undefined && passengerPreference.preferenceCode.length > 0)
	  			{
	  				if(passengerPreference.preferenceCode == "W" || passengerPreference.preferenceCode == "w")
	  					preferenceCode = "Window";
	  				else if(passengerPreference.preferenceCode == "A" || passengerPreference.preferenceCode == "a")
	  					preferenceCode = "Aisle";

	  			}
              html += '<td class="col-md-6 travel-td">'+preferenceCode+'</td>';
              html += '</tr>'
		 });
		html += '</table>';
	});
	html += '</tbody>';
	html += '</table>';
	$("#SeatScorePassengerListViewDiv").html(html);
	$("#SeatScorePassengerListViewDiv").show();

}


function populateEvtTransactionList(data)
{

	var size = data != undefined ? data.length: 0;
	var html = "";
	if(size == 0)
		html = "No data found based on the search criteria!";
	else
	{
		html += '<div class="row box scg-patient-list-bg">';
		html += '<div class="col-md-5">Transaction Id</div>';
		html += '<div class="col-md-2">Flight#</div>';
		html += '<div class="col-md-3">Departure Date</div>';
		html += '<div class="col-md-2">Total Pnrs</div>';
		html += '</div>';
	}

	for(var i = 0; i<size;i++)
	{
		html += '<div class="row box scg-patient-list-bg" onclick="getFlightData(\''+data[i].transactionId+'\',\''+data[i].operatingFlightNum+'\',\''+data[i].scheduledDepartureLocalDate+'\',\''+data[i].totalPnrs+'\',\''+data[i].offerId+'\')">';
		html += '<div class="col-md-5">'+data[i].transactionId+'</div>';
		html += '<div class="col-md-2">'+data[i].operatingFlightNum+'</div>';
		html += '<div class="col-md-3">'+data[i].scheduledDepartureLocalDate+'</div>';
		html += '<div class="col-md-2">'+data[i].totalPnrs+'</div>';
		html += '</div>';
	}
	$("#evtTransactionListDiv").html(html);
	$('#transactionModal').modal({
	    backdrop: 'static',
	    keyboard: false,
	    show : true
	})
}

function populateTransactionInfo(transactionId, flightNo, departureDate, totalPnr,offerId)
{
	$("#transactionInfoDiv").html("<b>Transaction Id: "+transactionId+"&nbsp&nbsp&nbspDeparture Date: "+departureDate+"&nbsp&nbsp&nbsp<br/>Offer Id: "+offerId+"&nbsp&nbsp&nbspFlight#: "+flightNo+"&nbsp&nbsp&nbspTotal Pnrs: "+totalPnr+"</b>")
}

function populateSeatView(data)
{
	var obj = JSON.parse(data.requestClob);
    var compartments = obj.flightSegments[0].seatMap.compartments;
    var html="";
    var customClass = "";
    for(var i=0;i<compartments.length;i++)
    {
    	var seatRows = compartments[i].seatRows;

    	var seatConfigurationCode =compartments[i].seatConfigurationCode;
    	var islePosition = seatConfigurationCode.indexOf("|");

    	html += '<div class="row"><div class="col-md-1 custom-col-md-1"></div>';
    	for(m=0;m<seatConfigurationCode.length;m++){
    		if(seatConfigurationCode[m] == "|")
    			html+='<div class="col-md-1 custom-col-md-1 default-col"></div>';
    	    else
    	    	html+='<div class="col-md-1 custom-col-md-1 default-col text-center">'+seatConfigurationCode[m]+'</div>';
    	}
    	html += "</div>";

    	for(var j=0;j<seatRows.length;j++)
        {
    		html+='<div class="row"><div class="col-md-1 custom-col-md-1 text-right">'+seatRows[j].cabinRowSequenceNum+'</div>';
    		var seats = seatRows[j].seats;

    		var indexNum =0;
    		for(var l=0;l<seats.length;l++)
    	    {
    			if(compartments[i].compartmentCode=="W")
    	    		customClass = "comfort-seat";
    	    	else
    	    		customClass = "";

    			if(seats[l].exitRowSeat != null && seats[l].exitRowSeat == true)
    				customClass +=' exit-row-seat';
    			else if(seats[l].bulkhead != null && seats[l].bulkhead == true)
    				customClass +=' bulkhead-seat';
    			else if(seats[l].blocked != null && seats[l].blocked == true)
    				customClass +=' blocked-seat';
    			else if(seats[l].infant != null && seats[l].infant == true)
    				customClass +=' infant-restricted-seat';
    			else if(seats[l].limitedRecline != null && seats[l].limitedRecline == true)
    				customClass +=' limited-recline-seat';

    			//unavailable seat
    			if(seats[l].seatAvailableCode != null && seats[l].seatAvailableCode == 'false')
    				 customClass +=' unavailable-seat';

    			var gapSize = seats[l].indexNum - indexNum;
    			for(var k=0;k<gapSize;k++)
    			{
    				html+='<div class="col-md-1 custom-col-md-1 default-col"></div>';
    				indexNum++;
    			}
    			html+='<div id="'+seats[l].seatId+'" class="well col-md-1 custom-col-md-1 text-center custom-well '+customClass+'"></div>';
    			indexNum++;

    	    }
    		html+='</div>';
        }
    	html+='<div class="row" style="margin-top:40px"></div>'
    }
    $("#flightInfoDiv").html(html);
}

function populateSeatScoreSeatView(data)
{
	var obj = JSON.parse(data.requestClob);
    var compartments = obj.flightSegments[0].seatMap.compartments;
    var html="";
    var customClass = "";
    for(var i=0;i<compartments.length;i++)
    {
    	var seatRows = compartments[i].seatRows;

    	var seatConfigurationCode =compartments[i].seatConfigurationCode;
    	var islePosition = seatConfigurationCode.indexOf("|");

    	html += '<div class="row"><div class="col-md-1 custom-col-md-1"></div>';
    	for(m=0;m<seatConfigurationCode.length;m++){
    		if(seatConfigurationCode[m] == "|")
    			html+='<div class="col-md-1 custom-col-md-1"></div>';
    	    else
    	    	html+='<div class="col-md-1 custom-col-md-1 text-center">'+seatConfigurationCode[m]+'</div>';
    	}
    	html += "</div>";

    	for(var j=0;j<seatRows.length;j++)
        {
    		html+='<div class="row"><div class="col-md-1 custom-col-md-1 text-right seat-score-seq-no">'+seatRows[j].cabinRowSequenceNum+'</div>';
    		var seats = seatRows[j].seats;

    		var indexNum =0;
    		for(var l=0;l<seats.length;l++)
    	    {
    			if(compartments[i].compartmentCode=="W")
    	    		customClass = "comfort-seat";
    	    	else
    	    		customClass = "";

    			if(seats[l].exitRowSeat != null && seats[l].exitRowSeat == true)
    				customClass +=' exit-row-seat';
    			else if(seats[l].bulkhead != null && seats[l].bulkhead == true)
    				customClass +=' bulkhead-seat';
    			else if(seats[l].blocked != null && seats[l].blocked == true)
    				customClass +=' blocked-seat';
    			else if(seats[l].infant != null && seats[l].infant == true)
    				customClass +=' infant-restricted-seat';
    			else if(seats[l].limitedRecline != null && seats[l].limitedRecline == true)
    				customClass +=' limited-recline-seat';

    			//unavailable seat
    			if(seats[l].seatAvailableCode != null && seats[l].seatAvailableCode == 'false')
   				 	customClass +=' unavailable-seat';

    			var gapSize = seats[l].indexNum - indexNum;
    			for(var k=0;k<gapSize;k++)
    			{
    				html+='<div class="col-md-1 custom-col-md-1"></div>';
    				indexNum++;
    			}
    			html+='<div class="col-md-1 custom-col-md-1 text-center seat-score-well"><div class="seat-score-amt-span" id="amount-'+seats[l].seatId+'"></div><div id="'+seats[l].seatId+'" class="well col-md-11 text-center custom-well '+customClass+'"></div></div>';
    			indexNum++;

    	    }
    		html+='</div>';
        }
    	html+='<div class="row" style="margin-top:40px"></div>'
    }
    $("#flightInfoDiv").html(html);
}

function populateOfferedSeat(data, travelerId,selectedRecordLocatorId)
{
	var requestObj = JSON.parse(data.requestClob);
    var offers = requestObj.flightSegments[0].offers;
    var alreadyPopulated = false;
	offers.forEach(function(element) {
		var offerItem = element;
	    var recordLocatorId = element.recordLocatorId;
		var passengers = offerItem.passengers;

		if((selectedRecordLocatorId == undefined || selectedRecordLocatorId == null
		   || recordLocatorId == selectedRecordLocatorId) && alreadyPopulated == false)
        {
			if(recordLocatorId == selectedRecordLocatorId)
				alreadyPopulated = true;

			passengers.forEach(function(element){

				 if(travelerId == undefined || travelerId == null || travelerId == element.travelerId)
				 {
					  var seatOffers = element.seatOffers;
					  seatOffers.forEach(function(element){
						  if($("#"+element.seatId).hasClass('comfort-seat'))
						  {
							  $("#"+element.seatId).removeClass("confort-seat");
							  $("#"+element.seatId).addClass("confort-offered-seat");
						  }
						  else
							  $("#"+element.seatId).addClass("offered-seat");

						  if(travelerId != undefined || travelerId != null)
						  {
							  $("#"+element.seatId).append('<br\><span style="color:#FF0000"><b>'+element.seatScoreNum+'</b></span>');
							  $("#amount-"+element.seatId).html("<b>$"+element.paymentAmt+"</b>");
						  }
					  });
				 }

			 });
        }

	});
}

function populatePassengerSeating(data,travelerId)
{
	var responseObj = JSON.parse(data.responseClob);
	var passengerSeats = null;
	if(responseObj.flightSegmentOutputs != undefined && responseObj.flightSegmentOutputs != null)
		passengerSeats =responseObj.flightSegmentOutputs[0].seatSelections;

	if(passengerSeats == undefined || passengerSeats == null)
		return;

	passengerSeats.forEach(function(element) {
		  var seatItem = element;
		  if(travelerId == undefined || travelerId == null || travelerId == seatItem.travelerId)
		  {
			  if(seatItem.recordLocatorId != null)
				  $("#"+seatItem.selectedSeatId).html("<b>"+seatItem.recordLocatorId+"</b>");
			  else
				  $("#"+seatItem.selectedSeatId).html(seatItem.travelerId);
		  }

		  if(travelerId == undefined || travelerId == null)
		  {
			  $("#"+seatItem.selectedSeatId).attr('data-travelerid', seatItem.travelerId+"-"+seatItem.recordLocatorId);
			  $("#"+seatItem.selectedSeatId).attr('data-toggle',"tooltip");
			  $("#"+seatItem.selectedSeatId).attr('data-html',true);
			  var html = "<b>Traveler Id: "+seatItem.travelerId+"</b>";
			      html += "<br\><b>First Name: "+seatItem.firstNameNum+"</b>";
			      html += "<br\><b>Last Name: "+seatItem.lastNameNum+"</b>";
			  $("#"+seatItem.selectedSeatId).attr('title',html);
		  }
	 });
	$('[data-toggle="tooltip"]').tooltip();
}


function showOrHidePassengerPreference(data,isShow)
{
	var requestObj = JSON.parse(data.requestClob);
	 var offers = requestObj.flightSegments[0].offers;
	 offers.forEach(function(element){
		var passengers = element.passengers;
		var recordLocatorId = element.recordLocatorId;
		passengers.forEach(function(element){

			var passengerPreference = element.loyaltyMemberPreferences[0];
			var preferenceCode = "Generic"
			if(passengerPreference.preferenceCode != undefined && passengerPreference.preferenceCode.length > 0)
			{
				if(passengerPreference.preferenceCode == "W" || passengerPreference.preferenceCode == "w")
					preferenceCode = "Window";
				else if(passengerPreference.preferenceCode == "A" || passengerPreference.preferenceCode == "a")
					preferenceCode = "Aisle";

			}
			var passengerSeat = $("[data-travelerid="+element.travelerId+"-"+recordLocatorId+"]");
			var recordLocatorText = recordLocatorId;
			if(recordLocatorId == null)
				recordLocatorText = element.travelerId;

			if(isShow)
			{
				var text = $(passengerSeat).html();
				var index = text != undefined ? text.indexOf("<br>") : -1;
				if(index != -1)
					passengerSeat.html("<b>"+recordLocatorText+"</b>");
				passengerSeat.append('<br><span style="color:#FF0000"><b>'+preferenceCode +'</b></span>');
			}
			else
			{
				var text = $(passengerSeat).html();
				var index = text != undefined ? text.indexOf("<br>") : -1;
				if(index != -1)
					passengerSeat.html("<b>"+recordLocatorText+"</b>");
			}
		 });
	 });
}

function showOrHideSeatScore(data,isSeatScore)
{
	var requestObj = JSON.parse(data.requestClob);
	 var offers = requestObj.flightSegments[0].offers;
	 offers.forEach(function(element){
		var passengers = element.passengers;
		passengers.forEach(function(element){
			var seatOffers = element.seatOffers;
			seatOffers.forEach(function(element){
				if(isSeatScore)
				{
					var text = $("#"+element.seatId).html();
					var index = text != undefined ? text.indexOf("<br>") : -1;
					if(index != -1)
						$("#"+element.seatId).html(text.substring(0,index));
					$("#"+element.seatId).append('<br><span style="color:#FF0000"><b>'+element.seatScoreNum +'</b></span>');
				}
				else
				{
					var text = $("#"+element.seatId).html();
					var index = text != undefined ? text.indexOf("<br>") : -1;
					if(index != -1)
						$("#"+element.seatId).html(text.substring(0,index));
				}
			 });
		 });
	 });
}

function populateAvailabelInventoryCount(data)
{
	var obj = JSON.parse(data.requestClob);
    var compartments = obj.flightSegments[0].seatMap.compartments;
    var size = compartments != undefined ? compartments.length : 0;
    if(size == 0)
    	return;
    var html = '<table class="table table-bordered">';
    	html += '<thead><tr><th>Compartment Code</th><th>Available Inventory</th></tr></thead>';
    	html += '<tbody>';
    for(var i=0;i<compartments.length;i++)
    {
    	html += '<tr><td>'+compartments[i].compartmentCode+'</td><td>'+((compartments[i].avilableInventoryCnt == undefined || compartments[i].avilableInventoryCnt == null) ? 0 : compartments[i].avilableInventoryCnt )+'</td></tr>';
    }
    html += '</tbody></table>';
    $("#inventoryConuntTableDiv").html(html);
}
function bindVisualizerEvent()
{
	$('#seatPassengerPreferenceCheckBox').change(function () {

		if(SengEvgMsgData == undefined || SengEvgMsgData == null)
			return;

	    if($(this).prop('checked'))
	    {
	    	showOrHidePassengerPreference(SengEvgMsgData,true);
	    }
	    else
	    {
	    	showOrHidePassengerPreference(SengEvgMsgData,false);
	    }
	});

	$( "#scheduleDepartureFromDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#scheduleDepartureToDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
	
	//seat analytics date picker bind
	$( "#seatAnalyticsDepartureFromDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#seatAnalyticsDepartureToDate" ).datepicker({ dateFormat: 'yy-mm-dd' });

	$( "#seat_assignment_btn").click(function() {

		$("#seat_score_btn").addClass("btn-default");
		$("#seat_score_btn").removeClass("btn-primary");
		$("#seat_assignment_btn").addClass("btn-primary");
		$("#seat_assignment_btn").removeClass("btn-default");
		$("#analytics_btn").removeClass("btn-primary");
		$("#analytics_btn").addClass("btn-default");
		$("#airline_seat_view").show();
		$("#airline_analytics_view").hide();


		if(SengEvgMsgData == undefined || SengEvgMsgData == null)
			return;

		populateSeatAssignmentView(SengEvgMsgData);
	});

	$( "#seat_score_btn").click(function() {
		$("#seat_score_btn").removeClass("btn-default");
		$("#seat_score_btn").addClass("btn-primary");
		$("#seat_assignment_btn").removeClass("btn-primary");
		$("#seat_assignment_btn").addClass("btn-default");
		$("#analytics_btn").removeClass("btn-primary");
		$("#analytics_btn").addClass("btn-default");
		$("#airline_seat_view").show();
		$("#airline_analytics_view").hide();

		if(SengEvgMsgData == undefined || SengEvgMsgData == null)
			return;

		populateSeatScoretView(SengEvgMsgData);
	});
	$( "#analytics_btn").click(function() {
		$("#seat_score_btn").removeClass("btn-primary");
		$("#seat_score_btn").addClass("btn-default");
		$("#seat_assignment_btn").removeClass("btn-primary");
		$("#seat_assignment_btn").addClass("btn-default");
		$("#analytics_btn").removeClass("btn-default");
		$("#analytics_btn").addClass("btn-primary");
		$("#airline_seat_view").hide();
		$("#airline_analytics_view").show();

	
	});
}