<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.delta.seat.api.util.AppConstants,java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Delta Airlines</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="static/css/bootstrap.min.css">
<link rel="stylesheet" href="static/css/jquery-ui.min.css">
<link rel="stylesheet" href="static/css/style.css">

<script src="static/js/jquery-3.3.1.min.js"></script>
<script src="static/js/jquery-ui.min.js"></script>
<script src="static/js/jquery.validate.min.js"></script>
<script src="static/js/jquery.blockUI.js"></script>
<script src="static/js/bootstrap-3.3.7.min.js"></script>
<script src="static/js/jszip.js"></script>
<script src="static/js/app/app-visualize-script.js"></script>



<style>
</style>

<script type="text/javascript">
var base_url = "<%=request.getContextPath().toString()%>";

function uploadFiles() {
	var fd = new FormData();
	fd.append( "newFile", $("#newFile")[0].files[0]);
	fd.append( "oldFile", $("#oldFile")[0].files[0]);
	$.blockUI({ message: '<img src="static/images/ajax-loader.gif" />' });
	 $.ajax({
            type: "POST",
            url: base_url+"/fileUpload",
            data: fd,
            contentType: false,
            processData: false,
            enctype : "multipart/form-data",
            cache: false,
            /*beforeSend: function(xhr, settings) {
                xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
                settings.data = {name: "file", file: $("input[name=file]")[0].files[0]};                    
            },*/
            success: function (result) {
            	$.unblockUI();
            	console.log(result);
            	console.log(result == "SUCCESS");
                if ( result == "SUCCESS" ) {
                	
                    var html = '<div class="alert alert-success alert-dismissible" role="alert">'
                             + 'File uploaded and generated flight leg table successfully'
                    		 + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                      		 + '<span aria-hidden="true">&times;</span>'
                    		 + '</button>'
                     		 + '</div>';
                     $("#successDiv").html(html);
                } else {
                	var html = '<div class="alert alert-danger alert-dismissible" role="alert">'
                        + 'File uploaded and generated flight leg table failed'
               		 + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                 		 + '<span aria-hidden="true">&times;</span>'
               		 + '</button>'
                		 + '</div>';
                         $("#successDiv").html(html);
                }
            },
            error: function (result) {
            	$.unblockUI();
                console.log(result);
            }
        });
}

</script>
</head>
<body>
	<div class="container" style="width: 1230px !important;padding-top:40px">
		<div id="airline_seat_view" class="panel panel-primary">
		    <div class="panel-heading">SKD Change</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8 panel" style="padding-top: 15px">
					  <form class="form-horizontal" method="POST" action="//uploadMultiFile" enctype="multipart/form-data">
						   <div class="form-group">
								<label class="control-label col-md-3" for="oldFile">Select Old File:</label>
								<div class="col-md-6">
									<input type="file" name="oldFile" id="oldFile"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newFile">Select New File:</label>
								<div class="col-md-6">
									<input type="file" name="newFile" id="newFile"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<input type="button" class="btn btn-primary" onclick="uploadFiles()" value="Submit" />
								</div>
							</div>
						</form>
					</div>

				</div>
				<div id="successDiv">
				</div>
			</div>
		</div>
	</div>

</body>
</html>

