package com.delta.seat.api.service;

import com.delta.seat.util.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.delta.seat.api.vo.*;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static com.delta.seat.constants.SeatEngineConstants.NO_SEAT_SELECTION_MSG;

public class SeatServiceApiTest {

    private SeatSelectionInput seatSelectionRequest;
    private SeatApiServiceImpl seatApiService;
    private static final String TRANSACTION_ID = "123456";
    private SeatServiceDelegator seatServiceDelegator;

    @Before
    public void before() throws Exception {
        seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator = new SeatServiceDelegatorImpl();
        seatApiService.setSeatServiceDelegator(seatServiceDelegator);
    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testPreferredAislePreferrence(){

        seatSelectionRequest = new SeatSelectionInput();
        seatSelectionRequest = createSeatSelectionInputForPreferredAislePreference();
        SeatSelectionOutput seatSelectionResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatSelectionResponse);
        assertEquals(1,seatSelectionResponse.getFlightSegmentOutputs().size());
        assertEquals("8A", seatSelectionResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());

        assertEquals("SEA",seatSelectionResponse.getFlightSegmentOutputs().get(0).getDestinationAirportCode());
        assertEquals("01",seatSelectionResponse.getFlightSegmentOutputs().get(0).getAssociationId());
        assertEquals("M",seatSelectionResponse.getFlightSegmentOutputs().get(0).getClassOfServiceCode());
        assertEquals("LHR",seatSelectionResponse.getFlightSegmentOutputs().get(0).getOriginAirportCode());
        assertEquals("DL",seatSelectionResponse.getFlightSegmentOutputs().get(0).getOperatingCarrierCode());
        assertEquals("2018-03-06",seatSelectionResponse.getFlightSegmentOutputs().get(0).getScheduledDepartureLocalDate());
        assertEquals("true",seatSelectionResponse.getFlightSegmentOutputs().get(0).getInboundFlightCode());
        assertEquals("Delta",seatSelectionResponse.getFlightSegmentOutputs().get(0).getMarketedAsConnectionCarrierFlightDetailsText());
        assertEquals("08.04",seatSelectionResponse.getFlightSegmentOutputs().get(0).getFlightDepartureTime());
        assertEquals("DL",seatSelectionResponse.getFlightSegmentOutputs().get(0).getMarketingCarrierCode());
        assertEquals("W",seatSelectionResponse.getFlightSegmentOutputs().get(0).getOperatingClassOfServiceCode());
        assertEquals("DL",seatSelectionResponse.getFlightSegmentOutputs().get(0).getMarketingCarrierCode());
        assertEquals("105",seatSelectionResponse.getFlightSegmentOutputs().get(0).getMarketingFlightNum());
        assertEquals("DL",seatSelectionResponse.getFlightSegmentOutputs().get(0).getOperatingCarrierCode());
        assertEquals("105",seatSelectionResponse.getFlightSegmentOutputs().get(0).getOperatingFlightNum());
        assertEquals("01",seatSelectionResponse.getFlightSegmentOutputs().get(0).getFlightSegmentNum());
        assertEquals("359",seatSelectionResponse.getFlightSegmentOutputs().get(0).getEquipmentCode());

//        System.out.println(String.format("seatSelectionRequest json [%s] ", StringUtils.getJsonString(seatSelectionRequest)));
//        System.out.println(String.format("seatRecommedationResponse json [%s] ", StringUtils.getJsonString(seatSelectionResponse)));
    }

    @Test
    public void testPreferredAislePreferrence_PriceAmtNull(){

        seatSelectionRequest = new SeatSelectionInput();
        seatSelectionRequest = createSeatSelectionInputForPreferredAislePreferencePriceAmtNull();
        SeatSelectionOutput seatSelectionResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatSelectionResponse);
        assertEquals(1,seatSelectionResponse.getFlightSegmentOutputs().size());
        assertNotEquals("8A", seatSelectionResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
    }

    @Test
    public void testPreferredAislePreferenceNoAisleFound_PreferredWindowFound(){

        seatSelectionRequest = createSeatSelectionInputPreferredAislePreferenceNoAisleFound_PreferredWindowFound();
        SeatSelectionOutput seatSelectionResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatSelectionResponse);
        assertEquals(1,seatSelectionResponse.getFlightSegmentOutputs().size());
        assertEquals("7J", seatSelectionResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
    }

    @Test
    public void testPreferredAislePreferenceNoAisleNoWindowFound_MiddleFound(){

        seatSelectionRequest = createSeatSelectionInputPreferredAislePreferenceNoAisleNoWindowFound_MiddleFound();
        SeatSelectionOutput seatSelectionResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatSelectionResponse);
        assertEquals(1,seatSelectionResponse.getFlightSegmentOutputs().size());
        assertEquals("8G", seatSelectionResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
    }

    @Test
    public void testPreferredWindowPreference() {
        seatSelectionRequest = createSeatSelectionInputForPreferredWindowPreference();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("8J", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }
    @Test
    public void testPreferredWindowPreferenceNoWindowFound_AisleFound() {
        seatSelectionRequest = createSeatSelectionInputForPreferredWindowPreferenceNoWindowFound_AisleFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("8A", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }
    @Test
    public void testPreferredWindowPreferenceNoWindowNoAisleFound_MiddleFound() {
        seatSelectionRequest = createSeatSelectionInputForPreferredWindowPreferenceNoWindowNoAisleFound_MiddleFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("23A", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }

    @Test
    public void testNoPreference_PreferredAisleFound() {
        seatSelectionRequest = createSeatSelectionInputForNoPreference_PreferredAisleFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("7C", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }

    @Test
    public void testNoPreference_NonPreferredAisleFound() {
        seatSelectionRequest = createSeatSelectionInputForNoPreference_NonPreferredAisleFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("8A", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }

    @Test
    public void testNoPreference_PreferredWindwFound() {
        seatSelectionRequest = createSeatSelectionInputForNoPreference_PreferredWindowFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("23A", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());

    }

    @Test
    public void testNoPreference_NonPreferredWindwFound() {
        seatSelectionRequest = createSeatSelectionInputForNoPreference_NonPreferredWindowFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("24C", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }

    @Test
    public void testNoPreference_PreferredMiddleFound() {
        seatSelectionRequest = createSeatSelectionInputForNoPreference_PreferredMiddleFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("23C", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
//        System.out.println(String.format("seatSelectionRequest json [%s] ", StringUtils.getJsonString(seatSelectionRequest)));
//        System.out.println(String.format("seatRecommedationResponse json [%s] ", StringUtils.getJsonString(seatRecommedationResponse)));
    }

    @Test
    public void testNoPreference_NonPreferredMiddleFound() {
        seatSelectionRequest = createSeatSelectionInputForNoPreference_NonPreferredMiddleFound();
        SeatSelectionOutput seatRecommedationResponse = seatApiService.getSeatSelection(seatSelectionRequest, TRANSACTION_ID);
        assertNotNull(seatRecommedationResponse);
        assertEquals(1,seatRecommedationResponse.getFlightSegmentOutputs().size());
        assertEquals("24B", seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId());
        //       System.out.println(String.format("seatRecommendationRequest json [%s] ", StringUtils.getJsonString(seatRecommendationRequest)));
    }

    private SeatSelectionInput createSeatSelectionInputPreferredAislePreferenceNoAisleNoWindowFound_MiddleFound() {

        SeatSelectionInput seatSelectionRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = new FlightSegmentInput();
        flightInfoRequest.setDestinationAirportCode("SEA");
        flightInfoRequest.setAssociationId("01");
        flightInfoRequest.setClassOfServiceCode("M");
        flightInfoRequest.setOriginAirportCode("LHR");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setScheduledDepartureLocalDate("2018-03-06");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setMarketingFlightNum("105");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setOperatingFlightNum("105");
        flightInfoRequest.setFlightSegmentNum("01");
        flightInfoRequest.setFlightIdRefText("01");
        flightInfoRequestList.add(flightInfoRequest);


        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = new Offer();
        offer.setOfferId("43b0d83d-d0ae-424b-b043-3098c3e89a23_FA");
        offer.setRecordLocatorId("PDXHTR");
        offer.setOrderNum("01");
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = new Passenger();
        passenger.setFirstNameNum("01");
        passenger.setLastNameNum("01");
        passenger.setTravelerId("01-01");

        passenger.setPaxPriorityScoreNum("12345");
        passenger.setCurrencyCode("USD");
        passenger.setOnBoard(false);
        passenger.setBoardingPass(true);
        passenger.setCheckedIn(false);
        passenger.setConnection(false);
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        Seatoffer seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("23A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("24A");
        seatoffer.setOfferItemNum("1");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("7A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("8A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("7J");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("8G");
        seatOfferList.add(seatoffer);


        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("A");
        loyaltyPreference.setPreferenceCodeDesc("Aisle");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);


        seatSelectionRequest.setFlightSegments(flightInfoRequestList);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;
        String rowNumber = "0";

        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("false");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setWindow(false);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("false");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(false);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("false");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("false");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);


        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(false);
        seatInfoList.add(seatInfo);


        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        row = new SeatRow();
        rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();


        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "D");
        seatInfo.setAisle(false);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);
        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);


        return seatSelectionRequest;
    }

    private SeatSelectionInput createSeatSelectionInputPreferredAislePreferenceNoAisleFound_PreferredWindowFound() {

        SeatSelectionInput SeatSelectionInput = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = new FlightSegmentInput();
        flightInfoRequest.setDestinationAirportCode("SEA");
        flightInfoRequest.setAssociationId("01");
        flightInfoRequest.setClassOfServiceCode("M");
        flightInfoRequest.setOriginAirportCode("LHR");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setScheduledDepartureLocalDate("2018-03-06");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setMarketingFlightNum("105");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setOperatingFlightNum("105");
        flightInfoRequest.setFlightSegmentNum("01");
        flightInfoRequest.setFlightIdRefText("01");
        flightInfoRequestList.add(flightInfoRequest);


        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = new Offer();
        offer.setOfferId("43b0d83d-d0ae-424b-b043-3098c3e89a23_FA");
        offer.setRecordLocatorId("PDXHTR");
        offer.setOrderNum("01");
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = new Passenger();
        passenger.setFirstNameNum("01");
        passenger.setLastNameNum("01");
        passenger.setTravelerId("01-01");

        passenger.setPaxPriorityScoreNum("12345");
        passenger.setCurrencyCode("USD");
        passenger.setOnBoard(false);
        passenger.setBoardingPass(true);
        passenger.setCheckedIn(false);
        passenger.setConnection(false);
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        Seatoffer seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("23A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("24A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("7A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("8A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("7J");
        seatOfferList.add(seatoffer);

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("A");
        loyaltyPreference.setPreferenceCodeDesc("Aisle");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);


        SeatSelectionInput.setFlightSegments(flightInfoRequestList);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;
        String rowNumber = "0";

        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);


        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);


        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        row = new SeatRow();
        rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();


        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "D");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);
        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);


        return SeatSelectionInput;
    }

    private SeatSelectionInput createSeatSelectionInputForPreferredAislePreference() {

        SeatSelectionInput SeatSelectionInput = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = new FlightSegmentInput();

        flightInfoRequest.setDestinationAirportCode("SEA");
        flightInfoRequest.setAssociationId("01");
        flightInfoRequest.setClassOfServiceCode("M");
        flightInfoRequest.setOriginAirportCode("LHR");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setScheduledDepartureLocalDate("2018-03-06");
        flightInfoRequest.setInboundFlightCode("true");
        flightInfoRequest.setMarketedAsConnectionCarrierFlightDetailsText("Delta");
        flightInfoRequest.setFlightDepartureTime("08.04");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setOperatingClassOfServiceCode("W");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setMarketingFlightNum("105");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setOperatingFlightNum("105");
        flightInfoRequest.setFlightSegmentNum("01");
        flightInfoRequest.setFlightIdRefText("01");
        flightInfoRequest.setEquipmentCode("359");

        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        cabin.setAvilableInventoryCnt("3");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();


        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "D");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();
        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "D");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        cabin.setAvilableInventoryCnt("4");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);



        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = new Offer();
        offer.setOfferId("43b0d83d-d0ae-424b-b043-3098c3e89a23_FA");
        offer.setRecordLocatorId("PDXHTR");
        offer.setOrderNum("01");
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = new Passenger();
        passenger.setFirstNameNum("01");
        passenger.setLastNameNum("01");
        passenger.setTravelerId("01-01");
        passenger.setPaxPriorityScoreNum("12345");
        passenger.setCurrencyCode("USD");
        passenger.setOnBoard(false);
        passenger.setBoardingPass(true);
        passenger.setCheckedIn(false);
        passenger.setConnection(false);
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        Seatoffer seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("23A");
        seatoffer.setOfferItemNum("1");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("24A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("7A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("8A");
        seatOfferList.add(seatoffer);

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("A");
        loyaltyPreference.setPreferenceCodeDesc("Aisle");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);


        SeatSelectionInput.setFlightSegments(flightInfoRequestList);

        return SeatSelectionInput;
    }

    private SeatSelectionInput createSeatSelectionInputForPreferredAislePreferencePriceAmtNull() {

        SeatSelectionInput SeatSelectionInput = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = new FlightSegmentInput();
        flightInfoRequest.setDestinationAirportCode("SEA");
        flightInfoRequest.setAssociationId("01");
        flightInfoRequest.setClassOfServiceCode("M");
        flightInfoRequest.setOriginAirportCode("LHR");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setScheduledDepartureLocalDate("2018-03-06");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setMarketingFlightNum("105");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setOperatingFlightNum("105");
        flightInfoRequest.setFlightSegmentNum("01");
        flightInfoRequest.setFlightIdRefText("01");
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();


        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "D");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();
        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "D");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "B");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("false");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);

        seatInfoList = new ArrayList<Seat>();

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "A");
        seatInfo.setAisle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "G");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "C");
        seatInfo.setMiddle(true);
        seatInfoList.add(seatInfo);

        seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode("true");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText("true");
        seatInfo.setSeat(true);
        seatInfo.setSeatId(rowNumber + "J");
        seatInfo.setWindow(true);
        seatInfoList.add(seatInfo);



        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = new Offer();
        offer.setOfferId("43b0d83d-d0ae-424b-b043-3098c3e89a23_FA");
        offer.setRecordLocatorId("PDXHTR");
        offer.setOrderNum("01");
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = new Passenger();
        passenger.setFirstNameNum("01");
        passenger.setLastNameNum("01");
        passenger.setTravelerId("01-01");
        passenger.setPaxPriorityScoreNum("12345");
        passenger.setCurrencyCode("USD");
        passenger.setOnBoard(false);
        passenger.setBoardingPass(true);
        passenger.setCheckedIn(false);
        passenger.setConnection(false);
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        Seatoffer seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("23A");
        seatoffer.setOfferItemNum("1");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("24A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId("7A");
        seatOfferList.add(seatoffer);

        seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt(null);
        seatoffer.setSeatId("8A");
        seatOfferList.add(seatoffer);

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("A");
        loyaltyPreference.setPreferenceCodeDesc("Aisle");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);


        SeatSelectionInput.setFlightSegments(flightInfoRequestList);

        return SeatSelectionInput;
    }

    private SeatSelectionInput createSeatSelectionInputForPreferredWindowPreference() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("W");
        loyaltyPreference.setPreferenceCodeDesc("Window");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForPreferredWindowPreferenceNoWindowFound_AisleFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("W");
        loyaltyPreference.setPreferenceCodeDesc("Window");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForPreferredWindowPreferenceNoWindowNoAisleFound_MiddleFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, true, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("7C"));
        seatOfferList.add(createSeatOffer("8J"));
        seatOfferList.add(createSeatOffer("8G"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreference.setPreferenceCode("W");
        loyaltyPreference.setPreferenceCodeDesc("Window");
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForNoPreference_PreferredAisleFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();
        String rowNumber = "";

//        row = new SeatRow();
//        Integer rowNumber = 24;
//        row.setCabinRowSequenceNum(rowNumber);
//        seatInfoList = new ArrayList<Seat>();
//        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        row.setSeats(seatInfoList);
//        rowList.add(row);
//
//
//        row = new SeatRow();
//        rowNumber = 23;
//        row.setCabinRowSequenceNum(rowNumber);
//        seatInfoList = new ArrayList<Seat>();
//        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        seatInfo = createSeat(rowNumber + "D", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        seatInfo = createSeat(rowNumber + "B", false, false, true, "true", true);
//        seatInfoList.add(seatInfo);
//        row.setSeats(seatInfoList);
//        rowList.add(row);
//
//        cabin.setSeatRows(rowList);
//        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", true, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7C"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForNoPreference_NonPreferredAisleFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, true, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", true, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForNoPreference_PreferredWindowFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", true, false, true, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, true, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForNoPreference_PreferredMiddleFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("23C"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForNoPreference_NonPreferredMiddleFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24B"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }


    private SeatSelectionInput createSeatSelectionInputForNoPreference_NonPreferredWindowFound() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", true, false, true, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, true, "false", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, true, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, true, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("23A"));
        seatOfferList.add(createSeatOffer("24A"));
        seatOfferList.add(createSeatOffer("7A"));
        seatOfferList.add(createSeatOffer("8A"));
        seatOfferList.add(createSeatOffer("7J"));
        seatOfferList.add(createSeatOffer("8J"));
        seatOfferList.add(createSeatOffer("24C"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private SeatSelectionInput createSeatSelectionInputForNoSeatSelection() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, TRANSACTION_ID);;

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("1A"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }



    private Offer createOffer() {
        Offer offer = new Offer();
        offer.setOfferId("43b0d83d-d0ae-424b-b043-3098c3e89a23_FA");
        offer.setRecordLocatorId("PDXHTR");
        offer.setOrderNum("01");
        return offer;
    }
    private Seat createSeat(String seatId, Boolean window, Boolean middle, Boolean aisle, String preferred, Boolean available) {
        Seat seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode(available?"true":"false");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText(preferred);
        seatInfo.setSeat(true);
        seatInfo.setSeatId(seatId);
        seatInfo.setWindow(window);
        seatInfo.setMiddle(middle);
        seatInfo.setAisle(aisle);
        return seatInfo;
    }

    private Seatoffer createSeatOffer(String seatId) {
        Seatoffer seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId(seatId);
        return seatoffer;
    }

    private FlightSegmentInput createFlightSegmentInput() {
        FlightSegmentInput flightInfoRequest = new FlightSegmentInput();
        flightInfoRequest.setDestinationAirportCode("SEA");
        flightInfoRequest.setAssociationId("01");
        flightInfoRequest.setClassOfServiceCode("M");
        flightInfoRequest.setOriginAirportCode("LHR");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setScheduledDepartureLocalDate("2018-03-06");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setMarketingFlightNum("105");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setOperatingFlightNum("105");
        flightInfoRequest.setFlightIdRefText("01");
        return flightInfoRequest;
    }

    private Passenger createPassenger() {
        Passenger passenger = new Passenger();
        passenger.setFirstNameNum("01");
        passenger.setLastNameNum("01");
        passenger.setTravelerId("01-01");
        passenger.setPaxPriorityScoreNum("12345");
        passenger.setCurrencyCode("USD");
        passenger.setOnBoard(false);
        passenger.setBoardingPass(true);
        passenger.setCheckedIn(false);
        passenger.setConnection(false);
        return passenger;
    }
}
