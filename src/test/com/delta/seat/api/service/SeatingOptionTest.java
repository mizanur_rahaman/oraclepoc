package com.delta.seat.api.service;

import com.delta.seat.api.selection.*;
import com.delta.seat.api.test.util.TestUtil;
import com.delta.seat.api.vo.FlightSegmentOutput;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * These are jvm parameters needed to run the class:
 * -Dseat.logdir=C:\logs\seat -Dseat.logfile=SEAT.log -Ddatadir=C:\work\seating\data
 */
public class SeatingOptionTest {
    private static String TRANSACTION_ID = "Test123456";
    private SeatingOption seatingOption;
    @Before
    public void before() throws Exception {
        seatingOption = new SeatingOptionImpl();
    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testCreateAdjacentOptions() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request28.txt");
        Request request = new Request(voReq, TRANSACTION_ID);
        AdjacentOption[] adjOptions = seatingOption.createAdjacentOptions(request.getFltLeg(), TRANSACTION_ID);
        assertNotNull(adjOptions);
        assertEquals(22, adjOptions.length);
        assertEquals("18C 18D 18E 11.0", getAdjString(adjOptions[0]));
        assertEquals("18A 18C 18D 9.0", getAdjString(adjOptions[1]));
        assertEquals("35B 35C 36A 8.0", getAdjString(adjOptions[2]));
        assertEquals("35B 35C 36B 8.0", getAdjString(adjOptions[3]));
        assertEquals("35B 36A 36B 8.0", getAdjString(adjOptions[4]));
        assertEquals("35C 36A 36B 8.0", getAdjString(adjOptions[5]));
        assertEquals("35E 36D 36E 8.0", getAdjString(adjOptions[6]));
        assertEquals("15B 15C DR 6.0", getAdjString(adjOptions[7]));
        assertEquals("18D 18E DR 6.0", getAdjString(adjOptions[8]));
        assertEquals("35B 35C DR 6.0", getAdjString(adjOptions[9]));
        assertEquals("36A 36B DR 6.0", getAdjString(adjOptions[10]));
        assertEquals("36D 36E DR 6.0", getAdjString(adjOptions[11]));
        assertEquals("18C 18D AA 5.0", getAdjString(adjOptions[12]));
        assertEquals("18A 18C SS 4.0", getAdjString(adjOptions[13]));
        assertEquals("24B 25B NR 1.0", getAdjString(adjOptions[14]));
        assertEquals("32B 33A NR 1.0", getAdjString(adjOptions[15]));
        assertEquals("35B 36A NR 1.0", getAdjString(adjOptions[16]));
        assertEquals("35B 36B NR 1.0", getAdjString(adjOptions[17]));
        assertEquals("35C 36A NR 1.0", getAdjString(adjOptions[18]));
        assertEquals("35C 36B NR 1.0", getAdjString(adjOptions[19]));
        assertEquals("35E 36D NR 1.0", getAdjString(adjOptions[20]));
        assertEquals("35E 36E NR 1.0", getAdjString(adjOptions[21]));
    }

    private String getAdjString(AdjacentOption adjOpt) {
        Seat[] seats = adjOpt.getSeats();
        String temp = null;
        if (adjOpt.size() == 3)
            temp = seats[0].getSeatId() + ' ' + seats[1].getSeatId() + ' ' + seats[2].getSeatId() + ' ' + String.format("%.1f", adjOpt.getScore());
        else if (adjOpt.size() == 2)
            temp = seats[0].getSeatId() + ' ' + seats[1].getSeatId() + ' ' + adjOpt.getAdjType12() + ' ' + String.format("%.1f", adjOpt.getScore());
        return temp;
    }

    @Test
    public void testCreateOptions() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request28.txt");
        Request request = new Request(voReq, TRANSACTION_ID);
        seatingOption.createAdjacentOptions(request.getFltLeg(), TRANSACTION_ID);
        Option[] options = seatingOption.createOptions(request.getFltLeg(), request.getPnrs()[0], TRANSACTION_ID);
        assertNotNull(options);
        assertEquals(816, options.length);
    }

    private String getOptString(Option option) {
        PsgrSeat[] psgrSeats = option.getPsgrSeats();
        String opt = psgrSeats[0].getSeat().getSeatId() + ' ' + psgrSeats[1].getSeat().getSeatId() + ' ' + psgrSeats[2].getSeat().getSeatId() + ' ' + String.format("%.1f", option.getScore());
        return opt;
    }


    @Test
    public void testSelectSeats28() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request28.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, TRANSACTION_ID, false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("18C", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18D", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("18E", voSeatSelctList.get(2).getSelectedSeatId());
    }

    @Test
    public void test2PaxPnrAisleAisle() {
        //this is test scenario #5
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//2pax_pnr_Aisle_Aisle.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "Test2pax_pnr_Aisle_Aisle", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("18C", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18D", voSeatSelctList.get(1).getSelectedSeatId());
    }

    @Test
    public void test2PaxPnrOneSeat() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//2pax_pnr_One_Seat.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "TEST2pax_pnr_One_Seat", false);
        assertNull(resp.getFlightSegmentOutputs());
    }


    @Test
    public void test2PaxPnrAisleGeneric() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//2pax_pnr_Aisle_Generic.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "Test2pax_pnr_Aisle_Generic", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("18C", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18D", voSeatSelctList.get(1).getSelectedSeatId());
    }

    @Test
    public void test3PaxPnr2Seats() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//3pax_pnr_2_Seats.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "TEST3pax_pnr_2_Seats", false);
        assertNull(resp.getFlightSegmentOutputs());
    }

    @Test
    public void test3PaxPnrAisleGeneric() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//3pax_pnr_NR_A_W_G.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "TEST3pax_pnr_NR_A_W_G", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("35E", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("36E", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("36D", voSeatSelctList.get(2).getSelectedSeatId());
    }

    @Test
    public void test3PaxPnrAisleGeneric2() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//3pax_pnr_NR_A_W_G_2.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "TEST3pax_pnr_NR_A_W_G_2", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("35E", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("36E", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("36D", voSeatSelctList.get(2).getSelectedSeatId());
    }

    @Test
    public void testWide2PaxAisleWindow() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//wide_2pax_Aisle_window.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "Testwide_2pax_Aisle_window", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("18F", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18G", voSeatSelctList.get(1).getSelectedSeatId());
    }

    @Test
    public void testWide3PaxAisleWindowGeneric() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//wide_3_pax_NR_A_W_G.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "TESTwide_3_pax_NR_A_W_G", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("17C", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18A", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("17B", voSeatSelctList.get(2).getSelectedSeatId());
    }

    @Test
    public void testWide3PaxPnrTriplets() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//wide_3_pax_pnr_triplets.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "TESTwide_3_pax_pnr_triplets", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("17C", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18A", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("17B", voSeatSelctList.get(2).getSelectedSeatId());
    }

    @Test
    public void testAscendingOrderSolution() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//ascendingOrderSolution.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "ascendingOrderSolution", false);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("05A", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("09D", voSeatSelctList.get(1).getSelectedSeatId());
    }

    @Test
    public void testMasterList() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//masterListTest.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = seatingOption.selectSeats(voReq, "masterList", true);
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("15A", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("15D", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("15G", voSeatSelctList.get(2).getSelectedSeatId());
    }
}
