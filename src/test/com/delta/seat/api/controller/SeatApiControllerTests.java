package com.delta.seat.api.controller;

import com.delta.seat.api.dao.SeatEngineEvtMsgDao;
import com.delta.seat.api.service.SeatApiServiceImpl;
import com.delta.seat.api.service.SeatServiceDelegator;
import com.delta.seat.api.test.util.TestSupport;
import com.delta.seat.api.test.util.TestUtil;
import com.delta.seat.api.vo.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.delta.seat.constants.SeatEngineConstants.NO_SEAT_SELECTION_MSG;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SeatApiControllerTests  extends TestSupport{

    @Autowired
    private SeatApiController seatApiController;

    private String authorization = "123";
    private String contentType = "application/json";
    private String transactionId = "12ZZZZZ";
    private String appId = "seat";
    private String channelId = "seat";
    private String applicationName = "seat";
    private String seatScore = "Y";
    private String agentId = "123";
    private String masterList = "N";

    @Autowired
    private SeatEngineEvtMsgDao seatEngineEvtMsgDao;


    private SeatSelectionInput seatSelectionRequest;

    @Autowired
    private SeatApiServiceImpl seatApiService;
    @Autowired
    private SeatServiceDelegator seatServiceDelegator;

    @Before
    public void before() throws Exception {
          seatSelectionRequest = new SeatSelectionInput();
    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testNoSeatSelected_VerifyResponseMsg() throws IOException {

        String transaction_id = "43b0d83d-d0ae-424b-b043-3098c3e89a23_FA";

        transaction_id = transaction_id + "_" + System.currentTimeMillis();

        System.out.println("transaction_id: " + transaction_id);

        seatSelectionRequest = createSeatSelectionInputForNoSeatSelection();
        SeatSelectionOutput seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                transactionId,appId,channelId,applicationName,seatScore,masterList);
        assertNotNull(seatRecommedationResponse);
        assertEquals(NO_SEAT_SELECTION_MSG, seatRecommedationResponse.getResponseMessageText());
    }

    @Test
    public void testAdjacenty_wide_3pax_A_W_G_WithSeatScore() {
        com.delta.seat.api.vo.SeatSelectionInput seatSelectionRequest = TestUtil.getRequest("UnitTests//JsonFiles//wide_3pax_new_A_W_G.txt");
        SeatSelectionOutput seatRecommedationResponse = null;
        try {
            seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                    transactionId,appId,channelId,seatScore,applicationName,masterList);

//            List<String> seatSelectionJsonList = seatEngineEvtMsgDao.getSeatEngReqMsgBasedOnOpasFltNumber("0006");
//            SeatSelectionInput jsonObjectFromString = (SeatSelectionInput)StringUtils.getJsonObjectFromString(seatSelectionJsonList.get(0), SeatSelectionInput.class);
            assertNotNull(seatRecommedationResponse);
            assertNotNull(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0));
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId(),"18F");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(1).getSelectedSeatId(),"18I");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(2).getSelectedSeatId(),"18G");

        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(seatRecommedationResponse);
    }

    @Test
    public void testAdjacenty_SkipExitRowSeat() {
        com.delta.seat.api.vo.SeatSelectionInput seatSelectionRequest = TestUtil.getRequest("UnitTests//JsonFiles//SkipExitRowSeatScenario.txt");
        SeatSelectionOutput seatRecommedationResponse = null;
        try {
            seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                    transactionId,appId,channelId,seatScore,applicationName,masterList);



//            List<String> seatSelectionJsonList = seatEngineEvtMsgDao.getSeatEngReqMsgBasedOnOpasFltNumber("0006");
//            SeatSelectionInput jsonObjectFromString = (SeatSelectionInput)StringUtils.getJsonObjectFromString(seatSelectionJsonList.get(0), SeatSelectionInput.class);
            assertNotNull(seatRecommedationResponse);
            assertNotNull(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0));
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId(),"18B");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(1).getSelectedSeatId(),"18A");

        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(seatRecommedationResponse);
    }

    @Test
    public void testMasterList_Scenario_1() {
        seatScore = "N";
        masterList = "Y";
        com.delta.seat.api.vo.SeatSelectionInput seatSelectionRequest = TestUtil.getRequest("UnitTests//JsonFiles//masterListTest.txt");
        SeatSelectionOutput seatRecommedationResponse = null;
        try {
            seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                    transactionId,appId,channelId,seatScore,applicationName,masterList);

            assertNotNull(seatRecommedationResponse);
            assertNotNull(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0));
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId(),"15A");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(1).getSelectedSeatId(),"15D");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(2).getSelectedSeatId(),"15G");

        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(seatRecommedationResponse);
    }

    @Test
    public void testASAAdjacency_Scenario_1() {
        transactionId = "Test1" + transactionId;
        seatScore = "Y";
        masterList = "N";
        com.delta.seat.api.vo.SeatSelectionInput seatSelectionRequest = TestUtil.getRequest("UnitTests//JsonFiles//test1_OCI.txt");
        SeatSelectionOutput seatRecommedationResponse = null;
        try {
            seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                    transactionId,appId,channelId,seatScore,applicationName,masterList);

            assertNotNull(seatRecommedationResponse);
            assertNotNull(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0));
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId(),"19A");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(1).getSelectedSeatId(),"16D");

        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(seatRecommedationResponse);
    }

    @Test
    public void testASAAdjacency_Scenario_2() {
        transactionId = "Test2" + transactionId;
        seatScore = "Y";
        masterList = "N";
        com.delta.seat.api.vo.SeatSelectionInput seatSelectionRequest = TestUtil.getRequest("UnitTests//JsonFiles//test2_OCI.txt");
        SeatSelectionOutput seatRecommedationResponse = null;
        try {
            seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                    transactionId,appId,channelId,seatScore,applicationName,masterList);

            assertNotNull(seatRecommedationResponse);
            assertNotNull(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0));
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId(),"16F");

        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(seatRecommedationResponse);
    }

    @Test
    public void testASAAdjacency_Scenario_3() {
        transactionId = "Test3" + transactionId;
        seatScore = "Y";
        masterList = "N";
        com.delta.seat.api.vo.SeatSelectionInput seatSelectionRequest = TestUtil.getRequest("UnitTests//JsonFiles//test3_KIOSK.txt");
        SeatSelectionOutput seatRecommedationResponse = null;
        try {
            seatRecommedationResponse = seatApiController.selectseat(seatSelectionRequest, authorization,contentType,
                    transactionId,appId,channelId,seatScore,applicationName,masterList);

            assertNotNull(seatRecommedationResponse);
            assertNotNull(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0));
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getSelectedSeatId(),"25E");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(0).getRecordLocatorId(),"GXL5JK");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(1).getSelectedSeatId(),"27F");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(1).getRecordLocatorId(),"GXL5JK");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(2).getSelectedSeatId(),"27G");
            assertEquals(seatRecommedationResponse.getFlightSegmentOutputs().get(0).getSeatSelections().get(2).getRecordLocatorId(),"GXL5JK");

        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(seatRecommedationResponse);
    }

    private SeatSelectionInput createSeatSelectionInputForNoSeatSelection() {
        SeatSelectionInput seatRecommendationRequest = new SeatSelectionInput();
        List<FlightSegmentInput> flightInfoRequestList = new ArrayList<FlightSegmentInput>();
        FlightSegmentInput flightInfoRequest = createFlightSegmentInput();
        flightInfoRequestList.add(flightInfoRequest);

        SeatMap seatMap = new SeatMap();

        List<Compartment> cabinList = new ArrayList<Compartment>();
        Compartment cabin = null;
        SeatRow row = null;
        List<SeatRow> rowList = null;
        List<Seat> seatInfoList = null;
        Seat seatInfo = null;

        //Creating Y cabin first intentionally to see the sort order works.
        //creat Y cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("Y");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        String rowNumber = "24";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "C", false, false, false,"false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);


        row = new SeatRow();
        rowNumber = "23";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "D", false, false, false,"false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "B", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);
        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);


        //creat C cabin
        cabin = new Compartment();
        cabin.setCompartmentCode("C");
        rowList = new ArrayList<SeatRow>();

        row = new SeatRow();
        rowNumber = "7";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "false", false);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, true, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, true, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        row = new SeatRow();
        rowNumber = "8";
        row.setCabinRowSequenceNum(rowNumber);
        seatInfoList = new ArrayList<Seat>();
        seatInfo = createSeat(rowNumber + "A", false, false, false, "false", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "G", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "C", false, false, false, "true", true);
        seatInfoList.add(seatInfo);
        seatInfo = createSeat(rowNumber + "J", false, false, false, "true", false);
        seatInfoList.add(seatInfo);
        row.setSeats(seatInfoList);

        rowList.add(row);

        cabin.setSeatRows(rowList);
        cabinList.add(cabin);

        seatMap.setCompartments(cabinList);


        SeatApiServiceImpl seatApiService = new SeatApiServiceImpl();
        seatServiceDelegator.sortSeatMapData(seatMap, transactionId);

        flightInfoRequest.setSeatMap(seatMap);
        List<Offer> offerList = new ArrayList<Offer>();
        Offer offer = createOffer();
        offerList.add(offer);

        List<Passenger> passengerList = new ArrayList<Passenger>();
        Passenger passenger = createPassenger();
        passengerList.add(passenger);

        List<Seatoffer> seatOfferList = new ArrayList<Seatoffer>();
        seatOfferList.add(createSeatOffer("1A"));

        passenger.setSeatOffers(seatOfferList);

        List<LoyaltyMemberPreference> loyaltyPreferenceList = new ArrayList<LoyaltyMemberPreference>();

        LoyaltyMemberPreference loyaltyPreference = new LoyaltyMemberPreference();
        loyaltyPreferenceList.add(loyaltyPreference);
        passenger.setLoyaltyMemberPreferences(loyaltyPreferenceList);

        offer.setPassengers(passengerList);
        flightInfoRequest.setOffers(offerList);

        seatRecommendationRequest.setFlightSegments(flightInfoRequestList);
        return seatRecommendationRequest;
    }

    private Offer createOffer() {
        Offer offer = new Offer();
        offer.setOfferId("43b0d83d-d0ae-424b-b043-3098c3e89a23_FA");
        offer.setRecordLocatorId("PDXHTR");
        offer.setOrderNum("01");
        return offer;
    }
    private Seat createSeat(String seatId, Boolean window, Boolean middle, Boolean aisle, String preferred, Boolean available) {
        Seat seatInfo = new Seat();
        seatInfo.setBrandId("DLCL");
        seatInfo.setSeatAvailableCode(available?"true":"false");
        seatInfo.setCharacteristicCode("O");
        seatInfo.setOccupationCode("O");
        seatInfo.setPreferredText(preferred);
        seatInfo.setSeat(true);
        seatInfo.setSeatId(seatId);
        seatInfo.setWindow(window);
        seatInfo.setMiddle(middle);
        seatInfo.setAisle(aisle);
        return seatInfo;
    }

    private Seatoffer createSeatOffer(String seatId) {
        Seatoffer seatoffer = new Seatoffer();
        seatoffer.setPaymentAmt("0.00");
        seatoffer.setSeatId(seatId);
        return seatoffer;
    }

    private FlightSegmentInput createFlightSegmentInput() {
        FlightSegmentInput flightInfoRequest = new FlightSegmentInput();
        flightInfoRequest.setDestinationAirportCode("SEA");
        flightInfoRequest.setAssociationId("01");
        flightInfoRequest.setClassOfServiceCode("M");
        flightInfoRequest.setOriginAirportCode("LHR");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setScheduledDepartureLocalDate("2018-03-06");
        flightInfoRequest.setMarketingCarrierCode("DL");
        flightInfoRequest.setMarketingFlightNum("105");
        flightInfoRequest.setOperatingCarrierCode("DL");
        flightInfoRequest.setOperatingFlightNum("105");
        return flightInfoRequest;
    }

    private Passenger createPassenger() {
        Passenger passenger = new Passenger();
        passenger.setFirstNameNum("01");
        passenger.setLastNameNum("01");
        passenger.setTravelerId("01-01");
        passenger.setPaxPriorityScoreNum("12345");
        passenger.setCurrencyCode("USD");
        passenger.setOnBoard(false);
        passenger.setBoardingPass(true);
        passenger.setCheckedIn(false);
        passenger.setConnection(false);
        return passenger;
    }
}


