package com.delta.seat.api.test.util;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.hamcrest.SelfDescribing;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/seatengineapi.context.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class TestSupport {

	@BeforeClass
	public static void loadProperties(){
		
	}	

	@Test
	public void doNothing(){
		
	}
	
}
