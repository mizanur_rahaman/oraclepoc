package com.delta.seat.api.test.util;

import com.delta.seat.api.selection.RequestTest;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import com.delta.seat.api.vo.*;

public class TestUtil {

    public static SeatSelectionInput getRequest(String fileName) {
        InputStream inputStream = null;
        File file = new File(fileName);
        if (file.exists())
            try {
                inputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        else
            inputStream = RequestTest.class.getClassLoader().getResourceAsStream(fileName);
        ObjectMapper objMapper = new ObjectMapper();
        com.delta.seat.api.vo.SeatSelectionInput voReq = null;
        try {
            voReq = objMapper.readValue(inputStream, com.delta.seat.api.vo.SeatSelectionInput.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return voReq;
    }
}
