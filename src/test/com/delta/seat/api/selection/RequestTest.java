package com.delta.seat.api.selection;

import com.delta.seat.api.test.util.TestUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.delta.seat.api.vo.*;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * These are jvm parameters needed to run the class:
 * -Dseat.logdir=C:\logs\seat -Dseat.logfile=SEAT.log -Ddatadir=C:\work\seating\data
 */
public class RequestTest {
    private static Request request;
    private static final String TRANSACTION_ID = "123456";

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testCreateAdjacentOptions() {
        Policy.reloadDefaultPolicy();
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request28.txt");
        request = new Request(voReq, "TEST123456");
        AdjacentOption[] adjOptions = request.createAdjacentOptions();
        assertNotNull(adjOptions);
        assertEquals(22, adjOptions.length);
        assertEquals("18C 18D 18E 11.0", getAdjString(adjOptions[0]));
        assertEquals("18A 18C 18D 9.0", getAdjString(adjOptions[1]));
        assertEquals("35B 35C 36A 8.0", getAdjString(adjOptions[2]));
        assertEquals("35B 35C 36B 8.0", getAdjString(adjOptions[3]));
        assertEquals("35B 36A 36B 8.0", getAdjString(adjOptions[4]));
        assertEquals("35C 36A 36B 8.0", getAdjString(adjOptions[5]));
        assertEquals("35E 36D 36E 8.0", getAdjString(adjOptions[6]));
        assertEquals("15B 15C DR 6.0", getAdjString(adjOptions[7]));
        assertEquals("18D 18E DR 6.0", getAdjString(adjOptions[8]));
        assertEquals("35B 35C DR 6.0", getAdjString(adjOptions[9]));
        assertEquals("36A 36B DR 6.0", getAdjString(adjOptions[10]));
        assertEquals("36D 36E DR 6.0", getAdjString(adjOptions[11]));
        assertEquals("18C 18D AA 5.0", getAdjString(adjOptions[12]));
        assertEquals("18A 18C SS 4.0", getAdjString(adjOptions[13]));
        assertEquals("24B 25B NR 1.0", getAdjString(adjOptions[14]));
        assertEquals("32B 33A NR 1.0", getAdjString(adjOptions[15]));
        assertEquals("35B 36A NR 1.0", getAdjString(adjOptions[16]));
        assertEquals("35B 36B NR 1.0", getAdjString(adjOptions[17]));
        assertEquals("35C 36A NR 1.0", getAdjString(adjOptions[18]));
        assertEquals("35C 36B NR 1.0", getAdjString(adjOptions[19]));
        assertEquals("35E 36D NR 1.0", getAdjString(adjOptions[20]));
        assertEquals("35E 36E NR 1.0", getAdjString(adjOptions[21]));
    }

    @Test
    public void testCreateAdjacentOptions_wide_3pax_A_W_G_WithSeatScore() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//wide_3pax_new_A_W_G.txt");
        request = new Request(voReq, "Test123456");
        AdjacentOption[] adjOptions = request.createAdjacentOptions();
        assertNotNull(adjOptions);
//        assertEquals(22, adjOptions.length);
//        assertEquals("18C 18D 18E 11.0", getAdjString(adjOptions[0]));
//        assertEquals("18A 18C 18D 9.0", getAdjString(adjOptions[1]));
//        assertEquals("35B 35C 36A 8.0", getAdjString(adjOptions[2]));
//        assertEquals("35B 35C 36B 8.0", getAdjString(adjOptions[3]));
//        assertEquals("35B 36A 36B 8.0", getAdjString(adjOptions[4]));
//        assertEquals("35C 36A 36B 8.0", getAdjString(adjOptions[5]));
//        assertEquals("35E 36D 36E 8.0", getAdjString(adjOptions[6]));
//        assertEquals("15B 15C DR 6.0", getAdjString(adjOptions[7]));
//        assertEquals("18D 18E DR 6.0", getAdjString(adjOptions[8]));
//        assertEquals("35B 35C DR 6.0", getAdjString(adjOptions[9]));
//        assertEquals("36A 36B DR 6.0", getAdjString(adjOptions[10]));
//        assertEquals("36D 36E DR 6.0", getAdjString(adjOptions[11]));
//        assertEquals("18C 18D AA 5.0", getAdjString(adjOptions[12]));
//        assertEquals("18A 18C SS 4.0", getAdjString(adjOptions[13]));
//        assertEquals("24B 25B NR 1.0", getAdjString(adjOptions[14]));
//        assertEquals("32B 33A NR 1.0", getAdjString(adjOptions[15]));
//        assertEquals("35B 36A NR 1.0", getAdjString(adjOptions[16]));
//        assertEquals("35B 36B NR 1.0", getAdjString(adjOptions[17]));
//        assertEquals("35C 36A NR 1.0", getAdjString(adjOptions[18]));
//        assertEquals("35C 36B NR 1.0", getAdjString(adjOptions[19]));
//        assertEquals("35E 36D NR 1.0", getAdjString(adjOptions[20]));
//        assertEquals("35E 36E NR 1.0", getAdjString(adjOptions[21]));
    }

    private String getAdjString(AdjacentOption adjOpt) {
        Seat[] seats = adjOpt.getSeats();
        String temp = null;
        if (adjOpt.size() == 3)
            temp = seats[0].getSeatId() + ' ' + seats[1].getSeatId() + ' ' + seats[2].getSeatId() + ' ' + String.format("%.1f", adjOpt.getScore());
        else if (adjOpt.size() == 2)
            temp = seats[0].getSeatId() + ' ' + seats[1].getSeatId() + ' ' + adjOpt.getAdjType12() + ' ' + String.format("%.1f", adjOpt.getScore());
        return temp;
    }

    @Test
    public void testCreateOptions() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request28.txt");
        String transactionId = "Test123455";
        request = new Request(voReq, transactionId);
        request.createAdjacentOptions();
        Option[] options = request.createOptions(0, transactionId);
        assertNotNull(options);
        assertEquals(816, options.length);
        PsgrSeat[] psgrSeats = options[0].getPsgrSeats();
        String opt = psgrSeats[0].getSeat().getSeatId() + ' ' + psgrSeats[1].getSeat().getSeatId() + ' ' + psgrSeats[2].getSeat().getSeatId() + ' ' + String.format("%.1f", options[0].getScore());
        assertEquals("18C 18E 18D 56.1", getOptString(options[0]));
        assertEquals("18C 18A 18D 54.1", getOptString(options[1]));   //optionally 18D 18A 18C
        assertEquals("18D 18A 18E 50.6", getOptString(options[2]));
        assertEquals("18D 18E 24D 49.3", getOptString(options[3]));   //optionally 24D 18E 18D
        assertEquals("18C 18A 18E 48.6", getOptString(options[4]));   //optionally 18C 18E 18A
        assertEquals("18C 18A 24D 47.3", getOptString(options[5]));
        assertEquals("18C 18D 24D 44.3", getOptString(options[6]));
        assertEquals("18D 18E 24B 44.3", getOptString(options[7]));
        assertEquals("18D 18E 25B 43.9", getOptString(options[8]));
        assertEquals("18D 18E 33A 43.9", getOptString(options[9]));
        assertEquals("15C 18E 18D 43.7", getOptString(options[10]));
        assertEquals("18C 33A 18D 43.4", getOptString(options[11]));
        assertEquals("18C 18E 24D 43.3", getOptString(options[13]));
        assertEquals("18D 18A 24D 43.3", getOptString(options[12]));
        assertEquals("24D 18A 18E 42.8", getOptString(options[14]));
        assertEquals("18C 18A 24B 42.3", getOptString(options[15]));
        assertEquals("18C 18A 25B 41.9", getOptString(options[16]));
        assertEquals("18C 18A 33A 41.9", getOptString(options[17]));
        assertEquals("15C 18A 18C 41.7", getOptString(options[18]));
        assertEquals("18D 18E 32B 40.1", getOptString(options[19]));
    }

    private String getOptString(Option option) {
        PsgrSeat[] psgrSeats = option.getPsgrSeats();
        String opt = psgrSeats[0].getSeat().getSeatId() + ' ' + psgrSeats[1].getSeat().getSeatId() + ' ' + psgrSeats[2].getSeat().getSeatId() + ' ' + String.format("%.1f", option.getScore());
        return opt;
    }
    @Test
    public void testRequest28() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request28.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = Request.run(voReq, "Test123456");
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("18C", voSeatSelctList.get(0).getSelectedSeatId());
        assertEquals("18E", voSeatSelctList.get(1).getSelectedSeatId());
        assertEquals("18D", voSeatSelctList.get(2).getSelectedSeatId());
    }

    @Test
    public void testRequest30() {
        com.delta.seat.api.vo.SeatSelectionInput voReq = TestUtil.getRequest("UnitTests//JsonFiles//request30.txt");
        com.delta.seat.api.vo.SeatSelectionOutput resp = Request.run(voReq, "Test123456");
        assertNotNull(resp);
        List<FlightSegmentOutput> voFltRespList = resp.getFlightSegmentOutputs();
        assertEquals(1, voFltRespList.size());
        List<com.delta.seat.api.vo.SeatSelection> voSeatSelctList = voFltRespList.get(0).getSeatSelections();
        assertEquals("18A", voSeatSelctList.get(0).getSelectedSeatId());
    }

}
